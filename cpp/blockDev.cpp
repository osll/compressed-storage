#include "blockDev.h"

void BlockDev::BDswap(BlockDev &bD){
  swap(size, bD.size);
  swap(blkSize, bD.blkSize);
  swap(blocks, bD.blocks);
}

BlockDev::BlockDev(long unsigned int blockNumber){
  
  do{
    blocks.push_back( Block (blkSize, 0));
  }while(blocks.size() < size);
}

blkDev::~blkDev(){}

blkDev::blkDev(blkDev &bD):size(bD.size), blkSize(bD.blkSize){
  for (int i = 0; i != size; ++i)
    for (int j = 0; j != blkSize; ++j)
      blocks.at(i).at(j) = bD.blocks.at(i).at(j);
}

blkDev& blkDev::operator=(blkDev &bD){
  if (&bD != this){
    blkDev(bD).BDswap(*this);
    return *this;
  }
}

int blkDev::getBlockNumber() const{
  return size;
}

void blkDev::readBlock(int blkID, Block &blk){
  if (blkID < size && blkID >= 0 && blk.size() >= blkSize)
    for (int j = 0; j != blkSize; ++j)
      blk.at(j) = blocks.at(blkID).at(j);
  else
    exit(EXIT_FAILURE);
}

void blkDev::writeBlock(int blkID, const Block &blk){
  if (blkID < size && blkID >= 0 && blk.size() <= blkSize){
    for (int j = 0; j != blkSize; ++j)
      blocks.at(blkID).at(j) = 0;
    for (int j = 0; j != blk.size(); ++j)
      blocks.at(blkID).at(j) = blk.at(j);
  }
  else
    exit(EXIT_FAILURE);
}


