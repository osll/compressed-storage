#include <vector>
#include <algorithm>
#include <stdint.h>
#include <cstdlib>
#include "block.h"

using namespace std;

#ifndef BLKDEV_H
#define BLKDEV_H

extern size_t BLOCK_SZ;

struct BlockDev
{
private:
  vector <Block> m_blocks;
  void BDswap(BlockDev &bD);
  ~BlockDev();
public:
  BlockDev(long unsigned int blockNumber);
  BlockDev(BlockDev &bD);
  BlockDev &operator=(BlockDev &bD);
  int getBlockNumber() const;
  int getBlockSize() const;
  void readBlock(int blkID, Block &blk);
  void writeBlock(int blkID, const Block &blk);
};

#endif
