#include <cstdio>
#include "Block.h"

int main(){
  g_BlockSize = 20;
  Block block;
  char wdata[g_BlockSize];
  char rdata[g_BlockSize];
  int count = 0;

  for (int i = 0; i < g_BlockSize; ++i)
    wdata[i] = 'a';

  block.init();
  // Check Block size
  if (g_BlockSize == block.getSize())
    printf("Block Size: PASS\n");
  else
    printf("Block Size: FAIL\n");

  // Check Read & Write methods
  block.setData(wdata);
  block.getData(rdata);
  // Contents of wdata (source of Block data) and rdata (copy of Block data) should be the same
  for (int i = 0; i < g_BlockSize; ++i)
    if (wdata[i] = rdata[i])
      count++;

  if (g_BlockSize == count)
    printf("RW: PASS\n");
  else
    printf("RW: FAIL\n");

  return 0;
}
