#include <vector>
#include <assert.h>
#include "Block.h"

using namespace std;

#ifndef BLKDEV_H
#define BLKDEV_H

// BlockDev is a 1D vector of Blocks, where Block is a 1D vector of chars
// blockNumber is BlockDev size measured in Blocks
// Blocks size is global var, defined in block.h

struct BlockDev
{
private:
  vector <Block> m_blocks;
public:
  ~BlockDev();
  BlockDev(long unsigned int blockNumber);
  BlockDev(BlockDev &bD);
  BlockDev &operator=(BlockDev &bD);
  int getBlockNumber() const; // Return BlockDev size in Blocks
  int getBlockSize() const;  // Return Block size in chars
  void readBlock(int blockID, char *data); // Read data from Block w/ number =blockID to blk
  void writeBlock(int blockID, const char *data); // Write data to Block w/ number =blockID from blk
};

#endif
