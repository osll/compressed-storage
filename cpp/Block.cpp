#include "Block.h"

long unsigned int g_BlockSize = 512;

void Block::init(){
  m_data.resize(g_BlockSize); //Create Block filled with "0"'s
}

int Block::getSize() const{
  return m_data.size(); //Return Block size measured in chars
}

void Block::getData(char *data){
  for (int i = 0; i < m_data.size(); ++i)
   data[i] = m_data[i]; // Assign Block data to blk
}

void Block::setData(const char *data){
  for (int i = 0; i < m_data.size(); ++i)
    m_data[i] = data[i];  // Assign blk data to Block
}


