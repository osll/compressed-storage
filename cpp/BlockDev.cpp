#include "BlockDev.h"


BlockDev::BlockDev(long unsigned int blockNumber){
  m_blocks.resize(blockNumber); // Allocate mem for BlockDev
  for(int i = 0; i != m_blocks.size(); ++i)
    m_blocks[i].init();      // Create empty Blocks
}

BlockDev::~BlockDev(){}

BlockDev::BlockDev(BlockDev &bD){
  m_blocks = bD.m_blocks;
}

BlockDev& BlockDev::operator=(BlockDev &bD){
  if (&bD != this){
    swap(m_blocks, bD.m_blocks);
    return *this;
  }
}

int BlockDev::getBlockNumber() const{
  return m_blocks.size(); //Return BlockDev size measured in Blocks
}

int BlockDev::getBlockSize() const{
  assert(m_blocks.size() > 0); // Check if there's any Block in BlockDev
  return m_blocks[0].getSize(); //Return Block size measured in chars
}

void BlockDev::readBlock(int blockID, char *data){
  assert(blockID < m_blocks.size()); // Check blockID validity
  m_blocks[blockID].getData(data); // Assign Block data to blk
}

void BlockDev::writeBlock(int blockID, const char *data){
  assert(blockID < m_blocks.size()); // Check blockID validity
  m_blocks[blockID].setData(data);  // Assign blk data to Block
}


