#include <cstdio>
#include "BlockDev.h"
#include "Block.h"

#define BLOCKNUMBER 10
#define BLOCKID 5

int main(){
  g_BlockSize = 20;
  BlockDev HDD (BLOCKNUMBER);
  char wdata[g_BlockSize];
  char rdata[g_BlockSize];
  int count = 0;

  for (int i = 0; i < g_BlockSize; ++i)
    wdata[i] = 'a';

  // Check BlockDev size
  if (BLOCKNUMBER == HDD.getBlockNumber())
    printf("HDD Size: PASS\n");
  else
    printf("HDD Size: FAIL\n");

  // Check Block size
  if (g_BlockSize == HDD.getBlockSize())
    printf("Block Size: PASS\n");
  else
    printf("Block Size: FAIL\n");

  // Check Read & Write methods
  HDD.writeBlock(BLOCKID, wdata);
  HDD.readBlock(BLOCKID, rdata);
  // Contents of wdata (source of Block #BLOCKID data) and rdata (copy of Block #BLOCKID data) should be the same
  for (int i = 0; i < g_BlockSize; ++i)
    if (wdata[i] = rdata[i])
      count++;

  if (g_BlockSize == count)
    printf("RW: PASS\n");
  else
    printf("RW: FAIL\n");

  return 0;
}
