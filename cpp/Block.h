#include <vector>

using namespace std;

#ifndef BLOCK_H
#define BLOCK_H

extern long unsigned int g_BlockSize;
// Block is a 1D vector of chars.
// g_BlockSize is Block size measured in chars, defined in block.cpp

struct Block
{
private:
  vector <char> m_data;
public:
  void init(); // Create dummy Block
  int getSize() const;  // Return Block size in chars
  void getData(char *data); // Read data from Block to blk
  void setData(const char *data); // Write data to Block from blk
};

#endif
