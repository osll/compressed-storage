#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <linux/netlink.h>

#define NETLINK_USER 26

#define MAX_PAYLOAD 1024


struct sockaddr_nl src_addr, dest_addr;
struct nlmsghdr *nlh = NULL;
struct iovec iov;
int sock_fd;
struct msghdr msg;

int main()
{
	int entire_count = -1;
	int entire_free_count = -1;
	int halves_count = -1;
	int halves_free_count = -1;
	int quarters_count = -1;
	int quarters_free_count = -1;
	printf("Loading client..\n");
   sock_fd = socket(PF_NETLINK, SOCK_RAW, NETLINK_USER);
   if (sock_fd < 0)
	{
		printf("Can't load client! Err: %d\n", sock_fd);
   	return -1;
	}
   memset(&src_addr, 0, sizeof(src_addr));
   src_addr.nl_family = AF_NETLINK;
   src_addr.nl_pid = getpid();

   bind(sock_fd, (struct sockaddr *)&src_addr, sizeof(src_addr));

   memset(&dest_addr, 0, sizeof(dest_addr));
   memset(&dest_addr, 0, sizeof(dest_addr));
   dest_addr.nl_family = AF_NETLINK;
   dest_addr.nl_pid = 0; 
   dest_addr.nl_groups = 0;

   nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PAYLOAD));
   memset(nlh, 0, NLMSG_SPACE(MAX_PAYLOAD));
   nlh->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD);
   nlh->nlmsg_pid = getpid();
   nlh->nlmsg_flags = 0;

   strcpy(NLMSG_DATA(nlh), "0");

   iov.iov_base = (void *)nlh;
   iov.iov_len = nlh->nlmsg_len;
   msg.msg_name = (void *)&dest_addr;
   msg.msg_namelen = sizeof(dest_addr);
   msg.msg_iov = &iov;
   msg.msg_iovlen = 1;

   printf("Sending message to kernel..\n");
   sendmsg(sock_fd, &msg, 0);
   printf("Waiting for message from kernel..\n");
	
	// Получаем сообщение от ядра
   recvmsg(sock_fd, &msg, 0);
	sscanf((const char *)NLMSG_DATA(nlh), "%d %d %d %d %d %d", 
			 &entire_free_count, &entire_count, &halves_free_count, 
			 &halves_count, &quarters_free_count, &quarters_count);
	printf("\nPool statistic:\n");
	printf("512-byte blocks \tall: %d\tfree: %d\n", entire_count, entire_free_count);
	printf("256-byte blocks \tall: %d\tfree: %d\n", halves_count, halves_free_count);
	printf("128-byte blocks \tall: %d\tfree: %d\n", quarters_count, quarters_free_count);
   close(sock_fd);
}
