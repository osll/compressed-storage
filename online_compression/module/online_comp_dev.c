#include "../common/common.h"
#include "../compression/lz4.c"
//#include "../common/ioctl.c"
#include "../common/daemon_param.h"

#include <linux/mutex.h>

static struct sock* netlink_socket = NULL;
static struct nlmsghdr* nlmsg_daemon;
static struct mutex my_mutex; 

static char** result = NULL;

static char* compress_buff;

static int daemon_pid = -1;



static int __init blk_init( void );
module_init( blk_init );

static void __exit blk_exit( void );
module_exit( blk_exit );

static int send_to_daemon(const char* source, size_t length)
{
	struct sk_buff* skb_out = NULL;

	// Создаем запрос на компрессию
	skb_out = nlmsg_new(length, 0);
	if (skb_out == NULL)
	{
		ERR("Failed to allocate skb_out!\n");
		return -1;
	}

	// Подготавливаем запрос
	nlmsg_daemon = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, length, 0);
	NETLINK_CB(skb_out).dst_group = 0;
	
	memcpy(nlmsg_data(nlmsg_daemon), source, length);

	// Отправляем демону
	if (nlmsg_unicast(netlink_socket, skb_out, daemon_pid))
	{
		ERR("Error while sending request to daemon!\n");
		return -1;	
	}
	return 0;
}

/**
 * Производит обращение к демону для компрессии блока
 *
 * @param source - буфер, содержащий блок для сжатия (размер блока KERNEL_SECTOR_SIZE)
 * @param dest - буфер, в который будет сохранен сжатый блок
 * @param size_after_compress - в данную переменную будет возвращен размер блока после сжатия
 * @return int - значение, отличное от нуля указывает на ошибку
 */
static int compress_block(const char* source, char* dest, size_t* size_after_compress)
{
	size_t command = COMPRESS;

	memcpy(compress_buff, &command, sizeof(size_t));
	memcpy(compress_buff + sizeof(size_t), source, KERNEL_SECTOR_SIZE);
	send_to_daemon(compress_buff, sizeof(size_t) + KERNEL_SECTOR_SIZE);

	*result = (char*)size_after_compress;

	ERR("Pointer before_compress: %p\n", *result);
	ERR("before %d\n", *size_after_compress);
	// Останавливаем выполнение до прихода ответа
	mutex_lock(&my_mutex);
	ERR("Pointer after_compress: %p\n", *result);
	ERR("after %d\n", *size_after_compress);

/*
	if (result == NULL)
	{
		ERR("Result_ptr doesnt change!\n");
		return -1;
	}
	LOG("Breakpoint WTF\n");
	memcpy(size_after_compress, *result + sizeof(size_t), sizeof(size_t));	

	LOG("Breakpoint 2\n");
	// Ответ получен, разбираем
	ERR("Answer is coming new!\n");
	
	if (*size_after_compress > 512)
	{
		return -1;
	}
	ERR("Coming size: %d!\n", *size_after_compress);	
	memcpy(dest, nlmsg_data(nlmsg_daemon) + sizeof(size_t)*2, *size_after_compress);*/
	return 0;
}

static int test_compress(void)
{
	size_t size_after_compress = 0;
	char* uncompress_data = vmalloc(KERNEL_SECTOR_SIZE);	
	char* compress_data = vmalloc(KERNEL_SECTOR_SIZE);
	
	sprintf(uncompress_data, "compress it, please");

	LOG("Breakpoint 1: %s\n", uncompress_data);

	compress_block(uncompress_data, compress_data, &size_after_compress);

	ERR("Answer coming, size after compress: %d\n", size_after_compress);

	vfree(uncompress_data);
	vfree(compress_data);
	return 0;
}

/**
 * Обработчик получения сообщения по netlink
 *
 * @param skb - буфер, содержащий полученное сообщение
 */
static void porcess_netlink_request(struct sk_buff* skb_in)
{
   struct sk_buff* skb_out = NULL;
   struct nlmsghdr* nl_msg = NULL;
	size_t command = 0;   
	int pid = -1;

	ERR("Get somethink netlink request!\n");
   nl_msg = (struct nlmsghdr*)skb_in->data;
	pid = nl_msg->nlmsg_pid;	   
	memcpy(&command, nlmsg_data(nl_msg), sizeof(size_t));
	ERR("smth going wrong: %d\n", command);	

	// Если это сообщение от демона - сохраняем для ответа
	if (command !=	GET_CAPACITY)
	{
		nlmsg_daemon = (struct nlmsghdr*)skb_in->data;
		daemon_pid = nlmsg_daemon->nlmsg_pid;
		ERR("Daemon pid: %d!\n", daemon_pid);
	}
	
	// Выполняем в соответствии с полученной командой
	switch (command)
	{
		case INIT:
		{
			LOG("Daemon success init!\n");
			test_compress();
			break;
		};
		
		case COMPRESS:
		{
			LOG("Comand unlock!\n");
			memcpy(*result, nlmsg_data(nl_msg) + sizeof(size_t), sizeof(size_t));
			ERR("Pointer after: %p\n", *result);			
			ERR("Size 1: %d\n", *((size_t*)*result));
			mutex_unlock(&my_mutex);
			LOG("Comand unlock success!\n");
			break; 
		};

		case DECOMPRESS:
		{
			LOG("Comand lock!\n");
			mutex_lock(&my_mutex);
			LOG("Comand lock success!\n");
			break;
		};		

		case GET_CAPACITY:
		{
			// Создаем ответ
			skb_out = nlmsg_new(CAPACITY_ANWSER_SIZE, 0);
			if (skb_out == NULL)
			{
				ERR("Failed to allocate skb_out\n");
				return;
			}

			// Отсылаем ответ
			nl_msg = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, CAPACITY_ANWSER_SIZE, 0);
			NETLINK_CB(skb_out).dst_group = 0;
			//strncpy(nlmsg_data(nl_msg), buff, BUFF_SIZE);
			if (nlmsg_unicast(netlink_socket, skb_out, pid))
			{
				ERR("Error while sending back to user\n");
			}
			break;
		}; 

		default:
		{
			ERR("Get unknown command: %d!\n", command);
			break;
		};
	}
}

/**
 * Инициализирует модуль в ядре
 */
static int __init blk_init(void) {
	struct netlink_kernel_cfg netlink_config = {
      .groups = 1,
   	.input = porcess_netlink_request
   };
	
	result = vmalloc(sizeof(char*));
	compress_buff = vmalloc(700); // TODO должно быть LZ4_compressBound(KERNEL_SECTOR_SIZE) + sizeof(size_t)
	
	// Инициализация netlink-сокета	
   netlink_socket = netlink_kernel_create(&init_net, NETLINK_USER, &netlink_config);
	if (netlink_socket == NULL)
   {
   	ERR("Failed init netlink socket!\n");
   	return -10;
   }

	// Инициализация мьютекса
	mutex_init(&my_mutex);
	//mutex_lock(&my_mutex);
   return 0;
}

/**
* Вызывается при выгрузке модуля из области ядра
*/
static void blk_exit(void) 
{
	vfree(result);
	vfree(compress_buff);
	sock_release(netlink_socket->sk_socket);
}

MODULE_AUTHOR( "Plokhoy Nikolay" );
