#include <string.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include "../user_compression/lz4.h"
#include "../user_compression/zlib.h"
#include "../user_compression/lz4.c"
#include "../user_compression/quicklz.h"
#include "../user_compression/quicklz.c"
#include "../common/daemon_param.h"
#include <unistd.h>
#include <linux/stat.h>
#include <errno.h>
#include <sys/socket.h>
#include "logger.c"
#define DATA_SIZE 700
#define MAX_SIZE 512

#define MAX_PAYLOAD 1024

static struct sockaddr_nl src_addr;
static struct sockaddr_nl dest_addr;
static struct iovec iov;
static struct nlmsghdr* nlh;
static struct msghdr msg;
static int socket_descriptor;
static char* compress_buff;
static char* uncompress_buff;

static struct LOG logger;
char b[MAX_SIZE];
char c[MAX_SIZE];

/**
 * Отправляет сообщение модулю ядра
 *
 * @param data - данные для передачи
 * @param length - длина данных в байтах
 */
static void send_to_kernel(const char* data, size_t length)

{

		
	memcpy(NLMSG_DATA(nlh), data, length);
        sendmsg(socket_descriptor, &msg, 0);

}

static void get_from_kernel(char* data, size_t* length)
{	
	int i = 0;
	recvmsg(socket_descriptor, &msg, 0);
	char buff[100] = {'\0'};
	//sscanf((const char*)NLMSG_DATA(nlh), "%s %s %s", &buff, &buff, &buff);
	memcpy(length, NLMSG_DATA(nlh), sizeof(size_t));
	memcpy(buff, NLMSG_DATA(nlh) + sizeof(size_t), 100);

	for (; i < 20; ++i)
	{
		printf("sym: |%c|\n", buff[i]);
	}

	printf("length: %d %s\n", *length, buff);
	
	
//	printf("data_1: %s\n", (const char*)NLMSG_DATA(nlh));
}

static void help_function_compress(size_t compressed_size)

{

	size_t command = COMPRESS;	
	if (compressed_size < 0)
	printf("Compress operation was not success n");
	// Если блок не сжался - сохраняем исходный блок
	if (compressed_size >= KERNEL_SECTOR_SIZE)
	{
		compressed_size = KERNEL_SECTOR_SIZE;
		memcpy(compress_buff + sizeof(size_t) * 2, NLMSG_DATA(nlh) + sizeof(size_t), KERNEL_SECTOR_SIZE);
	}
	
	// Записываем команду
	memcpy(compress_buff, &command, sizeof(size_t));

	// Записываем получившийся размер
	memcpy(compress_buff + sizeof(size_t), &compressed_size, sizeof(size_t));
	
	printf("compressed size: %d\n", (*(size_t*)(compress_buff + sizeof(size_t))));
	// Отсылаем модулю сжатый блок в формате |размер блока|+|блок|
	send_to_kernel(compress_buff, sizeof(size_t) * 2 + compressed_size);

}

static void help_function_decompress(size_t decompressed_size)

{

	size_t command = DECOMPRESS;	
	if (decompressed_size < 0)
	printf("decompress operation was not success n");
	// Если блок не сжался - сохраняем исходный блок
	if (decompressed_size >= KERNEL_SECTOR_SIZE)
	{
		decompressed_size = KERNEL_SECTOR_SIZE;
		memcpy(compress_buff + sizeof(size_t) * 2, NLMSG_DATA(nlh) + sizeof(size_t), KERNEL_SECTOR_SIZE);
	}
	
	// Записываем команду
	memcpy(compress_buff, &command, sizeof(size_t));

	// Записываем получившийся размер
	memcpy(compress_buff + sizeof(size_t), &decompressed_size, sizeof(size_t));
	
	printf("decompressed size: %d\n", (*(size_t*)(compress_buff + sizeof(size_t))));
	// Отсылаем модулю сжатый блок в формате |размер блока|+|блок|
	send_to_kernel(compress_buff, sizeof(size_t) * 2 + decompressed_size);

}

static void compress_block_with_lz4(const char* block)

{

	
	size_t compressed_size = 0; 
	printf("get block: %s\n", block);
	compressed_size = LZ4_COMPRESS(block, compress_buff );//TAKE FROM DAEMON_PARAM.H 	
	help_function_compress(compressed_size);
	
}

static void compress_block_with_zlib(const char* block)

{
 

    size_t compressed_size = 0;
    printf("zlib compress test start:\n");
    printf("get block: %s\n", block);
    z_stream defstream;
    defstream.zalloc = Z_NULL;
    defstream.zfree = Z_NULL;
    defstream.opaque = Z_NULL;
    defstream.avail_in = (uInt)strlen(block)+1;
    defstream.next_in = (Bytef *)block;
    defstream.avail_out = (uInt)sizeof(b);
    defstream.next_out = (Bytef *)b;  
    ZLIB_COMPRESS(defstream);
    help_function_compress(defstream.avail_out );		
 
 
}

static void decompress_block_with_zlib(const char* block,int alg)

{
 

      size_t decompressed_size=0;
      printf("zlib decompress test start:\n");
      printf("get sector: %s\n", block);
      z_stream defstream;
      z_stream infstream; 
      infstream.zalloc = Z_NULL;
      infstream.zfree = Z_NULL;
      infstream.opaque = Z_NULL;
      infstream.avail_in = (uInt)((char*)defstream.next_out - block);
      infstream.next_in = (Bytef *)block;
      infstream.avail_out = (uInt)sizeof(c);
      infstream.next_out = (Bytef *)c; 
      ZLIB_DECOMPRESS(infstream);
      help_function_decompress(infstream.avail_out);

 
      
}
 
static void decompress_block_with_lz4(const char* block)

{       

        printf("lz4 decompress  start:\n");
	size_t decompressed_size = 0; 
	printf("get sector: %s\n", block);
	decompressed_size = LZ4_DECOMPRESS(block, compress_buff); 	
	help_function_decompress(decompressed_size);
	

}

static void compress_block_with_quicklz( char* block) 

{
        size_t compressed_size=0;
        printf("QuickLz compress test start:\n");
        printf("get blok: %s\n", block);
        char  *compressed;
        size_t  size_in;
	size_in=sizeof(block);
	qlz_state_compress *state_compress = (qlz_state_compress *)malloc(sizeof(qlz_state_compress));
        compressed = (char*) malloc(10000 + 400); 
	memset(state_compress, 0, sizeof(qlz_state_compress)); 
        compressed_size=QLZ_COMPRESS(block,compressed,size_in,state_compress);
        help_function_compress(compressed_size);

   
}

static void decompress_block_with_quicklz(const char* block) 

{
    size_t decompressed_size=0; 
    printf("quicklz decompress start:\n");
    printf("get sector: %s\n", block);
    char  *decompressed;
    size_t size_out, size_in;
    qlz_state_decompress *state_decompress = (qlz_state_decompress *)malloc(sizeof(qlz_state_decompress));
    decompressed = (char*) malloc(10000);
    memset(state_decompress, 0, sizeof(qlz_state_decompress)); 
    size_in = qlz_size_compressed(block);
   decompressed_size = QLZ_DECOMPRESS(block,decompressed,state_decompress);  
    help_function_decompress(decompressed_size);   
 
}
/**
 * Осуществляет подключение к модулю в области ядра
 */
static void connect_to_module(void)
{
	size_t command = INIT;

	send_to_kernel((const char*)&command, sizeof(size_t));
}

/**
 * Инициализирует netlink-сокет и задает все требуемые параметры
 */
static int init_netlink(void)
{
        // Инициализация логгера
	LOG_init(&logger, "/tmp/cbda_demon_log");
	socket_descriptor = socket(PF_NETLINK, SOCK_RAW, NETLINK_USER);
   if (socket_descriptor < 0)
	{
		printf("Can't open socket! Err: %d\n", socket_descriptor);
   	return -1;
	}

	memset(&src_addr, 0, sizeof(src_addr));
   src_addr.nl_family = AF_NETLINK;
   src_addr.nl_pid = getpid();
	bind(socket_descriptor, (struct sockaddr*)&src_addr, sizeof(src_addr));

   memset(&dest_addr, 0, sizeof(dest_addr));
   dest_addr.nl_family = AF_NETLINK;
   dest_addr.nl_pid = 0; 
   dest_addr.nl_groups = 0;

	nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PAYLOAD));//DATA_SIZE
   memset(nlh, 0, NLMSG_SPACE(MAX_PAYLOAD));
   nlh->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD);
   nlh->nlmsg_pid = getpid();
   nlh->nlmsg_flags = 0;

   iov.iov_base = (void *)nlh;
   iov.iov_len = nlh->nlmsg_len;
   msg.msg_name = (void *)&dest_addr;
   msg.msg_namelen = sizeof(dest_addr);
   msg.msg_iov = &iov;
   msg.msg_iovlen = 1;
	return 0;
}

int main(int argc, char *argv[])
{
	size_t command = -1;
	int alg=0;
	
	init_netlink();	
	
	uncompress_buff = malloc(LZ4_compressBound(KERNEL_SECTOR_SIZE)); 
 	compress_buff = malloc(LZ4_compressBound(KERNEL_SECTOR_SIZE)); 
  
	//mkfifo(CBDA_FROM_KERNEL, 0777);
	//mkfifo(CBDA_TO_KERNEL, 0777);

	while (command != STOP)
	{       connect_to_module();
              // FILE* pipe_from_kernel = fopen(CBDA_FROM_KERNEL, "rb");
		printf("Waiting command...\n");
          //if(fread(&command, sizeof(size_t), 1, pipe_from_kernel) != 1)
		//{
			//printf("Can't get a command from PIPE\n");
			//logger.write(&logger, "Can't get a command from PIPE!");
		//}
             // size_t size_answer = 0;
	     //char*answer=NULL;
	     // get_from_kernel(answer, &size_answer);
	    recvmsg(socket_descriptor, &msg, 0);
              //  memcpy(&alg, argv[0], sizeof(size_t));
		memcpy(&command, NLMSG_DATA(nlh), sizeof(size_t));
               printf("Get a command from PIPE: %d\n", command);
	
		switch (command)
		{       
			case COMPRESS:
		{
                          
                             printf("Get compress request!\n");
			     
				//compress_block(pipe_from_kernel,alg);
				//if(alg==LZ4)
				
			//	compress_block_with_lz4(NLMSG_DATA(nlh) + sizeof(size_t));
				
				//if(alg==ZLIB)
				
				    // compress_block_with_zlib(NLMSG_DATA(nlh) + sizeof(size_t));
				
				//if(alg==QUICKLZ)
				compress_block_with_quicklz(NLMSG_DATA(nlh) + sizeof(size_t));
                                logger.write(&logger, "Get command: COMPRESS");
				break; 
                       
		};
			case DECOMPRESS:
			{       
			        printf("Get decompress request!\n");
				decompress_block_with_lz4(NLMSG_DATA(nlh) + sizeof(size_t));
				//decompress_block_with_zlib(NLMSG_DATA(nlh) + sizeof(size_t));
				//decompress_block_with_quicklz(NLMSG_DATA(nlh) + sizeof(size_t));
                                logger.write(&logger, "Get command: DECOMPRESS");
				break;
			};
			case STOP:
			{
				printf("Stop daemon!\n");
				break;
			};
		}
	}

	free(nlh);
	free(compress_buff);
   close(socket_descriptor);
}
/*static void compress_block(FILE *pipe_from_kernel)
{
/*
	FILE *pipe_to_kernel;
	size_t compressed_size = 0;   
	printf("breakpoint compress 1\n");
	// Получаем не сжатый блок
	fread(uncompress_buff, 1, KERNEL_SECTOR_SIZE, pipe_from_kernel);
      //compress_block_with_zlib(uncompress_buff);
	printf("breakpoint compress 2\n");
	// Производим компрессию блока
	compressed_size = LZ4_compress(uncompress_buff, compress_buff, KERNEL_SECTOR_SIZE);
	printf("breakpoint compress 3\n");
	if (compressed_size == 0)
	{
		logger.write(&logger, "Failed compress block!");
		fclose(pipe_from_kernel);
		return;
	}	
	//printf("size after compress in daemon: %d\n", compressed_size);
	printf("breakpoint compress 4\n");
	// Закрываем пайп на чтение и открываем на запись	
	fclose(pipe_from_kernel);
	printf("breakpoint compress 5\n");
	pipe_to_kernel = fopen(CBDA_TO_KERNEL, "wb");
	printf("breakpoint compress 6\n");
	// Записываем размер сжатого блока и сам блок и закрываем пайп на запись
	fwrite((const char*)&compressed_size, sizeof(size_t), 1, pipe_to_kernel);
	printf("breakpoint compress 7\n");
	fwrite(compress_buff, 1, compressed_size, pipe_to_kernel);
	printf("breakpoint compress 8\n");
	fclose(pipe_to_kernel);
	printf("breakpoint compress 9\n");

}

static void decompress_block(FILE *pipe_from_kernel)
{

	FILE *pipe_to_kernel;
	size_t compressed_size = 0;
	int decompressed = 0;
	printf("breakpoint decompress 1\n");
	// Получаем размер передаваемого сжатого блока и сам блок
   fread((char*)&compressed_size, sizeof(size_t), 1, pipe_from_kernel);
	printf("breakpoint decompress 2\n");
	fread(compress_buff, 1, compressed_size, pipe_from_kernel);
	printf("breakpoint decompress 3\n");

	// Производим декомпрессию	
	decompressed = LZ4_decompress_fast(compress_buff, uncompress_buff, KERNEL_SECTOR_SIZE);
	if (decompressed != compressed_size)
	{
		logger.write(&logger, "Failed decompress block!");
		fclose(pipe_from_kernel);
		return;
	}
	printf("breakpoint decompress 4\n");
	// Закрываем пайп на чтение и открываем на запись
	fclose(pipe_from_kernel);
	printf("breakpoint decompress 5\n");
	pipe_to_kernel = fopen(CBDA_TO_KERNEL, "wb");
	printf("breakpoint decompress 6\n");
	// Записываем разжатый блок и закрываем пайп на запись
	fwrite(uncompress_buff, 1, KERNEL_SECTOR_SIZE, pipe_to_kernel);
	printf("breakpoint decompress 7\n");
	fclose(pipe_to_kernel);
	printf("breakpoint decompress 8\n");
}

}*/
