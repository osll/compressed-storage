#include <sys/stat.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <asm/types.h>
#include <sys/socket.h>
#include <linux/netlink.h>

#include "../common/daemon_param.h"


static struct sockaddr_nl src_addr;
static struct sockaddr_nl dest_addr;
static struct nlmsghdr* nlh;
static struct iovec iov;
static struct msghdr msg;
static int sock_fd;

static void send_pid_to_kernel(void)
{
	FILE *pipe_file;
	int pid = getpid();

	mkfifo(CBDA_FROM_DAEMON, S_IRUSR| S_IWUSR);
   pipe_file = fopen(CBDA_FROM_DAEMON, "wb");	
	fwrite((const char*)&pid, sizeof(int), 1, pipe_file); 
	fclose(pipe_file);
}

static int initial(void)
{
	// Инициализация сокета
	sock_fd = socket(PF_NETLINK, SOCK_RAW, NETLINK_USER);
   if (sock_fd < 0)
	{
		printf("Failed to open socket!\n");
   	return -1;
	}
   memset(&src_addr, 0, sizeof(src_addr));
   src_addr.nl_family = AF_NETLINK;
   src_addr.nl_pid = getpid();

   bind(sock_fd, (struct sockaddr*)&src_addr, sizeof(src_addr));

   memset(&dest_addr, 0, sizeof(dest_addr));
   memset(&dest_addr, 0, sizeof(dest_addr));
   dest_addr.nl_family = AF_NETLINK;
   dest_addr.nl_pid = 0; 
   dest_addr.nl_groups = 0;

   nlh = (struct nlmsghdr*)malloc(NLMSG_SPACE(512));
   memset(nlh, 0, NLMSG_SPACE(512));
   nlh->nlmsg_len = NLMSG_SPACE(512);
   nlh->nlmsg_pid = getpid();
   nlh->nlmsg_flags = 0;

   iov.iov_base = (void *)nlh;
   iov.iov_len = nlh->nlmsg_len;
   msg.msg_name = (void *)&dest_addr;
   msg.msg_namelen = sizeof(dest_addr);
   msg.msg_iov = &iov;
   msg.msg_iovlen = 1;
	return 0;
}

int main(int argc, char *argv[])
{
	if (initial() < 0)
	{
		return -1;
	}
	send_pid_to_kernel();

	recvmsg(sock_fd, &msg, 0);
	printf("coming: %s\n", NLMSG_DATA(nlh));

   return 0;
}
