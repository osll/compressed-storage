#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <linux/stat.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <linux/netlink.h>

#include "../user_compression/lz4.h"
#include "../user_compression/lz4.c"
#include "../common/daemon_param.h"
#include "logger.c"

#define NETLINK_USER 26
#define MAX_PAYLOAD 1024

static struct sockaddr_nl src_addr;
static struct sockaddr_nl dest_addr;
static struct nlmsghdr* nlh;
static struct iovec iov;
static struct msghdr msg;
static int sock_fd;

static char* compress_buff;
static char* uncompress_buff;

static struct LOG logger;

/**
 * Посылает сообщение в модуль ядра
 */
static void send_to_kernel(char* data, size_t length)
{
	//memset(NLMSG_DATA(nlh), 0, NLMSG_SPACE(MAX_PAYLOAD));
   memcpy(NLMSG_DATA(nlh), data, length);
   sendmsg(sock_fd, &msg, 0);
}

/**
 * Получает сообщение из модуля ядра
 */
static void get_from_kernel(char* data, size_t* length)
{	
	int i = 0;
	recvmsg(sock_fd, &msg, 0);
	char buff[100] = {'\0'};
	//sscanf((const char*)NLMSG_DATA(nlh), "%s %s %s", &buff, &buff, &buff);
	memcpy(length, NLMSG_DATA(nlh), sizeof(size_t));
	memcpy(buff, NLMSG_DATA(nlh) + sizeof(size_t), 100);

	for (; i < 20; ++i)
	{
		printf("sym: |%c|\n", buff[i]);
	}

	printf("length: %d %s\n", *length, buff);
	
	
//	printf("data_1: %s\n", (const char*)NLMSG_DATA(nlh));
}

static int initial()
{
	// Инициализация логгера
	LOG_init(&logger, "/tmp/cbda_demon_log");

	// Инициализация сокета
	sock_fd = socket(PF_NETLINK, SOCK_RAW, NETLINK_USER);
   if (sock_fd < 0)
	{
		printf("Failed to open socket!\n");
		logger.write(&logger, "Failed to open socket!\n");
   	return -1;
	}
   memset(&src_addr, 0, sizeof(src_addr));
   src_addr.nl_family = AF_NETLINK;
   src_addr.nl_pid = getpid();

   bind(sock_fd, (struct sockaddr*)&src_addr, sizeof(src_addr));

   memset(&dest_addr, 0, sizeof(dest_addr));
   memset(&dest_addr, 0, sizeof(dest_addr));
   dest_addr.nl_family = AF_NETLINK;
   dest_addr.nl_pid = 0; 
   dest_addr.nl_groups = 0;

   nlh = (struct nlmsghdr*)malloc(NLMSG_SPACE(MAX_PAYLOAD));
   memset(nlh, 0, NLMSG_SPACE(MAX_PAYLOAD));
   nlh->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD);
   nlh->nlmsg_pid = getpid();
   nlh->nlmsg_flags = 0;

   iov.iov_base = (void *)nlh;
   iov.iov_len = nlh->nlmsg_len;
   msg.msg_name = (void *)&dest_addr;
   msg.msg_namelen = sizeof(dest_addr);
   msg.msg_iov = &iov;
   msg.msg_iovlen = 1;
	return 0;
}

int main(void)
{	
	size_t command = -1;
	size_t size_answer = 0;
	char buff[10] = {'\0'};
	char answer[100] = {'\0'};
	sprintf(buff, "hello!");	
	if (initial() != 0)
	{
		return -1;
	}

	send_to_kernel(buff, strlen(buff));
	get_from_kernel(answer, &size_answer);
	printf("answer: %s\n", answer);
   return 0;
}

/**
 * Производит компрессию блока, полученного через пайп
 * Возвращает сжатый блок в тот же пайп в следующем формате: size_t (размер сжатого блока) char* (данные)
 *
 * @param pipe_from_kernel - именованный канал, через который передаются данные
 */
static void compress_block(FILE *pipe_from_kernel)
{
/*
	FILE *pipe_to_kernel;
	size_t compressed_size = 0;   
	printf("breakpoint compress 1\n");
	// Получаем не сжатый блок
	fread(uncompress_buff, 1, KERNEL_SECTOR_SIZE, pipe_from_kernel);
	printf("breakpoint compress 2\n");
	// Производим компрессию блока
	compressed_size = LZ4_compress(uncompress_buff, compress_buff, KERNEL_SECTOR_SIZE);
	printf("breakpoint compress 3\n");
	if (compressed_size == 0)
	{
		logger.write(&logger, "Failed compress block!");
		fclose(pipe_from_kernel);
		return;
	}	
	//printf("size after compress in daemon: %d\n", compressed_size);
	printf("breakpoint compress 4\n");
	// Закрываем пайп на чтение и открываем на запись	
	fclose(pipe_from_kernel);
	printf("breakpoint compress 5\n");
	pipe_to_kernel = fopen(CBDA_TO_KERNEL, "wb");
	printf("breakpoint compress 6\n");
	// Записываем размер сжатого блока и сам блок и закрываем пайп на запись
	fwrite((const char*)&compressed_size, sizeof(size_t), 1, pipe_to_kernel);
	printf("breakpoint compress 7\n");
	fwrite(compress_buff, 1, compressed_size, pipe_to_kernel);
	printf("breakpoint compress 8\n");
	fclose(pipe_to_kernel);
	printf("breakpoint compress 9\n");
*/
}

/**
 * Производит декомпрессию блока, полученного через PIPE
 *
 * @param pipe_from_kernel - именованный канал, через который передаются данные
 */
static void decompress_block(FILE *pipe_from_kernel)
{
/*
	FILE *pipe_to_kernel;
	size_t compressed_size = 0;
	int decompressed = 0;
	printf("breakpoint decompress 1\n");
	// Получаем размер передаваемого сжатого блока и сам блок
   fread((char*)&compressed_size, sizeof(size_t), 1, pipe_from_kernel);
	printf("breakpoint decompress 2\n");
	fread(compress_buff, 1, compressed_size, pipe_from_kernel);
	printf("breakpoint decompress 3\n");

	// Производим декомпрессию	
	decompressed = LZ4_decompress_fast(compress_buff, uncompress_buff, KERNEL_SECTOR_SIZE);
	if (decompressed != compressed_size)
	{
		logger.write(&logger, "Failed decompress block!");
		fclose(pipe_from_kernel);
		return;
	}
	printf("breakpoint decompress 4\n");
	// Закрываем пайп на чтение и открываем на запись
	fclose(pipe_from_kernel);
	printf("breakpoint decompress 5\n");
	pipe_to_kernel = fopen(CBDA_TO_KERNEL, "wb");
	printf("breakpoint decompress 6\n");
	// Записываем разжатый блок и закрываем пайп на запись
	fwrite(uncompress_buff, 1, KERNEL_SECTOR_SIZE, pipe_to_kernel);
	printf("breakpoint decompress 7\n");
	fclose(pipe_to_kernel);
	printf("breakpoint decompress 8\n");
}

*/
}

/*				

	size_t command = -1;
	FILE *pipe_from_kernel;	

 	uncompress_buff = malloc(LZ4_compressBound(KERNEL_SECTOR_SIZE)); 
 	compress_buff = malloc(LZ4_compressBound(KERNEL_SECTOR_SIZE)); 
  
	mkfifo(CBDA_FROM_KERNEL, S_IRUSR| S_IWUSR);
	mkfifo(CBDA_TO_KERNEL, S_IRUSR| S_IWUSR);

   while(command)
   {
		printf("waiting command\n");
   	pipe_from_kernel = fopen(CBDA_FROM_KERNEL, "rb");
     	printf("something coming\n");
		if(fread(&command, sizeof(size_t), 1, pipe_from_kernel) != 1)
		{
			printf("Can't get a command from PIPE\n");
			logger.write(&logger, "Can't get a command from PIPE!");
		}
		printf("Get a command from PIPE: %d\n", command);
		switch (command)
		{
			case COMPRESS:
			{
				logger.write(&logger, "Get command: COMPRESS");
				compress_block(pipe_from_kernel);
				break;
			};
			case DECOMPRESS:
			{
				logger.write(&logger, "Get command: DECOMPRESS");
				decompress_block(pipe_from_kernel);
				break;
			};
			case STOP:
			{
				logger.write(&logger, "Get command: STOP");
				fclose(pipe_from_kernel);
				break;
			};
		}
   }

	logger.close(&logger);
	free(compress_buff);
	free(uncompress_buff);
   return 0;
}*/
