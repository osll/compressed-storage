#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/lz4.h>
#include <asm/unaligned.h>
#include "lz4defs.h"

/*
 * LZ4_compressCtx :
 * -----------------
 * Compress 'isize' bytes from 'source' into an output buffer 'dest' of
 * maximum size 'maxOutputSize'.  * If it cannot achieve it, compression
 * will stop, and result of the function will be zero.
 * return : the number of bytes written in buffer 'dest', or 0 if the
 * compression fails
 */
static inline int lz4_compressctx(void *ctx,
		const char *source,
		char *dest,
		int isize,
		int maxoutputsize)
{
	HTYPE *hashtable = (HTYPE *)ctx;
	const u8 *ip = (u8 *)source;
#if LZ4_ARCH64
	const BYTE * const base = ip;
#else
	const int base = 0;
#endif
	const u8 *anchor = ip;
	const u8 *const iend = ip + isize;
	const u8 *const mflimit = iend - MFLIMIT;
	#define MATCHLIMIT (iend - LASTLITERALS)

	u8 *op = (u8 *) dest;
	u8 *const oend = op + maxoutputsize;
	int length;
	const int skipstrength = SKIPSTRENGTH;
	u32 forwardh;
	int lastrun;

	/* Init */
	if (isize < MINLENGTH)
		goto _last_literals;

	memset((void *)hashtable, 0, LZ4_MEM_COMPRESS);

	/* First Byte */
	hashtable[LZ4_HASH_VALUE(ip)] = ip - base;
	ip++;
	forwardh = LZ4_HASH_VALUE(ip);

	/* Main Loop */
	for (;;) {
		int findmatchattempts = (1U << skipstrength) + 3;
		const u8 *forwardip = ip;
		const u8 *ref;
		u8 *token;

		/* Find a match */
		do {
			u32 h = forwardh;
			int step = findmatchattempts++ >> skipstrength;
			ip = forwardip;
			forwardip = ip + step;

			if (unlikely(forwardip > mflimit))
				goto _last_literals;

			forwardh = LZ4_HASH_VALUE(forwardip);
			ref = base + hashtable[h];
			hashtable[h] = ip - base;
		} while ((ref < ip - MAX_DISTANCE) || (A32(ref) != A32(ip)));

		/* Catch up */
		while ((ip > anchor) && (ref > (u8 *)source) &&
			unlikely(ip[-1] == ref[-1])) {
			ip--;
			ref--;
		}

		/* Encode Literal length */
		length = (int)(ip - anchor);
		token = op++;
		/* check output limit */
		if (unlikely(op + length + (2 + 1 + LASTLITERALS) +
			(length >> 8) > oend))
			return 0;

		if (length >= (int)RUN_MASK) {
			int len;
			*token = (RUN_MASK << ML_BITS);
			len = length - RUN_MASK;
			for (; len > 254 ; len -= 255)
				*op++ = 255;
			*op++ = (u8)len;
		} else
			*token = (length << ML_BITS);

		/* Copy Literals */
		LZ4_BLINDCOPY(anchor, op, length);
_next_match:
		/* Encode Offset */
		LZ4_WRITE_LITTLEENDIAN_16(op, (u16)(ip - ref));

		/* Start Counting */
		ip += MINMATCH;
		/* MinMatch verified */
		ref += MINMATCH;
		anchor = ip;
		while (likely(ip < MATCHLIMIT - (STEPSIZE - 1))) {
			#if LZ4_ARCH64
			u64 diff = A64(ref) ^ A64(ip);
			#else
			u32 diff = A32(ref) ^ A32(ip);
			#endif
			if (!diff) {
				ip += STEPSIZE;
				ref += STEPSIZE;
				continue;
			}
			ip += LZ4_NBCOMMONBYTES(diff);
			goto _endcount;
		}
		#if LZ4_ARCH64
		if ((ip < (MATCHLIMIT - 3)) && (A32(ref) == A32(ip))) {
			ip += 4;
			ref += 4;
		}
		#endif
		if ((ip < (MATCHLIMIT - 1)) && (A16(ref) == A16(ip))) {
			ip += 2;
			ref += 2;
		}
		if ((ip < MATCHLIMIT) && (*ref == *ip))
			ip++;
_endcount:
		/* Encode MatchLength */
		length = (int)(ip - anchor);
		/* Check output limit */
		if (unlikely(op + (1 + LASTLITERALS) + (length >> 8) > oend))
			return 0;
		if (length >= (int)ML_MASK) {
			*token += ML_MASK;
			length -= ML_MASK;
			for (; length > 509 ; length -= 510) {
				*op++ = 255;
				*op++ = 255;
			}
			if (length > 254) {
				length -= 255;
				*op++ = 255;
			}
			*op++ = (u8)length;
		} else
			*token += length;

		/* Test end of chunk */
		if (ip > mflimit) {
			anchor = ip;
			break;
		}

		/* Fill table */
		hashtable[LZ4_HASH_VALUE(ip-2)] = ip - 2 - base;

		/* Test next position */
		ref = base + hashtable[LZ4_HASH_VALUE(ip)];
		hashtable[LZ4_HASH_VALUE(ip)] = ip - base;
		if ((ref > ip - (MAX_DISTANCE + 1)) && (A32(ref) == A32(ip))) {
			token = op++;
			*token = 0;
			goto _next_match;
		}

		/* Prepare next loop */
		anchor = ip++;
		forwardh = LZ4_HASH_VALUE(ip);
	}

_last_literals:
	/* Encode Last Literals */
	lastrun = (int)(iend - anchor);
	if (((char *)op - dest) + lastrun + 1
		+ ((lastrun + 255 - RUN_MASK) / 255) > (u32)maxoutputsize)
		return 0;

	if (lastrun >= (int)RUN_MASK) {
		*op++ = (RUN_MASK << ML_BITS);
		lastrun -= RUN_MASK;
		for (; lastrun > 254 ; lastrun -= 255)
			*op++ = 255;
		*op++ = (u8)lastrun;
	} else
		*op++ = (lastrun << ML_BITS);
	memcpy(op, anchor, iend - anchor);
	op += iend - anchor;

	/* End */
	return (int)(((char *)op) - dest);
}

static inline int lz4_compress64kctx(void *ctx,
		const char *source,
		char *dest,
		int isize,
		int maxoutputsize)
{
	u16 *hashtable = (u16 *)ctx;
	const u8 *ip = (u8 *) source;
	const u8 *anchor = ip;
	const u8 *const base = ip;
	const u8 *const iend = ip + isize;
	const u8 *const mflimit = iend - MFLIMIT;
	#define MATCHLIMIT (iend - LASTLITERALS)

	u8 *op = (u8 *) dest;
	u8 *const oend = op + maxoutputsize;
	int len, length;
	const int skipstrength = SKIPSTRENGTH;
	u32 forwardh;
	int lastrun;

	/* Init */
	if (isize < MINLENGTH)
		goto _last_literals;

	memset((void *)hashtable, 0, LZ4_MEM_COMPRESS);

	/* First Byte */
	ip++;
	forwardh = LZ4_HASH64K_VALUE(ip);

	/* Main Loop */
	for (;;) {
		int findmatchattempts = (1U << skipstrength) + 3;
		const u8 *forwardip = ip;
		const u8 *ref;
		u8 *token;

		/* Find a match */
		do {
			u32 h = forwardh;
			int step = findmatchattempts++ >> skipstrength;
			ip = forwardip;
			forwardip = ip + step;

			if (forwardip > mflimit)
				goto _last_literals;

			forwardh = LZ4_HASH64K_VALUE(forwardip);
			ref = base + hashtable[h];
			hashtable[h] = (u16)(ip - base);
		} while (A32(ref) != A32(ip));

		/* Catch up */
		while ((ip > anchor) && (ref > (u8 *)source)
			&& (ip[-1] == ref[-1])) {
			ip--;
			ref--;
		}

		/* Encode Literal length */
		length = (int)(ip - anchor);
		token = op++;
		/* Check output limit */
		if (unlikely(op + length + (2 + 1 + LASTLITERALS)
			+ (length >> 8) > oend))
			return 0;
		if (length >= (int)RUN_MASK) {
			*token = (RUN_MASK << ML_BITS);
			len = length - RUN_MASK;
			for (; len > 254 ; len -= 255)
				*op++ = 255;
			*op++ = (u8)len;
		} else
			*token = (length << ML_BITS);

		/* Copy Literals */
		LZ4_BLINDCOPY(anchor, op, length);

_next_match:
		/* Encode Offset */
		LZ4_WRITE_LITTLEENDIAN_16(op, (u16)(ip - ref));

		/* Start Counting */
		ip += MINMATCH;
		/* MinMatch verified */
		ref += MINMATCH;
		anchor = ip;

		while (ip < MATCHLIMIT - (STEPSIZE - 1)) {
			#if LZ4_ARCH64
			u64 diff = A64(ref) ^ A64(ip);
			#else
			u32 diff = A32(ref) ^ A32(ip);
			#endif

			if (!diff) {
				ip += STEPSIZE;
				ref += STEPSIZE;
				continue;
			}
			ip += LZ4_NBCOMMONBYTES(diff);
			goto _endcount;
		}
		#if LZ4_ARCH64
		if ((ip < (MATCHLIMIT - 3)) && (A32(ref) == A32(ip))) {
			ip += 4;
			ref += 4;
		}
		#endif
		if ((ip < (MATCHLIMIT - 1)) && (A16(ref) == A16(ip))) {
			ip += 2;
			ref += 2;
		}
		if ((ip < MATCHLIMIT) && (*ref == *ip))
			ip++;
_endcount:

		/* Encode MatchLength */
		len = (int)(ip - anchor);
		/* Check output limit */
		if (unlikely(op + (1 + LASTLITERALS) + (len >> 8) > oend))
			return 0;
		if (len >= (int)ML_MASK) {
			*token += ML_MASK;
			len -= ML_MASK;
			for (; len > 509 ; len -= 510) {
				*op++ = 255;
				*op++ = 255;
			}
			if (len > 254) {
				len -= 255;
				*op++ = 255;
			}
			*op++ = (u8)len;
		} else
			*token += len;

		/* Test end of chunk */
		if (ip > mflimit) {
			anchor = ip;
			break;
		}

		/* Fill table */
		hashtable[LZ4_HASH64K_VALUE(ip-2)] = (u16)(ip - 2 - base);

		/* Test next position */
		ref = base + hashtable[LZ4_HASH64K_VALUE(ip)];
		hashtable[LZ4_HASH64K_VALUE(ip)] = (u16)(ip - base);
		if (A32(ref) == A32(ip)) {
			token = op++;
			*token = 0;
			goto _next_match;
		}

		/* Prepare next loop */
		anchor = ip++;
		forwardh = LZ4_HASH64K_VALUE(ip);
	}

_last_literals:
	/* Encode Last Literals */
	lastrun = (int)(iend - anchor);
	if (op + lastrun + 1 + (lastrun - RUN_MASK + 255) / 255 > oend)
		return 0;
	if (lastrun >= (int)RUN_MASK) {
		*op++ = (RUN_MASK << ML_BITS);
		lastrun -= RUN_MASK;
		for (; lastrun > 254 ; lastrun -= 255)
			*op++ = 255;
		*op++ = (u8)lastrun;
	} else
		*op++ = (lastrun << ML_BITS);
	memcpy(op, anchor, iend - anchor);
	op += iend - anchor;
	/* End */
	return (int)(((char *)op) - dest);
}

int lz4_compress(const unsigned char *src, size_t src_len,
			unsigned char *dst, size_t *dst_len, void *wrkmem)
{
	int ret = -1;
	int out_len = 0;

	if (src_len < LZ4_64KLIMIT)
		out_len = lz4_compress64kctx(wrkmem, src, dst, src_len,
				lz4_compressbound(src_len));
	else
		out_len = lz4_compressctx(wrkmem, src, dst, src_len,
				lz4_compressbound(src_len));

	if (out_len < 0)
		goto exit;

	*dst_len = out_len;

	return 0;
exit:
	return ret;
}
EXPORT_SYMBOL(lz4_compress);

static int lz4_uncompress(const char *source, char *dest, int osize)
{
	const BYTE *ip = (const BYTE *) source;
	const BYTE *ref;
	BYTE *op = (BYTE *) dest;
	BYTE * const oend = op + osize;
	BYTE *cpy;
	unsigned token;
	size_t length;
	size_t dec32table[] = {0, 3, 2, 3, 0, 0, 0, 0};
#if LZ4_ARCH64
	size_t dec64table[] = {0, 0, 0, -1, 0, 1, 2, 3};
#endif

	while (1) {

		/* get runlength */
		token = *ip++;
		length = (token >> ML_BITS);
		if (length == RUN_MASK) {
			size_t len;

			len = *ip++;
			for (; len == 255; length += 255)
				len = *ip++;
			if (unlikely(length > (size_t)(length + len)))
				goto _output_error;
			length += len;
		}

		/* copy literals */
		cpy = op + length;
		if (unlikely(cpy > oend - COPYLENGTH)) {
			/*
			 * Error: not enough place for another match
			 * (min 4) + 5 literals
			 */
			if (cpy != oend)
				goto _output_error;

			memcpy(op, ip, length);
			ip += length;
			break; /* EOF */
		}
		LZ4_WILDCOPY(ip, op, cpy);
		ip -= (op - cpy);
		op = cpy;

		/* get offset */
		LZ4_READ_LITTLEENDIAN_16(ref, cpy, ip);
		ip += 2;

		/* Error: offset create reference outside destination buffer */
		if (unlikely(ref < (BYTE *const) dest))
			goto _output_error;

		/* get matchlength */
		length = token & ML_MASK;
		if (length == ML_MASK) {
			for (; *ip == 255; length += 255)
				ip++;
			if (unlikely(length > (size_t)(length + *ip)))
				goto _output_error;
			length += *ip++;
		}

		/* copy repeated sequence */
		if (unlikely((op - ref) < STEPSIZE)) {
#if LZ4_ARCH64
			size_t dec64 = dec64table[op - ref];
#else
			const int dec64 = 0;
#endif
			op[0] = ref[0];
			op[1] = ref[1];
			op[2] = ref[2];
			op[3] = ref[3];
			op += 4;
			ref += 4;
			ref -= dec32table[op-ref];
			PUT4(ref, op);
			op += STEPSIZE - 4;
			ref -= dec64;
		} else {
			LZ4_COPYSTEP(ref, op);
		}
		cpy = op + length - (STEPSIZE - 4);
		if (cpy > (oend - COPYLENGTH)) {

			/* Error: request to write beyond destination buffer */
			if (cpy > oend)
				goto _output_error;
			LZ4_SECURECOPY(ref, op, (oend - COPYLENGTH));
			while (op < cpy)
				*op++ = *ref++;
			op = cpy;
			/*
			 * Check EOF (should never happen, since last 5 bytes
			 * are supposed to be literals)
			 */
			if (op == oend)
				goto _output_error;
			continue;
		}
		LZ4_SECURECOPY(ref, op, cpy);
		op = cpy; /* correction */
	}
	/* end of decoding */
	return (int) (((char *)ip) - source);

	/* write overflow error detected */
_output_error:
	return -1;
}

static int lz4_uncompress_unknownoutputsize(const char *source, char *dest,
				int isize, size_t maxoutputsize)
{
	const BYTE *ip = (const BYTE *) source;
	const BYTE *const iend = ip + isize;
	const BYTE *ref;


	BYTE *op = (BYTE *) dest;
	BYTE * const oend = op + maxoutputsize;
	BYTE *cpy;

	size_t dec32table[] = {0, 3, 2, 3, 0, 0, 0, 0};
#if LZ4_ARCH64
	size_t dec64table[] = {0, 0, 0, -1, 0, 1, 2, 3};
#endif

	/* Main Loop */
	while (ip < iend) {

		unsigned token;
		size_t length;

		/* get runlength */
		token = *ip++;
		length = (token >> ML_BITS);
		if (length == RUN_MASK) {
			int s = 255;
			while ((ip < iend) && (s == 255)) {
				s = *ip++;
				if (unlikely(length > (size_t)(length + s)))
					goto _output_error;
				length += s;
			}
		}
		/* copy literals */
		cpy = op + length;
		if ((cpy > oend - COPYLENGTH) ||
			(ip + length > iend - COPYLENGTH)) {

			if (cpy > oend)
				goto _output_error;/* writes beyond buffer */

			if (ip + length != iend)
				goto _output_error;/*
						    * Error: LZ4 format requires
						    * to consume all input
						    * at this stage
						    */
			memcpy(op, ip, length);
			op += length;
			break;/* Necessarily EOF, due to parsing restrictions */
		}
		LZ4_WILDCOPY(ip, op, cpy);
		ip -= (op - cpy);
		op = cpy;

		/* get offset */
		LZ4_READ_LITTLEENDIAN_16(ref, cpy, ip);
		ip += 2;
		if (ref < (BYTE * const) dest)
			goto _output_error;
			/*
			 * Error : offset creates reference
			 * outside of destination buffer
			 */

		/* get matchlength */
		length = (token & ML_MASK);
		if (length == ML_MASK) {
			while (ip < iend) {
				int s = *ip++;
				if (unlikely(length > (size_t)(length + s)))
					goto _output_error;
				length += s;
				if (s == 255)
					continue;
				break;
			}
		}

		/* copy repeated sequence */
		if (unlikely((op - ref) < STEPSIZE)) {
#if LZ4_ARCH64
			size_t dec64 = dec64table[op - ref];
#else
			const int dec64 = 0;
#endif
				op[0] = ref[0];
				op[1] = ref[1];
				op[2] = ref[2];
				op[3] = ref[3];
				op += 4;
				ref += 4;
				ref -= dec32table[op - ref];
				PUT4(ref, op);
				op += STEPSIZE - 4;
				ref -= dec64;
		} else {
			LZ4_COPYSTEP(ref, op);
		}
		cpy = op + length - (STEPSIZE-4);
		if (cpy > oend - COPYLENGTH) {
			if (cpy > oend)
				goto _output_error; /* write outside of buf */

			LZ4_SECURECOPY(ref, op, (oend - COPYLENGTH));
			while (op < cpy)
				*op++ = *ref++;
			op = cpy;
			/*
			 * Check EOF (should never happen, since last 5 bytes
			 * are supposed to be literals)
			 */
			if (op == oend)
				goto _output_error;
			continue;
		}
		LZ4_SECURECOPY(ref, op, cpy);
		op = cpy; /* correction */
	}
	/* end of decoding */
	return (int) (((char *) op) - dest);

	/* write overflow error detected */
_output_error:
	return -1;
}

int lz4_decompress(const unsigned char *src, size_t *src_len,
		unsigned char *dest, size_t actual_dest_len)
{
	int ret = -1;
	int input_len = 0;

	input_len = lz4_uncompress(src, dest, actual_dest_len);
	if (input_len < 0)
		goto exit_0;
	*src_len = input_len;

	return 0;
exit_0:
	return ret;
}
//EXPORT_SYMBOL(lz4_decompress);

int lz4_decompress_unknownoutputsize(const unsigned char *src, size_t src_len,
		unsigned char *dest, size_t *dest_len)
{
	int ret = -1;
	int out_len = 0;

	out_len = lz4_uncompress_unknownoutputsize(src, dest, src_len,
					*dest_len);
	if (out_len < 0)
		goto exit_0;
	*dest_len = out_len;

	return 0;
exit_0:
	return ret;
}
//EXPORT_SYMBOL(lz4_decompress_unknownoutputsize);
