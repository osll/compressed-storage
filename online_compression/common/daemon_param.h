#define FIFO_FILE   "/tmp/cbda_fifo"
#define CBDA_FROM_DAEMON   "/tmp/cbda_from_daemon"
#define CBDA_TO_KERNEL   "/tmp/cbda_to_kernel"
#define CBDA_FROM_KERNEL   "/tmp/cbda_from_kernel"
#define KERNEL_SECTOR_SIZE 512
#define COMPRESS 1
#define DECOMPRESS 2
#define INIT 3
#define GET_CAPACITY 4
#define STOP 0
#define ZLIB 1
#define LZ4 2
#define QUICKLZ 3
#define NETLINK_USER 30
#define LZ4_COMPRESS(block, compress_buff) LZ4_compress(block, compress_buff + sizeof(size_t) * 2, KERNEL_SECTOR_SIZE); 
#define LZ4_DECOMPRESS(block, compress_buff) LZ4_decompress_safe(block, compress_buff + sizeof(size_t) * 2, KERNEL_SECTOR_SIZE,0); 
#define ZLIB_COMPRESS(defstream)  deflateInit(&defstream, Z_BEST_COMPRESSION);deflate(&defstream, Z_FINISH);deflateEnd(&defstream); 
#define ZLIB_DECOMPRESS(infstream)  inflateInit(&infstream);inflate(&infstream, Z_NO_FLUSH);inflateEnd(&infstream);
#define QLZ_COMPRESS(block,decompressed,size_in,state_decompress)   qlz_compress(block, compressed, size_in, state_compress);
#define QLZ_DECOMPRESS(block,decompressed,state_decompress)   qlz_decompress(block, decompressed, state_decompress);
#define CAPACITY_ANWSER_SIZE sizeof(size_t)*6
#define COMPRESS_REQUEST_SIZE sizeof(size_t) + KERNEL_SECTOR_SIZE
