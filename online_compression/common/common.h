#include <linux/module.h>
#include <linux/vmalloc.h>
#include <linux/blkdev.h>
#include <linux/genhd.h>
#include <linux/errno.h>
#include <linux/hdreg.h>
#include <linux/version.h>
#include <net/sock.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>

// Можно использовать собственный размер диска (кратный 2), но рекомендуется использовать именно 512б
#define KERNEL_SECTOR_SIZE    	512
#define MY_DEVICE_NAME 		"cbd"
#define DEV_MINORS		16
#define FILE_NAME_LEN 256

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,35)
#define blk_fs_request(rq)      ((rq)->cmd_type == REQ_TYPE_FS)
#endif

enum MODES 					// Допустимые значения для задания параметра "режим обработки запросов"
{	
   RM_SIMPLE  = 0,		// Использование очереди для обработки запросов ввода-вывода
   RM_FULL    = 1,		// Запрос с обработкой вектора BIO 
   RM_NOQUEUE = 2			// Прямое выполнение запроса без использования очереди 
};

static int diskmb = 4;
static int debug = 0;
static int major = 0;
static int num_devices = 1;
static int mode = RM_SIMPLE;

static int entire_count = -1;
static int halves_count = -1;
static int quarters_count = -1;
static char file_name[FILE_NAME_LEN];

module_param_string(file, file_name, FILE_NAME_LEN, 0);
module_param_named(size, diskmb, int, 0); 	// размер диска в Mb, по умолчанию - 4Mb
module_param(debug, int, 0);              	// уровень отладочных сообщений
module_param(major, int, 0);
module_param(num_devices, int, 0);
module_param(mode, int, 0);
module_param(entire_count, int, 0);
module_param(halves_count, int, 0);
module_param(quarters_count, int, 0);

#define ERR(...) printk( KERN_ERR "! "__VA_ARGS__ )
#define LOG(...) printk( KERN_INFO "+ "__VA_ARGS__ )
#define DBG(...) if( debug > 0 ) printk( KERN_DEBUG "# "__VA_ARGS__ )

