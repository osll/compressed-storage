#include "block.h"

CompressorLZ Block::s_compressor;

Block::Block(const QByteArray &data)
{
    Q_ASSERT(data.size()>1);
    setData(data);
}

const QByteArray &Block::getData() const
{
    return m_rawdata;
}

void Block::setData(const QByteArray &data)
{
    m_rawdata = data;
    m_compressed = s_compressor.compress(m_rawdata);
}

void Block::setCompressedData(const QByteArray &data)
{
    m_compressed = data;
    m_rawdata = s_compressor.decompress(m_compressed);

}

const QByteArray &Block::getCompressedData() const
{
    return m_compressed;
}

QDebug operator<<(QDebug &stream, const Block &block)
{
    stream << "< raw_data: " << block.m_rawdata.size() << ", compressed_data: "  << block.m_compressed.size();
    return stream;
}

