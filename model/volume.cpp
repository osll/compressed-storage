
#include <QSettings>
#include <QStringList>

#include <iostream>

#include "volume.h"
#include "utils.h"

Volume::Volume(PhyDevice* ptrPhyDevice)
{
    Q_ASSERT(ptrPhyDevice);

    m_ptrPhyDevice = ptrPhyDevice;

    QSettings settings(".config",QSettings::IniFormat);

    // TODO : set compressor;

    // TODO : что если этого места не хватит для записи служебной информации?
    m_cbdSystemInfoSize = settings.value("cbd.sys_info_size", 4096).toInt();
    m_cbdSystemInfoBlocksNumber = m_cbdSystemInfoSize / m_ptrPhyDevice->blockSize();
    if (m_cbdSystemInfoSize % m_ptrPhyDevice->blockSize() != 0) {
        ++m_cbdSystemInfoBlocksNumber;
    }

    QVector<int> dividers;
    QStringList poolDividersList = settings.value("cbd.pools","1 2").toString().split(" ");
    QString dividerValue;
    foreach(dividerValue, poolDividersList)
    dividers << dividerValue.toInt();
    if( dividers.size() == 0 ){
        dividers << 1;
    }

    int poolBlocksNumber = (m_ptrPhyDevice->blocksNumber() - m_cbdSystemInfoBlocksNumber)/dividers.size();
    for(int i = 0; i < dividers.size(); ++i){
        m_cbdPoolsMap.insert(dividers[i], CbdBlockPool(m_ptrPhyDevice, dividers[i], m_cbdSystemInfoBlocksNumber + i * poolBlocksNumber, poolBlocksNumber));
    }
}

bool Volume::initFromPhyDevice()
{
    size_t dataPtr = 0;
    QByteArray initData = m_ptrPhyDevice->read(0);
    size_t blocksNumber = Utils::getSize_t(initData, dataPtr);
    dataPtr += sizeof(size_t);
    if (blocksNumber == 0){
        qDebug() << "Couldn't init cbd from physical device";
        qDebug() << "New cbd will be created";
        return false;
    }
    Q_ASSERT(blocksNumber == m_cbdSystemInfoBlocksNumber);
    for(size_t i = 1; i < blocksNumber; ++i){
        initData.append(m_ptrPhyDevice->read(i));
    }

    size_t poolsNumber = Utils::getSize_t(initData, dataPtr);
    dataPtr += sizeof(size_t);
    Q_ASSERT(poolsNumber == (size_t) m_cbdPoolsMap.size());
    for (size_t i = 0; i < poolsNumber; ++i) {
        size_t poolDivider = Utils::getSize_t(initData, dataPtr);
        dataPtr += sizeof(size_t);
        Q_ASSERT(m_cbdPoolsMap.contains(poolDivider));
    }

    size_t mapSize = Utils::getSize_t(initData, dataPtr);
    dataPtr += sizeof(size_t);
    for (size_t i = 0; i < mapSize; ++i) {
        size_t blocknum = Utils::getSize_t(initData, dataPtr);
        dataPtr += sizeof(size_t);
        size_t poolDivider = Utils::getSize_t(initData, dataPtr);
        dataPtr += sizeof(size_t);
        size_t poolBlocknum = Utils::getSize_t(initData, dataPtr);
        dataPtr += sizeof(size_t);
        Q_ASSERT(m_cbdPoolsMap.contains(poolDivider));
        CbdBlockPointer ptr;
        ptr.m_cbdPoolDivider = poolDivider;
        ptr.m_poolBlockNumber = poolBlocknum;
        m_blocksMap[blocknum] = ptr;
        m_cbdPoolsMap[poolDivider].putAllocVirtualBlockNumber(poolBlocknum);
    }

    return true;
}

void Volume::saveToPhyDevice()
{
    QByteArray data;
    data.append((const char*) &m_cbdSystemInfoBlocksNumber, sizeof(size_t));

    size_t poolsSize = m_cbdPoolsMap.size();
    data.append((const char*) &poolsSize, sizeof(size_t));
    for (QMap<size_t,CbdBlockPool>::iterator it = m_cbdPoolsMap.begin(); it != m_cbdPoolsMap.end(); ++it) {
        size_t poolDivider =it.key();
        data.append((const char*) &poolDivider, sizeof(size_t));
    }

    size_t mapSize = m_blocksMap.size();
    data.append((const char*) &mapSize, sizeof(size_t));
    for (QMap<size_t,CbdBlockPointer>::iterator it = m_blocksMap.begin(); it != m_blocksMap.end(); ++it) {
        data.append((const char*) &(it.key()), sizeof(size_t));
        data.append((const char*) &(it.value().m_cbdPoolDivider), sizeof(size_t));
        data.append((const char*) &(it.value().m_poolBlockNumber), sizeof(size_t));
    }

    Q_ASSERT((size_t)data.size() <= m_cbdSystemInfoSize);

    for(size_t i = 0; i < m_cbdSystemInfoBlocksNumber; ++i){
        QByteArray outputData = data.mid(i * m_ptrPhyDevice->blockSize(), m_ptrPhyDevice->blockSize());
        if((size_t)outputData.size() < m_ptrPhyDevice->blockSize()) {
            outputData.append("0", m_ptrPhyDevice->blockSize() - (size_t)outputData.size());
        }
        m_ptrPhyDevice->write(i, outputData);
    }
}

size_t Volume::getVirtualBlocksNumber() const
{
    size_t count = 0;
    for (QMap<size_t,CbdBlockPool>::const_iterator it = m_cbdPoolsMap.begin(); it != m_cbdPoolsMap.end(); ++it) {
        count += it.value().virtualBlocksNumber();
    }
    return count;
}

size_t Volume::getPhysicalBlocksNumber() const
{
    return m_ptrPhyDevice->blocksNumber();
}

size_t Volume::getVirtualCapacity() const
{
    return getVirtualBlocksNumber() * m_ptrPhyDevice->blockSize();
}

size_t Volume::getPhysicalCapacity() const
{
    return getPhysicalBlocksNumber() * m_ptrPhyDevice->blockSize();
}

size_t Volume::getBlockSize() const
{
    return m_ptrPhyDevice->blockSize();
}

void Volume::write(size_t blocknum, const QByteArray &data)
{
    QByteArray compressedData = m_compressor.compress(data);

    // do not compress if comressed size > than physBlockSise/2
    if ((size_t) compressedData.size() > m_ptrPhyDevice->blockSize()/2){
        qDebug() << "data is not compressed";
        compressedData = data;
    }

    unmap(blocknum);
    map(blocknum,compressedData);
    writePool(blocknum,compressedData);

}

QByteArray Volume::read(size_t blocknum)
{
    Q_ASSERT(m_blocksMap.contains(blocknum) && m_blocksMap[blocknum].isValid());

    QByteArray data =  m_cbdPoolsMap[m_blocksMap[blocknum].m_cbdPoolDivider].read(m_blocksMap[blocknum].m_poolBlockNumber);
    if ((size_t) data.size() > m_ptrPhyDevice->blockSize()/2){
        return data;
    }
    return m_compressor.decompress(data);
}

bool Volume::map(size_t blocknum, const QByteArray& compressedData)
{
    size_t blk = (size_t)-1;
    CbdBlockPointer ptr;

    // pools in order of dividors increase
    QList<size_t> dividers = m_cbdPoolsMap.keys();
    for(int i = dividers.size() - 1; i >= 0; --i){
        if(m_cbdPoolsMap[dividers[i]].virtualBlockSize() >= (size_t)compressedData.size()) {
            blk = m_cbdPoolsMap[dividers[i]].getFreeVirtualBlockNumber();
            ptr.m_cbdPoolDivider = m_cbdPoolsMap[dividers[i]].poolDivider();
            ptr.m_poolBlockNumber = blk;
            m_blocksMap[blocknum] = ptr;
            break;
        }
    }
    return ptr.isValid();
}

bool Volume::unmap(size_t blocknum)
{
    if(m_blocksMap.contains(blocknum) && m_blocksMap[blocknum].isValid()) {
        m_cbdPoolsMap[m_blocksMap[blocknum].m_cbdPoolDivider].putFreeVirtualBlockNumber(m_blocksMap[blocknum].m_poolBlockNumber);
    }
    m_blocksMap[blocknum]=CbdBlockPointer();
    return true;
}

bool Volume::writePool(size_t blocknum, const QByteArray& compressedData)
{
    Q_ASSERT(m_blocksMap.contains(blocknum));
    if(m_blocksMap[blocknum].isValid()) {
        m_cbdPoolsMap[m_blocksMap[blocknum].m_cbdPoolDivider].write(m_blocksMap[blocknum].m_poolBlockNumber, compressedData);
    }
    return true;
}

QDebug& operator<<(QDebug& stream, const Volume& volume)
{
    stream << "(physical size (BLK):" << volume.getPhysicalBlocksNumber() << ", virtual size (BLK):" << volume.getVirtualBlocksNumber()
           << ", physical capacity = " << volume.getPhysicalCapacity() <<  ", virtual capacity = " << volume.getVirtualCapacity() <<  ")";

    size_t countAllocatedBlocks = 0;
    size_t countAllocatedSize = 0;
    for (QMap<size_t,CbdBlockPool>::const_iterator it = volume.m_cbdPoolsMap.begin(); it != volume.m_cbdPoolsMap.end(); ++it) {
        stream <<  "\n > " << it.value();
        size_t poolAllocatedBlocks = it.value().numberAllocatedBlocks();
        countAllocatedBlocks += poolAllocatedBlocks;
        countAllocatedSize += poolAllocatedBlocks * it.value().virtualBlockSize();
    }
    stream << "\nallocated blocks number = " << countAllocatedBlocks << ", allocated space size = " << countAllocatedSize
            << ", compression = " << (countAllocatedBlocks == 0 ? 0 : (1 - (double)countAllocatedSize / (countAllocatedBlocks * volume.getBlockSize())) * 100) << "%";

    return stream;
}
