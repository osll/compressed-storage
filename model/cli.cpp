
#include <QFile>
#include <QStringList>
#include <QTextStream>

#include <iostream>
#include <string>

#include "cli.h"
#include "compressormpeg.h"

void CLI::startInterractiveCLI(Volume& volume)
{
    QTextStream inputStream(stdin);
    QString command;
    while(true) {
        std::cout << "cbd > ";
        command = inputStream.readLine();
        if (command.isEmpty()) {
            continue;
        }
        QStringList commandArgs = command.split(" ", QString::SkipEmptyParts);
        if (commandArgs.at(0) == "write") {
            if (commandArgs.length() != 3) {
                qDebug() << "wrong number of arguments for command";
                continue;
            }
            QString blockNumber = commandArgs.at(1);
            QString inputFileName = commandArgs.at(2);
            commandWrite(volume, blockNumber.toInt(), inputFileName);
            continue;
        } else if (commandArgs.at(0) == "read") {
            if (commandArgs.length() != 3) {
                qDebug() << "wrong number of arguments for command";
                continue;
            }
            QString blockNumber = commandArgs.at(1);
            QString outputFileName = commandArgs.at(2);
            commandRead(volume, blockNumber.toInt(), outputFileName);
            continue;
        } else if (commandArgs.at(0) == "devinfo") {
            commandDevInfo(volume);
            continue;
        } else if (commandArgs.at(0) == "exit") {
            return;
        } else {
            qDebug() << "unknown command";
            continue;
        }

    }
}

bool CLI::commandWrite(Volume& volume, int blockNumber, QString inputFileName)
{
    QFile inputFile(inputFileName);
    if(!inputFile.open(QIODevice::ReadOnly)) {
        qDebug() << "couln't open file: " << inputFileName;
        return false;
    }
    QByteArray data = inputFile.readAll();
    inputFile.close();

    size_t dataSize = (size_t)data.size();
    size_t blockSize = volume.getBlockSize();
    if (dataSize > blockSize) {
        qDebug() << "file size is bigger than block size of cdb";
        return false;
    } else if (dataSize < blockSize) {
        data.append(QByteArray(blockSize - dataSize, '0'));
    }

    // TODO : проверить на свободное место на диске
    if(volume.getVirtualCapacity() < (size_t)data.size()) {
        qDebug() << "no space on device. Available:" << volume.getVirtualCapacity() << ", required: " << data.size();
        return false;
    }

//    CompressorMpeg compressorMpeg;
//    QByteArray out = compressorMpeg.compress(data);
//    qDebug() << "1 test ffmpeg";
//    QByteArray outData = compressorMpeg.decompress(out);
//    qDebug() << "2 test ffmpeg";
//    QFile outPutFile("outTest");
//    if(!outPutFile.open(QIODevice::WriteOnly)) {
//        qDebug() << "couldn't create file: ";
//        return false;
//    }
//    outPutFile.write(outData);
//    outPutFile.close();
//    return true;

    // TODO : результат записи
    volume.write(blockNumber, data);
    return true;
}

bool CLI::commandRead(Volume& volume, int blockNumber, QString outputFileName)
{
    QFile outPutFile(outputFileName);
    if(!outPutFile.open(QIODevice::WriteOnly)) {
        qDebug() << "couldn't create file: " << outputFileName;
        return false;
    }
    QByteArray data = volume.read(blockNumber);
    outPutFile.write(data);
    outPutFile.close();
    return true;
}

bool CLI::commandDevInfo(Volume& volume)
{
    qDebug() << "volume" << volume;
    return true;
}
