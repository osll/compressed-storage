#ifndef CLI_H
#define CLI_H

#include <QString>

#include "volume.h"

class CLI {
public:
    static bool commandWrite(Volume& volume, int blockNumber, QString inputFileName);
    static bool commandRead(Volume& volume, int blockNumber, QString outputFileName);
    static bool commandDevInfo(Volume& volume);

public:
    void startInterractiveCLI(Volume& volume);
};

#endif // CLI_H
