
#include <QFile>

#include "test.h"

void Test::testForFile(QString filename, Volume &volume)
{
    // TODO : переписать тест для файла - чтобы файл обратно сохранялся в файловую систему, чтобы можно было сравнить результаты
    // rename simpleTest?

    QFile file(filename);

    if(!file.open(QIODevice::ReadOnly))
    {
        qDebug() << "Couln't open " << filename;
        return;
    }

    QByteArray data = file.readAll();
    qDebug() << "["<<filename<<"]" << "Initial file size: " << data.size();


    if(volume.getVirtualCapacity() < (size_t)data.size())
    {
        qDebug() << "No space on device. Available:" << volume.getVirtualCapacity() << ", required: " << data.size();
        return;
    }

    const int blksz = volume.getBlockSize();
    const int blocknum=data.size()/blksz;

    for(int i=0;i<blocknum+1;i++)
    {
        QByteArray chunk(data.data()+i*blksz,blksz);
        volume.write(i,chunk);
    }

    for(int i=0;i<blocknum+1;i++)
    {
        volume.read(i);
    }
} // Test::testForFile(QString filename, Volume &volume)
