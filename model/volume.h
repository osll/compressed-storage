#ifndef VOLUME_H
#define VOLUME_H

#include <QObject>
#include <QVector>
#include <QMap>
#include <QDebug>

#include "phydevice.h"
#include "cbdblockpool.h"
#include "compressorlz.h"
#include "compressormpeg.h"

struct CbdBlockPointer
{
    size_t      m_cbdPoolDivider; // identificator of internal cbd pool
    size_t      m_poolBlockNumber; // block number in internal cbd pool

    CbdBlockPointer() : m_cbdPoolDivider(0), m_poolBlockNumber((size_t) - 1) {}
    bool isValid() const { return m_cbdPoolDivider != 0; }
};


class Volume
{
    PhyDevice*                     m_ptrPhyDevice; // pointer to physical device
    CompressorLZ                   m_compressor; // compressor to compress blocks
//    CompressorMpeg                   m_compressor; // compressor to compress blocks
    size_t                         m_cbdSystemInfoSize; // size in bytes of internal system information for cbd
    size_t                         m_cbdSystemInfoBlocksNumber; // size in physical blocks number of internal system information for cbd
//    QList<CbdBlockPool>            m_cbdPools; // list of internal pools
    QMap<size_t, CbdBlockPool>     m_cbdPoolsMap; // map "identificator of internal cbd pool" -> "cbd pool"
    QMap<size_t,CbdBlockPointer>   m_blocksMap; // map "OS block number"->"pointer where block in cdb locate"

public:
    explicit Volume(PhyDevice* ptrPhyDevice);

    bool initFromPhyDevice();
    void saveToPhyDevice();

    size_t getVirtualBlocksNumber() const;
    size_t getPhysicalBlocksNumber() const;
    size_t getVirtualCapacity() const;
    size_t getPhysicalCapacity() const;
    size_t getBlockSize() const;

    void write(size_t blocknum, const QByteArray& data);
    QByteArray read(size_t blocknum);

    friend QDebug& operator<<(QDebug& stream, const Volume& volume);

protected:

    bool map(size_t blocknum, const QByteArray& compressedData);
    bool unmap(size_t blocknum);
    bool writePool(size_t blocknum, const QByteArray& compressedData);

};

#endif // VOLUME_H
