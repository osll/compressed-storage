#include <QDebug>

#include "compressormpeg.h"

extern "C"
{
#include <libavutil/avutil.h>
#include <libavutil/opt.h>
//#include <libavutil/channel_layout.h>
//#include <libavutil/common.h>
#include <libavutil/imgutils.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
}

CompressorMpeg::CompressorMpeg(QObject *parent) :
    Compressor(parent)
{
    //TODO:
//    // Register all available file formats and codecs
//    av_register_all();

    // register all the codecs
    avcodec_register_all();
}

QByteArray CompressorMpeg::compress(const QByteArray &input)
{
    QByteArray output(input);

    AVCodec *codec;
    AVCodecContext *c= NULL;
    AVFrame *frame;
    AVPacket pkt;
    int i, ret, x, y, got_output;

    // find the video encoder
    codec = avcodec_find_encoder(AV_CODEC_ID_FFV1);
    if (!codec) {
        qDebug() << "Codec not found";
        exit(1);
    }

    c = avcodec_alloc_context3(codec);
    if (!c) {
        qDebug() << "Could not allocate video codec context";
        exit(1);
    }

    // set codec context settings
    /* put sample parameters */
    c->bit_rate = 400000;
    /* resolution must be a multiple of two */
    c->width = 64;
    c->height = 64;
    /* frames per second */
    c->time_base = (AVRational){1,25};
    c->gop_size = 10; /* emit one intra frame every ten frames */
    c->max_b_frames = 1;
    c->pix_fmt = AV_PIX_FMT_YUV420P;

    // open codec
    if (avcodec_open2(c, codec, NULL) < 0) {
        qDebug() << "Could not open codec";
        exit(1);
    }

    frame = av_frame_alloc();
    if (!frame) {
        qDebug() << "Could not allocate video frame";
        exit(1);
    }
    frame->format = c->pix_fmt;
    frame->width = c->width;
    frame->height = c->height;

    // the image can be allocated by any means and av_image_alloc() is just the most convenient way if av_malloc() is to be used
    ret = av_image_alloc(frame->data, frame->linesize, c->width, c->height, c->pix_fmt, 32);
    if (ret < 0) {
        fprintf(stderr, "Could not allocate raw picture buffer\n");
        exit(1);
    }

    // encode one frame
    {
        i = 1;
        av_init_packet(&pkt);
        pkt.data = NULL; // packet data will be allocated by the encoder
        pkt.size = 0;

        fflush(stdout);

        /* prepare a dummy image */
        /* Y */
        for (y = 0; y < c->height; y++) {
            for (x = 0; x < c->width; x++) {
//                frame->data[0][y * frame->linesize[0] + x] = x + y + i * 3;
                frame->data[0][y * frame->linesize[0] + x] = input.at(y * x + x);
            }
        }

//        /* Cb and Cr */
//        for (y = 0; y < c->height/2; y++) {
//            for (x = 0; x < c->width/2; x++) {
//                frame->data[1][y * frame->linesize[1] + x] = 128 + y + i * 2;
//                frame->data[2][y * frame->linesize[2] + x] = 64 + x + i * 5;
//            }
//        }

        /* Cb and Cr */
        for (y = 0; y < c->height/2; y++) {
            for (x = 0; x < c->width/2; x++) {
                frame->data[1][y * frame->linesize[1] + x] = 0;
                frame->data[2][y * frame->linesize[2] + x] = 0;
            }
        }

        frame->pts = i;

        /* encode the image */
        ret = avcodec_encode_video2(c, &pkt, frame, &got_output);
        if (ret < 0) {
            qDebug() << "Error encoding frame";
            exit(1);
        }

        if (got_output) {
            qDebug() << "Write frame size = " << pkt.size;
            output.clear();
            output.append(reinterpret_cast<char*>(pkt.data), pkt.size);
//            fwrite(pkt.data, 1, pkt.size, f);
            av_free_packet(&pkt);
        }
    }

    avcodec_close(c);
    av_free(c);
    av_freep(&frame->data[0]);
    av_frame_free(&frame);

    return output;

}

QByteArray CompressorMpeg::decompress(const QByteArray &input)
{
    QByteArray output = QByteArray();

    qDebug() << "test ffmpeg 1";

    uint8_t buffer[4096];
    memset(buffer, 0, 4096);
    std::copy(input.data(), input.data() + input.size(), buffer);

    uint8_t outbuffer[4096];
    memset(outbuffer, 0, 4096);

    qDebug() << "test ffmpeg 2";

    AVCodec *codec;
    AVCodecContext *c= NULL;
    AVFrame *frame;
    AVPacket avpkt;
    int len, got_frame, y, x;

    av_init_packet(&avpkt);

    qDebug() << "test ffmpeg 3";

    // TODO


    // find the video decoder
    codec = avcodec_find_decoder(AV_CODEC_ID_FFV1);
    if (!codec) {
        qDebug() << "Codec not found";
        exit(1);
    }

    qDebug() << "test ffmpeg 4";

    c = avcodec_alloc_context3(codec);
    if (!c) {
        qDebug() << "Could not allocate video codec context";
        exit(1);
    }

    qDebug() << "test ffmpeg 5";

    /* For some codecs, such as msmpeg4 and mpeg4, width and height
    MUST be initialized there because this information is not
    available in the bitstream. */
    c->width = 64;
    c->height = 64;

    // open codec
    if (avcodec_open2(c, codec, NULL) < 0) {
        qDebug() << "Could not open codec";
        exit(1);
    }

    qDebug() << "test ffmpeg 6";

    frame = av_frame_alloc();
    if (!frame) {
        qDebug() << "Could not allocate video frame";
        exit(1);
    }

    qDebug() << "test ffmpeg 7";

    // decode packet
    avpkt.size = 4096;
    avpkt.data = buffer;

    qDebug() << "test ffmpeg 8";

    len = avcodec_decode_video2(c, frame, &got_frame, &avpkt);
    qDebug() << "test ffmpeg 9";
    if (len < 0) {
        qDebug() << "Error while decoding frame";
        exit(1);
    }
    qDebug() << "test ffmpeg 10";
    if (got_frame) {
        fflush(stdout);

        qDebug() << "test ffmpeg 11";

        /* the picture is allocated by the decoder, no need to free it */
//        for (i = 0; i < c->height; i++) {
//            qDebug() << "test ffmpeg 11a";
//            output.size();
//            qDebug() << "test ffmpeg 11b";
//            output.append(reinterpret_cast<char*>(frame->data[0] + i * frame->linesize[0]), c->width);
//        }
        for (y = 0; y < c->height; y++) {
            for (x = 0; x < c->width; x++) {
                outbuffer[y * x + x] = frame->data[0][y * frame->linesize[0] + x];

            }
        }

        qDebug() << "test ffmpeg 12";
    }

    avcodec_close(c);
    av_free(c);
    av_frame_free(&frame);

    qDebug() << "test ffmpeg 13";

    return QByteArray(reinterpret_cast<char*>(outbuffer), 4096);
}
