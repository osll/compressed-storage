
#include <QDebug>
#include <QSettings>
#include <QFile>

#include "phydevice.h"
#include "utils.h"

int PhyDevice::s_objCount = 0;

PhyDevice::PhyDevice(size_t blocksNumber, size_t blockSize) :
    m_blocksNumber(blocksNumber),
    m_blockSize(blockSize),
    m_space(blocksNumber*blockSize,0)
{
    Q_ASSERT(s_objCount == 0);
    s_objCount++;
}

PhyDevice::~PhyDevice()
{
    s_objCount--;
}

void PhyDevice::write(size_t blocknum, const QByteArray &data)
{
    Q_ASSERT(blocknum < m_blocksNumber);
    Q_ASSERT((size_t)data.size() == m_blockSize);

    try{
        m_space.replace(blocknum * m_blockSize, m_blockSize, data);
    } catch(std::exception& e){
        qDebug() << e.what();
    }
}

QByteArray PhyDevice::read(size_t blocknum) const
{
    Q_ASSERT(blocknum < m_blocksNumber);

    try{
        return m_space.mid(blocknum * m_blockSize, m_blockSize);
    } catch(std::exception& e){
        qDebug() << e.what();
    }

    return QByteArray(m_space.constData()+((blocknum-1)*m_blockSize),m_blockSize*sizeof(char));
}

size_t PhyDevice::blocksNumber() const
{
    return m_blocksNumber;
}

size_t PhyDevice::blockSize() const
{
    return m_blockSize;
}

void PhyDevice::makeRestoreFile() const
{
    QSettings settings(".config", QSettings::IniFormat);
    QString restoreFileName = settings.value("phydev.restore_file", "../data/phydevice").toString();

    QFile restoreFile(restoreFileName);
    if(!restoreFile.open(QIODevice::WriteOnly)) {
        qDebug() << "Couldn't create restore file: " << restoreFileName;
        return;
    }
    restoreFile.write((const char*) &m_blockSize, sizeof(size_t));
    restoreFile.write((const char*) &m_blocksNumber, sizeof(size_t));

    restoreFile.write(m_space);
    restoreFile.close();
}

bool PhyDevice::restoreFromFile()
{
    QSettings settings(".config",QSettings::IniFormat);
    QString restoreFileName = settings.value("phydev.restore_file", "../data/phydevice").toString();

    QFile restoreFile(restoreFileName);
    if(!restoreFile.open(QIODevice::ReadOnly)) {
        qDebug() << "Couldn't open file: " << restoreFileName;
        qDebug() << "New physical storage will be created";
        return false;
    }
    QByteArray data = restoreFile.readAll();
    restoreFile.close();

    if ((size_t)data.size() != (size_t)m_space.size() + 2 * sizeof(size_t)){
        qDebug() << "Can not restore physical device";
        // TODO : handle this situation on upper lavel
        return false;
    }
    size_t dataPtr = 0;
    size_t blockSize = Utils::getSize_t(data, dataPtr);
    dataPtr += sizeof(size_t);
    size_t blocksNumber = Utils::getSize_t(data, dataPtr);
    dataPtr += sizeof(size_t);
    data.remove(0, dataPtr);
    if (blockSize != m_blockSize || blocksNumber != m_blocksNumber || data.size() != m_space.size()) {
        qDebug() << "Can not restore physical device";
        // TODO : handle this situation on upper lavel
        return false;
    }
    m_space = data;
    return true;
}
