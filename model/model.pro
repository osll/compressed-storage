TARGET = compressed-storage-model

CONFIG += qt debug

# compilation flags
QMAKE_CXXFLAGS += -D__STDC_CONSTANT_MACROS
# specifies the #include directories which should be searched when compiling the project
INCLUDEPATH += ../lib/ffmpeg/include/
# add path to search libraries
LIBS += -L../lib/ffmpeg/lib
# list of libraries to be linked into the project
LIBS += -lavformat -lavcodec -lavutil -lswscale -lva -lvorbisenc -lvorbis -ltheoraenc -ltheoradec -lx264 -lvpx -lpostproc -lopus -lmp3lame -lfdk-aac -lm -lz

SOURCES += \
    main.cpp \
    cbdblockpool.cpp \
    compressor.cpp \
    compressorlz.cpp \
    compressormpeg.cpp \
    phydevice.cpp \
    block.cpp \
    volume.cpp \
    test.cpp \
    cli.cpp \
    utils.cpp \

HEADERS += \
    cbdblockpool.h \
    compressor.h \
    compressorlz.h \
    compressormpeg.h \
    phydevice.h \
    block.h \
    volume.h \
    test.h \
    cli.h \
    utils.h \
