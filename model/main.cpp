#include <QApplication>
#include <QDebug>
#include <QSettings>
#include <QStringList>
#include <QTime>

#include <getopt.h>

#include "volume.h"
#include "test.h"
#include "cli.h"

static const char *TEXT_FILE="../data/text.txt";
static const char *IMAGE_FILE="../data/image.jpg";

void make_config()
{
    QSettings settings(".config",QSettings::IniFormat);
    // physical device blocksize
    settings.setValue("phydev.blocksize",1024*4);
    // physical device blocks number
    settings.setValue("phydev.blocks",100);
    // path to file that contains restore data for physical device
    settings.setValue("phydev.restore_file", "../data/phydevice");
    // compressed block device pools
    settings.setValue("cbd.pools","8 4 2 1");
    // size of system information for block device
    settings.setValue("cbd.sys_info_size", 4096);
    // compressed block device compressor
    settings.setValue("cbd.compressor","default");
    // test file
    settings.setValue("test.input_file",IMAGE_FILE);
}

static const char* optString = "r:w:dcmh";
static const struct option longOpts[] = {
    {"read", required_argument, NULL, 'r'},
    {"write", required_argument, NULL, 'w'},
    {"dev_info", no_argument, NULL, 'd'},
    {"cli", no_argument, NULL, 'c'},
    {"make_config", no_argument, NULL, 'm'},
    {"help", no_argument, NULL, 'h'}
};

int main(int argc, char** argv)
{
    QApplication app(argc,argv);

    QSettings settings(".config",QSettings::IniFormat);
    const int blockSize = settings.value("phydev.blocksize", 1024 * 2).toInt();
    const int blocksNumber = settings.value("phydev.blocks",10).toInt();

    QTime timeWithInit;
    timeWithInit.start();

    int longIndex;
    int opt = getopt_long(argc, argv, optString, longOpts, &longIndex);
    if(opt != -1) {
        switch(opt) {
            case 'r':
            case 'w':
            case 'd':
            case 'c':
            {
                PhyDevice phyDevice(blocksNumber, blockSize);
                phyDevice.restoreFromFile();
                Volume volume(&phyDevice);
                volume.initFromPhyDevice();

                QTime timeWithoutInit;
                timeWithoutInit.start();

                if(opt == 'r'){
                    if(optind != argc - 1) {
                        qDebug() << "usage: -r [block_number] [file]";
                        break;
                    }
                    QString blockNumber = optarg;
                    QString outputFileName = *(argv + optind);
                    if(!CLI::commandRead(volume, blockNumber.toInt(), outputFileName)) {
                        qDebug() << "error executing command";
                    }
                } else if(opt == 'w') {
                    if(optind != argc - 1) {
                        qDebug() << "usage: -w [block_number] [file]";
                        break;
                    }
                    QString blockNumber = optarg;
                    QString inputFileName = *(argv + optind);
                    if(!CLI::commandWrite(volume, blockNumber.toInt(), inputFileName)) {
                        qDebug() << "error executing command";
                    }
                } else if(opt == 'd') {
                    CLI::commandDevInfo(volume);
                } else if(opt == 'c') {
                    CLI cli;
                    cli.startInterractiveCLI(volume);
                }

                int durationWithoutInit = timeWithoutInit.elapsed();
                qDebug() << "time of execution without initialisation = " << durationWithoutInit << " ms";

                volume.saveToPhyDevice();
                phyDevice.makeRestoreFile();
                break;
            }
            case 'm':
            {
                make_config();
                qDebug() << "default config is created";
                break;
            }
            case 'h':
            {
                qDebug() << "Commands:";
                qDebug() << "   -r, --read [block_number] [file] - read block with number [block_number] to [file]";
                qDebug() << "   -w, --write [block_number] [file] - write from [file] to block with number [block_number]";
                qDebug() << "   -d, --dev_info - show inflormation about block device";
                qDebug() << "   -c, --cli - start CLI mode";
                qDebug() << "   -m, --make_config - make config file";
                qDebug() << "   -h, --help - help";
                break;
            }
        }
    }

    int durationWithInit = timeWithInit.elapsed();
    qDebug() << "time of execution with initialisation = " << durationWithInit << " ms";

    return 0;
}
