For application building you should execute command qmake or simply execute bash script ./build.sh

When you execute application at first time, you don't need any extra resources, except maybe .config file, if you don't want to use default configuration parameters.

Application can be executed with following keys (multiple keys is not supported):
  -r, --read [block_number] [file] - read block with number [block_number] to [file];
  -w, --write [block_number] [file] - write from [file] to block with number [block_number];
  -d, --dev_info - show inflormation about block device;
  -c, --cli - start CLI mode";
  -m, --make_config - make config file";
  -h, --help - help";

In interactive mode of CLI you can use following commands:
  read [block_number] [output_file_path] - read block with number [block_number] to [file]
  write [block_number] [input_file_path] - write from [file] to block with number [block_number]
  devinfo - show inflormation about block device
  exit - exit from CLI

The test input file for application is in the directory ../data.

When application finished its work, in directory ../data would be created file phydevice (configurable parameter in ./config).
This file is used to restore date from physical disk. If application can't find this file, then application create new clean model of physical device.
