#ifndef COMPRESSOR_H
#define COMPRESSOR_H

#include <QObject>
#include <QByteArray>
#include <QMap>

class Compressor : public QObject
{
    Q_OBJECT;

public:
    //TODO: use types and factory method;
    enum Type
    {
        None,
        LZ,
        MPEG
    };

private:

    static QMap<Type,Compressor*>  s_compressors;
    static Type                    s_current;
    bool                           m_valid;


public:

    virtual QByteArray compress(const QByteArray& input);
    virtual QByteArray decompress(const QByteArray& input);
    Compressor(QObject *parent = NULL);


private:
};

#endif // COMPRESSOR_H
