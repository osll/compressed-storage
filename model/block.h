#ifndef BLOCK_H
#define BLOCK_H

#include <QObject>
#include <QByteArray>
#include <QDebug>
#include "compressorlz.h"

class Block
{
    QByteArray          m_rawdata;
    QByteArray          m_compressed;
    static CompressorLZ s_compressor;

public:
    explicit Block(const QByteArray& data);
    const QByteArray& getData() const;
    void setData(const QByteArray& data);

    void setCompressedData(const QByteArray& data);
    const QByteArray& getCompressedData() const;

    friend QDebug operator<<(QDebug& stream, const Block& block);

    
};

#endif // BLOCK_H
