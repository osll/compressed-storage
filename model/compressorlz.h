#ifndef COMPRESSORLZ_H
#define COMPRESSORLZ_H

#include "compressor.h"

class CompressorLZ : public Compressor
{
    Q_OBJECT
public:
    explicit CompressorLZ(QObject *parent = 0);
    virtual QByteArray compress(const QByteArray& input);
    virtual QByteArray decompress(const QByteArray& input);
    
signals:
    
public slots:
    
};

#endif // COMPRESSORLZ_H
