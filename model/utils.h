#ifndef UTILS_H
#define UTILS_H

#include <QByteArray>

class Utils
{
public:
    static size_t getSize_t(const QByteArray& data, size_t dataPtr);
};

#endif // UTILS_H
