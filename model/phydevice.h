#ifndef PHYDEVICE_H
#define PHYDEVICE_H

#include <stdlib.h>
#include <QByteArray>
#include <QString>

class PhyDevice
{
    static int s_objCount;

    const size_t m_blocksNumber;
    const size_t m_blockSize;
    QByteArray   m_space;

public:
    explicit PhyDevice(size_t blocksNumber, size_t blockSize);
    virtual ~PhyDevice();

    void write(size_t blocknum, const QByteArray& data);
    QByteArray read(size_t blocknum) const;

    size_t blocksNumber() const;
    size_t blockSize() const;

    void makeRestoreFile() const;
    bool restoreFromFile();
};

#endif // PHYDEVICE_H
