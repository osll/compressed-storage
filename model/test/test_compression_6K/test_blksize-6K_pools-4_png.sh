#!/bin/bash

rm -f ./data/tmp/*
rm -f ./data/log
rm -f ./data/phydevice
rm -f ./data/output.png
split -b 6k -a 3 -d ./data/input.png ./data/tmp/in_
COUNT=0
COUNT_BLOCKS=`find ./data/tmp -name "in_*" | wc -w`
find ./data/tmp -name "in_*" | sort | while read FILENAME; do
../bin/compressed-storage-model --write "$COUNT" "$FILENAME" 
let "COUNT=$COUNT+1"
done
../bin/compressed-storage-model --dev_info
echo "$COUNT"
echo "$COUNT_BLOCKS"
while [ "$COUNT" -ne "$COUNT_BLOCKS" ]; do
OUTFILE=`printf "./data/tmp/out_%03d" "$COUNT"`
../bin/compressed-storage-model --read "$COUNT" "$OUTFILE" 
let "COUNT=$COUNT+1"
done
cat ./data/tmp/out_* > ./data/output.png
