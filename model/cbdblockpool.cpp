
#include <QDebug>

#include "cbdblockpool.h"

CbdBlockPool::CbdBlockPool(PhyDevice *device, size_t divider, size_t start_index, size_t blocks) :
    m_ptrPhyDevice(device), m_divider(divider), m_phyBegin(start_index), m_phyBlocksNumber(blocks)
{
    Q_ASSERT(device);
    Q_ASSERT(divider);
    Q_ASSERT(m_ptrPhyDevice->blocksNumber() >= m_phyBegin + m_phyBlocksNumber);
    Q_ASSERT( (m_ptrPhyDevice->blockSize() % m_divider) == 0 );

    m_allocated.fill(false,virtualBlocksNumber());
}

CbdBlockPool::CbdBlockPool(const CbdBlockPool &obj) :
    m_ptrPhyDevice(obj.m_ptrPhyDevice), m_divider(obj.m_divider),
    m_phyBegin(obj.m_phyBegin), m_phyBlocksNumber(obj.m_phyBlocksNumber), m_allocated(obj.m_allocated)
{
}

size_t CbdBlockPool::getPhyBlock(size_t virtualBlockNumber) const
{
//    return m_phyBegin + vnumber / virtualBlockSize();
    return m_phyBegin + virtualBlockNumber / m_divider;
}

size_t CbdBlockPool::getIndexInPhyBlock(size_t virtualBlockNumber) const
{
    return virtualBlockNumber % m_divider;
}

void CbdBlockPool::write(size_t blocknum, const QByteArray& compressedData)
{
    Q_ASSERT((size_t)compressedData.size() <= virtualBlockSize());
    Q_ASSERT(blocknum < virtualBlocksNumber());

    size_t phyBlockNum = getPhyBlock(blocknum);
    size_t phyIndex = getIndexInPhyBlock(blocknum);

    QByteArray phyBlock = m_ptrPhyDevice->read(phyBlockNum);
    phyBlock.replace(phyIndex * virtualBlockSize(), compressedData.size(), compressedData);
    m_ptrPhyDevice->write(phyBlockNum, phyBlock);
}

const QByteArray CbdBlockPool::read(size_t blocknum) const
{
    Q_ASSERT(blocknum < virtualBlocksNumber());

    QByteArray phyblock = m_ptrPhyDevice->read( getPhyBlock(blocknum) );

    // TODO : еужна ли проверка, что в этом блоке действительно записана информация?
    return extractData(phyblock, getIndexInPhyBlock(blocknum));

//    Block block = Block((extractData(phyblock,getIndexInPhyBlock(blocknum))));
//    block.setCompressedData((extractData(phyblock,getIndexInPhyBlock(blocknum))));
//    return block;
}

size_t CbdBlockPool::getFreeVirtualBlockNumber()
{
    size_t idx = m_allocated.indexOf(false);
    m_allocated[idx] = true;
    return idx;
}

void CbdBlockPool::putFreeVirtualBlockNumber(size_t blocknum)
{
    Q_ASSERT(blocknum < (size_t)m_allocated.size());

    m_allocated[blocknum] = false;
}

void CbdBlockPool::putAllocVirtualBlockNumber(size_t blocknum)
{
    Q_ASSERT(blocknum < (size_t)m_allocated.size());

    m_allocated[blocknum] = true;
}

QByteArray CbdBlockPool::extractData(const QByteArray phyBlock, size_t index) const
{
    return phyBlock.mid(index * virtualBlockSize(), virtualBlockSize());
}

QDebug operator<<(QDebug& stream, const CbdBlockPool& obj)
{
    size_t allocated = obj.numberAllocatedBlocks();
    size_t available = obj.numberAvailableBlocks();
    double freeSpaceProcent = 100 * ((double)available)/((double)obj.virtualBlocksNumber());

    stream << "Pool: begin=" << obj.getPhyBlock(0) << ", blksz=" << obj.virtualBlockSize()
           << ", phyBlocksNumber=" << obj.phyBlocksNumber() << ", virtualBlocksNumber=" << obj.virtualBlocksNumber()
           << " allocated=" << allocated << ", available=" << available << ", free " << freeSpaceProcent << "% )";
    return stream;
}


