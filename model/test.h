#ifndef TEST_H
#define TEST_H

#include <QString>

#include "volume.h"

class Test {
public:
    void testForFile(const QString filename, Volume& volume);
};

#endif // TEST_H
