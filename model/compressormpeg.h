#ifndef COMPRESSORMPEG_H
#define COMPRESSORMPEG_H

#include "compressor.h"

class CompressorMpeg : public Compressor
{
    Q_OBJECT
public:
    explicit CompressorMpeg(QObject *parent = 0);
    virtual QByteArray compress(const QByteArray& input);
    virtual QByteArray decompress(const QByteArray& input);
signals:
    
public slots:
    
};

#endif // COMPRESSORMPEG_H
