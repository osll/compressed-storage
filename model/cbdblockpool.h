#ifndef CBDBLOCKPOOL_H
#define CBDBLOCKPOOL_H

#include <QObject>
#include <QVector>
#include <QDebug>

#include "phydevice.h"

class CbdBlockPool
{

    PhyDevice*  m_ptrPhyDevice;       // Physical block device

    size_t      m_divider;            // PHY block size divider
    size_t      m_phyBegin;           // Pool starts on this phy block
    size_t      m_phyBlocksNumber;    // Physical blocks amount in this pool

    QVector<bool> m_allocated;   // bit list (dirty blocks) in this pool

public:
    CbdBlockPool()
    { Q_ASSERT(false); }
    explicit CbdBlockPool(PhyDevice* device, size_t divider, size_t start_index, size_t blocks);
    CbdBlockPool(const CbdBlockPool& obj);

    // return physical blocks number in pool
    inline size_t phyBlocksNumber() const
    { return m_phyBlocksNumber; }

    // return pool divider
    inline size_t poolDivider() const
    { return m_divider; }

    // return number of virtual (pool-sized) blocks
    inline size_t virtualBlocksNumber() const
    { return m_phyBlocksNumber * m_divider; }

    // return size of virtual (pool-sized) blocks
    inline size_t virtualBlockSize() const
    { return m_ptrPhyDevice->blockSize() / m_divider; }

    // return physical block number by virtual block number
    inline size_t getPhyBlock(size_t virtualBlockNumber) const;

    // return index of virtual block number inside physizal block number
    inline size_t getIndexInPhyBlock(size_t virtualBlockNumber) const;

    size_t numberAllocatedBlocks() const
    { return m_allocated.count(true); }

    size_t numberAvailableBlocks() const
    { return m_allocated.count(false); }

    void write(size_t virtualBlockNum, const QByteArray& compressedData);
    const QByteArray read(size_t virtualBlockNum) const;

    // get number of next free block in pool
    size_t getFreeVirtualBlockNumber();
    // mark block in pool as free
    void putFreeVirtualBlockNumber(size_t blocknum);
    // mark block in pool as allocated
    void putAllocVirtualBlockNumber(size_t blocknum);

    friend QDebug operator<<(QDebug& stream, const CbdBlockPool& obj);

protected:
    QByteArray extractData(const QByteArray phyBlock, size_t index) const;
};

#endif // CBDBLOCKPOOL_H
