
#include "utils.h"

size_t Utils::getSize_t(const QByteArray& data, size_t dataPtr)
{
    size_t* ptrValue = (size_t*) data.mid(dataPtr, sizeof(size_t)).constData();
    return *ptrValue;
}
