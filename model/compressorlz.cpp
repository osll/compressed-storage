#include "compressorlz.h"

CompressorLZ::CompressorLZ(QObject *parent) :
    Compressor(parent)
{
}

QByteArray CompressorLZ::compress(const QByteArray &input)
{
    QByteArray cdata = qCompress(input,9);
    return cdata;
}

QByteArray CompressorLZ::decompress(const QByteArray &input)
{
    return qUncompress(input);
}
