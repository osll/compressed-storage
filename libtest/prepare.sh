#!/bin/sh

FFMPEG_ROOT=/ws/FFmpeg

FF_LIBS=`find ${FFMPEG_ROOT} -name *.so`


mkdir ./libs 
ln -s ${FFMPEG_ROOT} ffmpeg

for l in ${FF_LIBS}
do	
		ln -s $l ./libs/
done

ln -s libs/libavformat.so libs/libavformat.so
ln -s libs/libavutil.so libs/libavutil.so.52
ln -s	libs/libavcodec.so libs/libavcodec.so.54
ln -s	libs/libavutil.so libs/slibavutil.so.51
