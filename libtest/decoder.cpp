#include <stdint.h>
#include <cstdlib>

extern "C"
{
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
#include <libavutil/avutil.h>

}

void SaveFrame(AVFrame *pFrame, int width, int height, int idx)
{
	int i=0;

  FILE *pFile = NULL;
  char filename[32];

  sprintf(filename,"Frame%d.ppm", idx);
  pFile = fopen(filename, "w");
  if (!pFile)
    exit(EXIT_FAILURE);
  fprintf(pFile, "p6\n%d %d\n255\n", width, height);

  for (i = 0; i < height; i++)
    fwrite(pFrame->data[0]+i*pFrame->linesize[0], 1, width*3, pFile);
  fclose(pFile);
}

int main(int argc, char **argv)
{
	int i=0;
  AVFormatContext *pFormatCtx = NULL;
  AVCodecContext *pCodecCtx = NULL;
  AVCodec *pCodec = NULL;
  AVDictionary *dict = NULL;
  AVFrame *pFrame = NULL;
  AVFrame *pFrameRGB = NULL;
  AVPacket packet;
  struct SwsContext *swsCtx = NULL;
  uint8_t *buffer = NULL;
  int vStream = -1;
  int bufSize = 0;
  int frameDec = 0;

  //  avcodec_register_all();
  av_register_all();
  // Open file
  if (avformat_open_input(&pFormatCtx, argv[1], NULL, &dict) != 0)
    return -1;
  // Get stream info
  if (avformat_find_stream_info(pFormatCtx, &dict) < 0)
    return -1;
  // Dump file info to stderr
  av_dump_format(pFormatCtx, 0, argv[1], 0);
  // Select first video stream
  for ( i = 0; i < pFormatCtx->nb_streams; ++i)
    if (pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO){
      vStream = i;
      break;
    }
  if (vStream == -1)
    return -1;
  // Get a pointer to the codec context for the vStream
  pCodecCtx = pFormatCtx->streams[vStream]->codec;
  // Get decoder for vStream
  pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
  if (pCodec == NULL)
    return -1;
  // Open codec
  if (avcodec_open2(pCodecCtx, pCodec, &dict) < 0)
    return -1;
  // Allocate mem for initial Frame
  pFrame = avcodec_alloc_frame();
  if (!pFrame)
    return -1;
  // Allocate mem for result Frame
  pFrameRGB = avcodec_alloc_frame();
  if (!pFrameRGB)
    return -1;
  // Allocate mem for working data
  bufSize = avpicture_get_size(PIX_FMT_RGB24, pCodecCtx->width, pCodecCtx->height);
  buffer = (uint8_t *)av_malloc(bufSize*sizeof(uint8_t));
  // Assign buffer to FrameRGB
  avpicture_fill((AVPicture *)pFrameRGB, buffer, PIX_FMT_RGB24, pCodecCtx->width, pCodecCtx->height);
  //Read frames and save then to disk
  //For vStream 1 packet == 1 frame
  int j = 0;
  swsCtx = sws_getCachedContext(swsCtx, pCodecCtx->width, pCodecCtx->height, pCodecCtx->pix_fmt, pCodecCtx->width, pCodecCtx->height, PIX_FMT_RGB24, SWS_BICUBIC, NULL, NULL, 0);

  while(av_read_frame(pFormatCtx, &packet) != 0){
    if (packet.stream_index == vStream){
      //Decode frame
      avcodec_decode_video2(pCodecCtx, pFrame, &frameDec, &packet);
      if (frameDec){
        sws_scale(swsCtx, (uint8_t const * const *)pFrame->data, pFrame->linesize, 0, pCodecCtx->height, pFrameRGB->data, pFrameRGB->linesize);
        SaveFrame(pFrameRGB, pCodecCtx->width, pCodecCtx->height, j++);
      }
    }
    av_free_packet(&packet);
  }
  av_free(buffer);
  av_free(pFrameRGB);
  av_free(pFrame);
  avcodec_close(pCodecCtx);
  avformat_close_input(&pFormatCtx);
  return 0;
}
