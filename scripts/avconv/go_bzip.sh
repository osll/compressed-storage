#!/bin/sh

files=`ls -1 block_*`

for i in $files; do
	echo "! file: $i"

	echo "C:"
  time tar cjf ${i}.bz ${i}
	echo "D:"
	time tar xjf ${i}.bz -C /tmp/o
done;
