#!/bin/sh

files=`ls -1 block_*`

for i in $files; do
	echo "! file: $i"

	echo "C:"
  time lha ${i}.lha ${i} 
	echo "D:"
	time lha -xw=/tmp/ ${i}.lha 
done;
