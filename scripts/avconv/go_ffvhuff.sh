#!/bin/sh

files=`ls -1 block_*`

for i in $files; do
	echo "! file: $i"

	echo "C:"
	time avconv -f rawvideo  -s 512x100 -i ${i} -c:v ffvhuff _${i}.mkv
	echo "D:"
	time avconv -i _${i}.mkv  /tmp/a%08d.bmp
done;
