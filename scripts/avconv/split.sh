#!/bin/bash

file="IMG00001.dcm"

for i in {1..9}  
do
	count=`echo ${i}*10000|bc`
	dd if=${file}  of=block_${i} bs=512 count=${count};
done
