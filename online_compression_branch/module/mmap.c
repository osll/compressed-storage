#include "../common/mmap_common.h"

#ifndef VM_RESERVED
	#define VM_RESERVED (VM_DONTEXPAND | VM_DONTDUMP)
#endif

const int TICK_MS = 20;		// Интервал опрашивания буфера на предмет получения новой команды

struct mmap_info {
	char *data;						// Данные, хранящиеся в mmap
	int reference;       		// Количество обращений к mmap  	
};

struct dentry  *mmap_descriptor;
struct mmap_info *info;

/**
 * Ожидает появления в буффере какой-либо команды 
 * Ожидание ограничено указанным количеством тактов
 * @param count_tick - Количество тактов, на протяжении которых ожидается поступление команды
 * 
 * @return 0 - команда успешно получена, отличное от нуля - команда не получена
 */ 
int waitSomeCommandTimer(int count_tick)
{
	int counter = 0;
	for (; info->data[0] != MESSAGE_EMPTY && counter != count_tick; ++counter)
   {
      udelay(TICK_MS);
   }
	return info->data[0] != MESSAGE_EMPTY ? 0 : -1;
}

/**
 * Ожидает появления в буффере указанной команды 
 * Ожидание ограничено указанным количеством тактов
 * @param command - Ожидаемая команда
 * @param count_tick - Количество тактов, на протяжении которых ожидается поступление команды
 * 
 * @return 0 - команда успешно получена, отличное от нуля - команда не получена
 */ 
int waitCommandTimer(const char command, int count_tick)
{
	int counter = 0;
	for (; info->data[0] != command && counter != count_tick; ++counter)
   {
      msleep(TICK_MS);
   }
 	if (info->data[0] == command) 
	{
		info->data[0] = MESSAGE_EMPTY;
		return 0;	
	}
	return -1;
}

/**
 * Ожидает появления в буффере какой-либо команды
 */ 
void waitSomeCommand(void)
{
	while (waitSomeCommandTimer(INT_MAX) != 0);
}

/**
 * Посылает указанную команду демону
 * @param command - Посылаемая команда
 */
void send_command(const char command)
{
	memcpy(info->data, &command, sizeof(char));
}

void mmap_open(struct vm_area_struct *vma)
{
	struct mmap_info *info = (struct mmap_info *)vma->vm_private_data;
	info->reference++;
}

void mmap_close(struct vm_area_struct *vma)
{
	struct mmap_info *info = (struct mmap_info *)vma->vm_private_data;
	info->reference--;
}

static int mmap_fault(struct vm_area_struct *vma, struct vm_fault *vmf)
{
	struct page *page;
	struct mmap_info *info;
	info = (struct mmap_info *)vma->vm_private_data;
	if (!info->data) {
		printk("no data\n");
		return -1;	
	}

	/* get the page */
	page = virt_to_page(info->data);
	
	/* increment the reference count of this page */
	get_page(page);
	vmf->page = page;
	return 0;
}

struct vm_operations_struct mmap_vm_ops = {
	.open =     mmap_open,
	.close =    mmap_close,
	.fault =    mmap_fault,
};

int my_mmap(struct file *filp, struct vm_area_struct *vma)
{
	vma->vm_ops = &mmap_vm_ops;
	vma->vm_flags |= VM_RESERVED;
	/* assign the file private data to the vm private data */
	vma->vm_private_data = filp->private_data;
	mmap_open(vma);
	return 0;
}

int my_close(struct inode *inode, struct file *filp)
{
	filp->private_data = NULL;
	return 0;
}

int my_open(struct inode *inode, struct file *filp)
{
	filp->private_data = info;																	 	// Привязываем mmap к дескриптору файла
	return 0;
}

static const struct file_operations my_fops = {
	.open = my_open,
	.release = my_close,
	.mmap = my_mmap,
};
