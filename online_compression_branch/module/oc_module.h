MODULE_LICENSE( "GPL v2" );
MODULE_VERSION( "1.5" );
MODULE_AUTHOR( "Plokhoy Nikolay" );

#define KERNEL_SECTOR_SIZE    	512
#define MY_DEVICE_NAME 		"cbd"
#define DEV_MINORS		16
#define FILE_NAME_LEN 256

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,35)
	#define blk_fs_request(rq)      ((rq)->cmd_type == REQ_TYPE_FS)
#endif

#define ERR(...) printk( KERN_ERR "! "__VA_ARGS__ )
#define LOG(...) printk( KERN_INFO "+ "__VA_ARGS__ )
#define DBG(...) if( debug > 0 ) printk( KERN_DEBUG "# "__VA_ARGS__ )

static int diskmb = 4;
static int debug = 0;
static int major = 0;
static int num_devices = 1;

static int entire_count = -1;
static int halves_count = -1;
static int quarters_count = -1;
static char file_name[FILE_NAME_LEN];

module_param_string(file, file_name, FILE_NAME_LEN, 0);
module_param_named(size, diskmb, int, 0); 	// размер диска в Mb, по умолчанию - 4Mb
module_param(debug, int, 0);              	// уровень отладочных сообщений
module_param(major, int, 0);
module_param(num_devices, int, 0);
module_param(entire_count, int, 0);
module_param(halves_count, int, 0);
module_param(quarters_count, int, 0);

static int my_getgeo( struct block_device *bdev, struct hd_geometry *geo ) {
   unsigned long sectors = ( diskmb * 1024 ) * 2;
   DBG( KERN_INFO "getgeo\n" );
   geo->heads = 4;
   geo->sectors = 16;
   geo->cylinders = sectors / geo->heads / geo->sectors;
   geo->start = geo->sectors;
   return 0;
}

static int my_ioctl( struct block_device *bdev, fmode_t mode, unsigned int cmd, unsigned long arg ) 
{
   DBG("ioctl cmd=%X\n", cmd);
   switch (cmd) 
   {
      case 5331: 
      {
         struct hd_geometry geo;
         DBG( "ioctk HDIO_GETGEO\n" );
         my_getgeo( bdev, &geo );
         if( copy_to_user( (void __user *)arg, &geo, sizeof( geo ) ) )
         {
            return -EFAULT;
         }
         return 0;
      }

      default:
         ERR( "ioctl unknown command\n" );
         return -ENOTTY;
   }
}

static struct block_device_operations mybdrv_fops = {
   .owner = THIS_MODULE,
   .ioctl = my_ioctl,
   .getgeo = my_getgeo
};
