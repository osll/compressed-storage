#include <linux/module.h>
#include <linux/delay.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/debugfs.h>
#include <linux/slab.h>
#include <linux/version.h>
#include <linux/vmalloc.h>
#include <linux/blkdev.h>
#include <linux/genhd.h>
#include <linux/errno.h>
#include <linux/hdreg.h>
#include <linux/mm.h> 

#include "oc_module.h"
#include "mmap.c"

/**
* Содержит информацию о блоке.
*
* Множество таких структур составляет пул блоков.
*/
struct block_info
{
   u8 *data;			// Указатель на область памяти, где расположен данный блок
   size_t size;		// Текущий размер блока
};

/**
* Внутреннее представление данного блочного устройства
*/
struct disk_dev
{               
   long block_count;                   // Размер устройства в блоках
	u8 *data;									// Массив данных
   spinlock_t lock;             			// Используется для спин-блокировки
   struct request_queue *queue; 			// Очередь запросов к устройству
   struct gendisk *gd;          			// Указатель на структуру gendisk
	struct block_info *pool;				// Общий пул блоков
	struct block_info *pool_entire;		// Пул целых блоков
	long entire_count;						// Количество целых блоков
	struct block_info *pool_halves; 		// Пул половинных блоков
	long halves_count;						// Количество половинных блоков   
	struct block_info *pool_quarters; 	// Пул четвертных блоков
	long quarters_count;						// Количество четвертных блоков
	struct block_info *empty_block;		// Пустой блок
	struct block_info **block_map;		// Карта блоков
	char *file_name;							// Имя файла, в котором хранятся данные
   char *compress_buff; 					// Буффер, используемый для хранения данных после операции компрессии/декомпрессии
};

static struct disk_dev *Devices = NULL;

// TODO - добавить возможность выбора алгоритма сжатия
static int compress_block(const char* source, char* dest, size_t* dest_len )
{
	char* offset_data = info->data + sizeof(char);
 	memcpy(offset_data, source, KERNEL_SECTOR_SIZE);
	send_command(MESSAGE_COMPRESS_BLOCK);
	waitCommandTimer(MESSAGE_BLOCK_COMPRESSED, 10);

	memcpy(dest_len, offset_data, sizeof(size_t));
	if (*dest_len == 0)
	{
		return -EIO;
	}
	offset_data += sizeof(size_t);
	memcpy(dest, offset_data, *dest_len);
	return 0;
}

// TODO - добавить возможность выбора алгоритма сжатия
static int decompress_block(const char* source, size_t* source_len, char* dest )
{
   char* offset_data = info->data + sizeof(char);
	memcpy(offset_data, source_len, sizeof(size_t));
	offset_data += sizeof(size_t);
	memcpy(offset_data, source, *source_len);
	send_command(MESSAGE_DECOMPRESS_BLOCK);
	waitCommandTimer(MESSAGE_BLOCK_DECOMPRESSED, 10);
	
	offset_data -= sizeof(size_t);
	memcpy(dest, offset_data, KERNEL_SECTOR_SIZE);
	return 0; 
}

/**
 * Получить свободный блок в пуле
 *
 * @param dev - устройство, на котором выполняется операция ввода/вывода
 * @param pool - пул, в котором производится поиск
 * @param pool_index - индекс пула
 * @return block_info - не занятый блок или NULL - если пул заполнен
 */
static struct block_info* get_free_block(struct disk_dev* dev, struct block_info* pool, int pool_index)
{
	int i;
	int count;	

	if (pool_index == 1) count = dev->entire_count;
	if (pool_index == 2) count = dev->halves_count;
	if (pool_index == 3) count = dev->quarters_count;

	for (i = 0; i < count; ++i)
	{
		if (pool[i].size == 0)
		{
			return &(pool[i]);
		}
	}
	return NULL;
}

/**
 * Получить пул блоков, соответствующий требуемому размеру
 *
 * @param dev - устройство, на котором выполняется операция ввода/вывода
 * @param block_size - размер блока, который будет использоваться с данным пулом
 * @param pool - пул, который удовлетворяет заданному блоку
 * @return int - возвращает индекс пула (1 - целые, 2 - половинные, 3 - четвертные)
 */
static int get_available_pool(struct disk_dev* dev, int block_size, struct block_info** pool)
{
	if (block_size <= KERNEL_SECTOR_SIZE / 4)
	{
		*pool = dev->pool_quarters;	
		return 3;
	}
	if (block_size <= KERNEL_SECTOR_SIZE / 2 && block_size > KERNEL_SECTOR_SIZE / 4)
	{
		*pool = dev->pool_halves;
		return 2;
	}
	*pool = dev->pool_entire;
	return 1;
}

/**
 * Выполняет операцию записи блока
 * 
 * @param dev - устройство, на котором выполняется операция ввода/вывода
 * @param index - индекс считываемого блока
 * @param buffer - буфер, содержащий блок для записи
 */
static int write_block(struct disk_dev* dev, int index, char *buffer)
{
	int pool_index = 0;
	int size_after_compress = -1;
	struct block_info **pool = vmalloc(sizeof(struct block_info*));
	struct block_info *block = dev->block_map[index];
	*pool = NULL;
 
	if (block->size != 0)
	{
		block->size = 0;
		dev->block_map[index] = dev->empty_block;
		DBG("Block wasn't empty, clear it!");
	}
	
	if (compress_block(buffer, dev->compress_buff, &size_after_compress))
   {
   	ERR("Failed compress block: %d\n", index);
      return -EIO;
   } 
	DBG("Size after compress %d block: %d\n", index, size_after_compress);
		  
	pool_index = get_available_pool(dev, size_after_compress, pool);
	block = get_free_block(dev, *pool, pool_index);
	if (block == NULL)
	{
   	ERR("Not found free block in pool: %d\n", pool_index);
      return -EIO;		
	} 

	block->size = size_after_compress;	
	memcpy(block->data, dev->compress_buff, size_after_compress);
	dev->block_map[index] = block;
	vfree(pool);
	return 0;
}

/**
 * Выполняет операцию чтения блока и запись его в буфер
 *
 * @param dev - устройство, на котором выполняется операция ввода/вывода
 * @param index - индекс считываемого блока
 * @param buffer - буфер, в который необходимо сохранить блок
 */
static int read_block(struct disk_dev *dev, int index, char *buffer)
{

	struct block_info *block = dev->block_map[index];

	if (block->size == 0)
	{
		memcpy(buffer, dev->empty_block, KERNEL_SECTOR_SIZE);		
		DBG("Read empty block: %d\n", index);
		return 0;
	}   

	// Производилась ли компрессия блока	
	if (block->size == KERNEL_SECTOR_SIZE)
	{
		DBG("Read uncompressed block\n");
		memcpy(buffer, block->data, KERNEL_SECTOR_SIZE);
	} 
	else
	{
		DBG("Read compressed block\n");
		if (decompress_block(block->data, &block->size, dev->compress_buff))	
   	{
			ERR("Failed decompress block: %d\n", index);
		   return -EIO;               
   	} 
		memcpy(buffer, dev->compress_buff, KERNEL_SECTOR_SIZE);
	}
   DBG("Success read block: %d\n", index);
	return 0;
}

/**
 * Осуществляет операцию ввода-вывода для заданного устройства.
 *
 * @param dev - устройство, для которого выполняется операция
 * @param index_first - номер начального блока
 * @param num_blocks - количество блоков, над которыми необходимо произвести операцию
 * @param buffer - буфер в/из которого будет произведена запись/чтение
 * @param is_write - значение отличное от нуля означает, что  будет произведена операция записи, иначе - чтения
 *
 * @return 0 - если операция завершена успешно, иначе - код ошибки
 */
static int transfer(struct disk_dev *dev, unsigned long index_first, 
						  unsigned long num_blocks, char *buffer, int is_write ) 
{
   int i;
   // Проверяем, что не выходим за границы реально существующей памяти
   if((index_first + num_blocks) > dev->block_count) 
   {
      ERR( "Overflow I/O operation (%ld %ld)\n", index_first, num_blocks );
      return -EIO;
   }
   
	// Обрабатываем каждый запрашиваемый блок
   for (i = 0; i < num_blocks; ++i)			
   {
      if (is_write)				
      {
			if (write_block(dev, index_first + i, buffer + i * KERNEL_SECTOR_SIZE) < 0)
			{
				return -EIO;
			}
      }
      else							
      {      
 			if (read_block(dev, index_first + i, buffer + i * KERNEL_SECTOR_SIZE) < 0)
			{
				return -EIO;
			}
      }
   }
   return 0;
}

/**
 * Простой запрос с обработкой очереди
 */
static void simple_request(struct request_queue *queue) 
{
   struct request *req;
   unsigned nr_sectors, sector;
   DBG("Enterg simple request routine\n");
   req = blk_fetch_request(queue);
   while (req) 
	{
      int ret = 0;
      struct disk_dev *dev = req->rq_disk->private_data;
      if(!blk_fs_request(req)) 
		{
         ERR("Skip non-filesystem request\n");
         __blk_end_request_all(req, -EIO);
         req = blk_fetch_request(queue);
         continue;
      }
      nr_sectors = blk_rq_cur_sectors(req);
      sector = blk_rq_pos(req);
      ret = transfer(dev, sector, nr_sectors, req->buffer, rq_data_dir(req));
      if (!__blk_end_request_cur(req, ret))
		{
         req = blk_fetch_request(queue);
		}   
	}
}

/**
 * Инициализирует пул блоков
 * 
 * @param pool - инициализируемый пул
 * @param pointer - указатель на область памяти для хранения данных
 * @param block_count - количество блоков в пуле
 * @param block_size - размер блоков
 */
static void init_pool(struct block_info** pool, u8** pointer, int block_count, int block_size)
{
	int i;

	for (i = 0; i < block_count; ++i)
	{
		(*pool)[i].size = 0;
		(*pool)[i].data = *pointer;
		*pointer += block_size;				// Переходим к следующему блоку
	}
}

/**
 * Освобождает память, позаимствованную при инициализации устройства
 *
 * @param dev - очищаемое устройство
 */
static void dev_vfree(struct disk_dev *dev)
{
	if (dev->data) 
   {
      vfree(dev->data);
		dev->data = NULL;
   }
	DBG("Free data\n");
	if (dev->pool) 
   {
      vfree(dev->pool);
		dev->pool = NULL;
   }
	DBG("Free pool\n");
   if (dev->compress_buff)
   {
      vfree(dev->compress_buff);
		dev->compress_buff = NULL;
   }	
	DBG("Free compress_buff\n");
	if (dev->block_map)
	{
		vfree(dev->block_map);
		dev->block_map = NULL;
	}
	DBG("Free block_map\n");
	if (dev->empty_block && dev->empty_block->data)
	{
		vfree(dev->empty_block->data);
		dev->empty_block->data = NULL;
	}
	DBG("Free dev->empty_block\n");
	if (dev->empty_block)
	{
		vfree(dev->empty_block);
		dev->empty_block = NULL;
	}
	DBG("Free empty_block\n");
}

/**
 * Производит первичную настройку устройства
 *
 * @param dev - Настраиваемое устройство
 * @param which - Индекс устройства
 * @return int - 0, если загрузка прошла успешно, иначе - код ошибки
 */
static int setup_device (struct disk_dev* dev, int which)
{
   int i;
	u8** pointer = vmalloc(sizeof(u8*));

	// Выделяем память под хранение данных
   dev->data = vmalloc(dev->entire_count * KERNEL_SECTOR_SIZE
				         + dev->halves_count * KERNEL_SECTOR_SIZE / 2
					      + dev->quarters_count * KERNEL_SECTOR_SIZE / 4);
   if (dev->data == NULL) 
   {
      ERR("vmalloc for data array failure.\n");
      return -ENOMEM;
   }

	// Инициализируем пулы блоков
	*pointer = dev->data;
	dev->pool = vmalloc(dev->block_count * sizeof(struct block_info));
	if (dev->pool == NULL)	
	{
      ERR("vmalloc for pool failure.\n" );
      return false;
   }
	dev->pool_entire = dev->pool;
	dev->pool_halves = dev->pool_entire + dev->entire_count;
	dev->pool_quarters = dev->pool_halves + dev->halves_count;
	init_pool(&dev->pool_entire, pointer, dev->entire_count, KERNEL_SECTOR_SIZE);
	init_pool(&dev->pool_halves, pointer, dev->halves_count, KERNEL_SECTOR_SIZE / 2);
	init_pool(&dev->pool_quarters, pointer, dev->quarters_count, KERNEL_SECTOR_SIZE / 4);
	
	// Инициализируем пустой блок
	dev->empty_block = vmalloc(sizeof(struct block_info));
	if (dev->empty_block == NULL) 
   {
      ERR("vmalloc for empty block failure.\n");
      goto out_vfree;
   }
	dev->empty_block->size = 0;
	dev->empty_block->data = vmalloc(KERNEL_SECTOR_SIZE);
	if (dev->empty_block->data == NULL)
	{
	   ERR("vmalloc for empty block data failure.\n");
      goto out_vfree;
	}		
	memset(dev->empty_block->data, 0, KERNEL_SECTOR_SIZE);

	// Инициализируем карту блоков
	dev->block_map = vmalloc(dev->block_count * sizeof(struct block_info*));
	if (dev->block_map == NULL) 
   {
      ERR("vmalloc for block map failure.\n");
      goto out_vfree;
   }	
	for (i = 0; i < dev->block_count; ++i)
	{
		dev->block_map[i] = dev->empty_block;
	}

	// Буфер, используемый для компрессии/декомпрессии алгоритмом lz4
   dev->compress_buff = vmalloc(KERNEL_SECTOR_SIZE + (KERNEL_SECTOR_SIZE / 255) + 17);  
   if (dev->compress_buff == NULL) 
   {
      ERR("vmalloc for compress_buff failure.\n");
      goto out_vfree;
   }

	// Задаем объект, через который будет осуществляться спин-лок
   spin_lock_init(&dev->lock);			
   dev->queue = blk_init_queue(simple_request, &dev->lock);
   if (dev->queue == NULL) 
   {
		ERR("Init queue failure.\n");
   	goto out_vfree;
   }
   
	// Устанавливает размер блока для данного устройства
	blk_queue_logical_block_size(dev->queue, KERNEL_SECTOR_SIZE);  	
   dev->queue->queuedata = dev;
   dev->gd = alloc_disk(DEV_MINORS);                        
   if (!dev->gd) 
   {
      ERR("alloc_disk failure\n");
      goto out_vfree;
   }

   dev->gd->major = major;
   dev->gd->minors = DEV_MINORS;
   dev->gd->first_minor = which * DEV_MINORS;
   dev->gd->fops = &mybdrv_fops;
   dev->gd->queue = dev->queue;
   dev->gd->private_data = dev;
   snprintf(dev->gd->disk_name, DISK_NAME_LEN - 1, MY_DEVICE_NAME"%c", which + 'a');
   set_capacity(dev->gd, dev->block_count);
	if (strlen(dev->file_name) != 0)
	{	
		//load_data_from_disk(dev); 		// TODO загрузка с диска
	}
   add_disk(dev->gd);
	if (pointer) 
   {
      vfree(pointer);
		pointer = NULL;
   }
   return 0;

out_vfree:
   if (pointer) 
   {
      vfree(pointer);
		pointer = NULL;
   }
	DBG("Free pointer\n");
	dev_vfree(dev);
	return -ENOMEM;
}

/**
 * Проверяет входные параметры, с которыми загружен модуль
 * @return 0 - все параметры в норме, иначе - отличное от нуля значение
 */
int check_args(void)
{
	if (entire_count < 0 || halves_count < 0 || quarters_count < 0)
	{
		//DBG("No counts but str = %s\n", file_name);
		//if (strlen(file_name) == 0 || read_pools_size(file_name) < 0)
		//{
			ERR("No set pools size\n");
		   return 1;
		//}
	}
	return 0;
}

static void __exit mmapexample_module_exit(void)
{
	int i;

	// Освобождаем демона
	send_command(MESSAGE_DAEMON_STOP);
	free_page((unsigned long)info->data);
  	kfree(info);
	debugfs_remove(mmap_descriptor);

	for (i = 0; i < num_devices; i++) 
   {
      struct disk_dev *dev = Devices + i;
      if (dev->gd) 
      {
         del_gendisk(dev->gd);
         put_disk(dev->gd);
      }
      if (dev->queue) 
      {
         blk_cleanup_queue(dev->queue);
      }
      dev_vfree(dev);
   }
   unregister_blkdev(major, MY_DEVICE_NAME);
   kfree(Devices);

	printk(KERN_DEBUG "-------------------------------------\n");
	printk(KERN_DEBUG "              MMAP_UNLOAD\n");
	printk(KERN_DEBUG "-------------------------------------\n");
}

static int __init mmapexample_module_init(void)
{
	int i;

	printk(KERN_DEBUG "+++++++++++++++++++++++++++++++++++++\n");
	printk(KERN_DEBUG "              MMAP_LOAD\n");
	printk(KERN_DEBUG "+++++++++++++++++++++++++++++++++++++\n");

	if (check_args() != 0)																			// Проверяем входные параметры
	{
		mmapexample_module_exit();
		return -1;
	} 																						

   mmap_descriptor = debugfs_create_file("mmap_example", 0644, NULL, NULL, &my_fops);
	info = kmalloc(sizeof(struct mmap_info), GFP_KERNEL);
   info->data = (char*)get_zeroed_page(GFP_KERNEL);                           	// Выделяем память для буфера mmap
	send_command(MESSAGE_KERNEL_LOAD);															// Посылаем сообщение об успешной загрузке модуля
	
	// Ожидаем загрузки демона
	if (waitCommandTimer(MESSAGE_DAEMON_LOAD, 250) != 0) 
	{		
		// Если демон не загрузился за 5 секунд - произошла ошибка, отменяем загрузку модуля
		mmapexample_module_exit();
		return -1;		
	}  

	major = register_blkdev(major, MY_DEVICE_NAME);											// Регистрируем блочное устройство
   if (major <= 0) 
   {
      ERR("unable to get major number\n");
      return -EBUSY;
   }

	// Создание массива устройств
   Devices = kmalloc(num_devices * sizeof(struct disk_dev), GFP_KERNEL); 	
   if (Devices == NULL) 
   {
		unregister_blkdev(major, MY_DEVICE_NAME);
		return -ENOMEM;
   }

   for (i = 0; i < num_devices; ++i) 
   {	
	   memset(&Devices[i], 0, sizeof(struct disk_dev));
		Devices[i].entire_count = (long)entire_count;						
		Devices[i].halves_count = (long)halves_count;
		Devices[i].quarters_count = (long)quarters_count;	
		Devices[i].block_count = (long)(entire_count + halves_count + quarters_count);
		Devices[i].file_name = file_name;
      if (setup_device(&Devices[i], i) < 0)													// Инициализация каждого устройства
		{
			return -ENOMEM;
		}
   }
	return 0;
}

module_init(mmapexample_module_init);
module_exit(mmapexample_module_exit);
MODULE_LICENSE("GPL");
