#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <limits.h>

#include "../user_compression/lz4.c"
#include "../common/mmap_common.h"

#define PAGE_SIZE 4096
#define KERNEL_SECTOR_SIZE 512

#define MMAP_FILE "/sys/kernel/debug/mmap_example"

const int TICK_MS = 200;									   // Интервал опрашивания буфера на предмет получения нового сообщения	

static int   mmap_file_descriptor;							// Дескриптор, через который производится работа с mmap
static char* mmap_buff = NULL;								// Область памяти mmap
static char* compress_buff;									// Буфер для компрессии блоков
static char* decompress_buff;									// Буфер для декомпрессии блоков

int waitMessageTimer(const char message, int count_tick);
int waitSomeMessageTimer(int count_tick);
void send_message(const char message);
void decompress_block(void);
void waitSomeMessage(void);
void compress_block(void);
void close_daemon(void);
void main_cycle(void) ;
int init(void);


/**
 * Точка входа в программу
 */
int main(void)
{
	if (init() == 0)
	{
		main_cycle();
	} 
	else 
	{
		printf("Failed to load system!\n");
	}
	return 0;
}

/**
 * Главный цикл приложения
 * Обрабатывает поступающие сообщения и выполняет указанные действия
 */
void main_cycle(void) 
{
	while (1)
	{
		waitSomeMessage();
		switch (mmap_buff[0])
		{
			case MESSAGE_COMPRESS_BLOCK_DEF:
				//printf("message compress block\n");
				compress_block();
				break;

			case MESSAGE_DECOMPRESS_BLOCK_DEF:
				//printf("message decompress block\n");
				decompress_block();
				break;

			case MESSAGE_DAEMON_STOP_DEF:
				//printf("daemon stop!\n");
				return;
		}
	}
	close_daemon();
}

/**
 * Производит загрузку сервиса 
 * - Загружает модуль в ядро
 * - Подключается к файлу с расшаренной памятью (mmap) 
 *       Если не удается подключиться в течение 10 секунд - запуск считается неудачным
 *       При этом производится попытка выгрузки модуля и завершения приложения
 *
 * @return true - запуск прошел успешно, иначе - false
 */
int init(void) 
{
	const int TIMEOUT_S = 10;									// Время ожидания загрузки модуля в секундах
	const int TICK_S = 1;										// Интервал между попытками подключения в секундах
	const int COUNT_TICK_S = TIMEOUT_S / TICK_S;
	int i = 0;

	for (; i < COUNT_TICK_S; ++i)
	{
		printf("Try %d connect to kernel module\n", i); 
		mmap_file_descriptor = open(MMAP_FILE, O_RDWR);
		if (mmap_file_descriptor >= 0) 
		{
			mmap_buff = mmap(NULL, PAGE_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, mmap_file_descriptor, 0);
			if (mmap_buff == MAP_FAILED) 
			{
				return -1;
			}

			// Ожидаем загрузки модуля ядра
			if (waitMessageTimer(MESSAGE_KERNEL_LOAD, 50) != 0) 
			{		
				printf("Kernel module doesn't load!\n");
				return -1;		
			}					

			// Сообщаем модулю ядра о собственной загрузке
			send_message(MESSAGE_DAEMON_LOAD);		

			// Выделяем память под буферы компрессии/декомпрессии
			decompress_buff = malloc(KERNEL_SECTOR_SIZE); 
 			compress_buff = malloc(LZ4_compressBound(KERNEL_SECTOR_SIZE)); 
			return 0;
		}
		else 
		{
			printf("Can't open mmap_file!\n");
		}
	   sleep(TICK_S);
	}
   return -1;
}

/**
 * Ожидает появления в буффере указанного сообщения 
 * Ожидание ограничено указанным количеством тактов. 
 * В случае успешного получения сообщения - буфер сообщений очищается (записывается значение MESSAGE_EMPTY)
 * @param message - Ожидаемая команда
 * @param count_tick - Количество тактов, на протяжении которых ожидается поступление сообщения
 * 
 * @return 0 - сообщение успешно получено, отличное от нуля - сообщение не получено
 */ 
int waitMessageTimer(const char message, int count_tick)
{
	int counter = 0;
	for (; mmap_buff[0] != message && counter != count_tick; ++counter)
   {
      usleep(TICK_MS);
   }
 	if (mmap_buff[0] == message) 
	{
		mmap_buff[0] = MESSAGE_EMPTY;
		return 0;	
	}
	return -1;
}

/**
 * Ожидает появления в буффере какого-либо сообщения 
 * Ожидание ограничено указанным количеством тактов
 * @param count_tick - Количество тактов, на протяжении которых ожидается поступление сообщения
 * 
 * @return 0 - сообщение успешно получено, отличное от нуля - сообщение не получено
 */ 
int waitSomeMessageTimer(int count_tick)
{
	int counter = 0;
	for (; mmap_buff[0] == MESSAGE_EMPTY && counter != count_tick; ++counter)
   {
      usleep(TICK_MS);
   }
	return mmap_buff[0] != MESSAGE_EMPTY ? 0 : -1;
}

/**
 * Ожидает появления в буффере какого-либо сообщения
 */ 
void waitSomeMessage(void)
{
	while (waitSomeMessageTimer(INT_MAX) != 0);
}

/**
 * Посылает указанное сообщение модулю ядра
 * @param - Посылаемая команда
 */
void send_message(const char message)
{
	memcpy(mmap_buff, &message, sizeof(char));
}

/**
 * Производит компрессию блока
 * Результат помещается в буфер mmap в следующем формате:
 * | MESSAGE_BLOCK_COMPRESSED - sizeof(char) | COMPRESSED_SIZE - sizeof(size_t) | _COMPRESSED_BLOCK_ - compressed_size |
 */
void compress_block(void)
{
	char* offset = mmap_buff + sizeof(char);
	size_t compressed_size = LZ4_compress(offset, compress_buff, KERNEL_SECTOR_SIZE);
	memcpy(offset, &compressed_size, sizeof(size_t));				// Записываем размер блока после сжатия

	if (compressed_size == 0)
	{
		printf("Failed compress block!");
		send_message(MESSAGE_BLOCK_COMPRESSED);
		return;
	}
	offset += sizeof(size_t);
	memcpy(offset, compress_buff, compressed_size);					// Передаем сжатый блок
	send_message(MESSAGE_BLOCK_COMPRESSED);							// Посылаем сообщение об успешной компрессии блока
}

/**
 * Производит декомпрессию блока
 * Результат помещается в буфер mmap в следующем формате:
 * | MESSAGE_BLOCK_DECOMPRESSED - sizeof(char) | _DECOMPRESSED_BLOCK_ - KERNEL_SECTOR_SIZE |
 */
void decompress_block(void)
{
	char* offset = mmap_buff + sizeof(char);
	size_t compressed_size;
	memcpy(&compressed_size, offset, sizeof(size_t));				// Извлекаем исходный размер блока
	offset += sizeof(size_t);

   size_t decompressed_size = LZ4_decompress_fast(offset, decompress_buff, KERNEL_SECTOR_SIZE);
	if (decompressed_size != compressed_size)
	{
		printf("Failed decompress block!");
		//return; 		//TODO сделать обработку случая провала декомпрессии
	}

	offset = mmap_buff + sizeof(char);
	memcpy(offset, decompress_buff, KERNEL_SECTOR_SIZE);			// Передаем разжатый блок
	send_message(MESSAGE_BLOCK_DECOMPRESSED);							// Посылаем сообщение об успешной декомпрессии блока
}

/**
 * Завершает работу демона, освобождая задействованные ресурсы
 */
void close_daemon(void)
{
	close(mmap_file_descriptor);
	free(compress_buff);
	free(decompress_buff);
}
