#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <errno.h>
#include <string.h>

void display(char *prog, char *bytes, int n);

int main(void)
{
  const char *name_in_segment = "/IN";	
const char *name_out_segment = "/OUT";
const char *name_flag_segment = "/flag";
  const int SIZE_IN = 4096;		
 const int SIZE_FLAG = 4096;
 const int SIZE_OUT = 4096;

  int shm_fd_in;		
  char *shm_base_in;
 int shm_fd_out;		
  char *shm_base_out;
int shm_fd_flag;		
  char *shm_base_flag;
  shm_fd_in = shm_open(name_in_segment, O_RDONLY, 0666);
  if (shm_fd_in == -1) {
    printf("cons: Shared memory failed: %s\n", strerror(errno));
    exit(1);
  }
 shm_fd_flag = shm_open(name_flag_segment, O_RDONLY, 0666);
  if (shm_fd_flag == -1) {
    printf("cons: Shared memory failed: %s\n", strerror(errno));
    exit(1);
  }
 shm_fd_out = shm_open(name_out_segment, O_RDONLY, 0666);
  if (shm_fd_out == -1) {
    printf("cons: Shared memory failed: %s\n", strerror(errno));
    exit(1);
  }

 
  shm_base_in = mmap(0, SIZE_IN, PROT_READ, MAP_SHARED, shm_fd_in, 0);
  if (shm_base_in == MAP_FAILED) {
    printf("cons: Map failed: %s\n", strerror(errno));
  
    exit(1);
  }
 shm_base_flag = mmap(0, SIZE_FLAG, PROT_READ, MAP_SHARED,shm_fd_flag, 0);
  if (shm_base_flag == MAP_FAILED) {
    printf("cons: Map failed: %s\n", strerror(errno));
   
    exit(1);
  }
 shm_base_out = mmap(0, SIZE_OUT, PROT_READ, MAP_SHARED, shm_fd_out, 0);
  if (shm_base_out == MAP_FAILED) {
    printf("cons: Map failed: %s\n", strerror(errno));
   
    exit(1);
  }


  display("cons_in", shm_base_in, 64);	
  printf("%s", shm_base_in);
 display("cons_flag", shm_base_flag, 64);	
  printf("%s", shm_base_flag);
 display("cons_out", shm_base_out, 64);	
  printf("%s", shm_base_out);


  return 0;
}

void display(char *prog, char *bytes, int n)
{
  printf("display: %s\n", prog);
  for (int i = 0; i < n; i++)
    { printf("%02x%c", bytes[i], ((i+1)%16) ? ' ' : '\n'); }
  printf("\n");
}

