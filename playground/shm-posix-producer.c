#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <errno.h>

void display(char *prog, char *bytes, int n);

int main(void)
{
   const char *name_in_segment = "/IN";	
const char *name_out_segment = "/OUT";
const char *name_flag_segment = "/flag";
  const int SIZE_IN = 4096;		
 const int SIZE_FLAG = 4096;
 const int SIZE_OUT = 4096;

  int shm_fd_in;		
  char *shm_base_in;
 int shm_fd_out;		
  char *shm_base_out;
int shm_fd_flag;		
  char *shm_base_flag;		


  const char *flag = "Done";
  const char *msg_end  = "\n";
  FILE *fp;
  long lSize;
  char *buffer;

fp = fopen ( "Text.txt" , "rb" );
if( !fp ) perror("blah.txt"),exit(1);

fseek( fp , 0L , SEEK_END);
lSize = ftell( fp );
rewind( fp );

/* allocate memory for entire content */
buffer = calloc( 1, lSize+1 );
if( !buffer ) fclose(fp),fputs("memory alloc fails",stderr),exit(1);

/* copy the file into the buffer */
if( 1!=fread( buffer , lSize, 1 , fp) )
  fclose(fp),free(buffer),fputs("entire read fails",stderr),exit(1);
  	
  char *ptr;		
  shm_fd_in = shm_open(name_in_segment, O_CREAT | O_RDWR, 0666);
  if (shm_fd_in == -1) {
    printf("prod: Shared memory failed: %s\n", strerror(errno));
    exit(1);
  }
 shm_fd_flag = shm_open(name_flag_segment, O_CREAT | O_RDWR, 0666);
  if (shm_fd_flag == -1) {
    printf("prod: Shared memory failed: %s\n", strerror(errno));
    exit(1);
  }
 shm_fd_out = shm_open(name_out_segment, O_CREAT | O_RDWR, 0666);
  if (shm_fd_out == -1) {
    printf("prod: Shared memory failed: %s\n", strerror(errno));
    exit(1);
  }
  ftruncate(shm_fd_in, SIZE_IN);
 ftruncate(shm_fd_flag, SIZE_FLAG);
  ftruncate(shm_fd_out, SIZE_OUT);
  shm_base_in = mmap(0, SIZE_IN, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd_in, 0);
  if (shm_base_in == MAP_FAILED) {
    printf("prod: Map failed: %s\n", strerror(errno));
    
    exit(1);
  }
  shm_base_flag = mmap(0, SIZE_FLAG, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd_flag, 0);
  if (shm_base_flag == MAP_FAILED) {
    printf("prod: Map failed: %s\n", strerror(errno));
    
    exit(1);
  }

  shm_base_out = mmap(0, SIZE_OUT, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd_out, 0);
  if (shm_base_out == MAP_FAILED) {
    printf("prod: Map failed: %s\n", strerror(errno));
    
    exit(1);
  }

 
  display("prod_in", shm_base_in, 64);
  ptr = shm_base_in;
  ptr += sprintf(ptr, "%s", buffer);
 
  ptr += sprintf(ptr, "%s", msg_end);
  display("prod_in", shm_base_in, 64);

 display("prod_flag", shm_base_flag, 64);
  ptr = shm_base_flag;
  ptr += sprintf(ptr, "%s", flag);
 
  ptr += sprintf(ptr, "%s", msg_end);
  display("prod_flag", shm_base_flag, 64);

 display("prod_out", shm_base_out, 64);
  ptr = shm_base_out;
  //ptr += sprintf(ptr, "%s", flag);
 
  //ptr += sprintf(ptr, "%s", msg_end);

  display("prod_out", shm_base_out, 64);

  return 0;
}

void display(char *prog, char *bytes, int n)
{
  printf("display: %s\n", prog);
  for (int i = 0; i < n; i++) 
    { printf("%02x%c", bytes[i], ((i+1)%16) ? ' ' : '\n'); }
  printf("\n");
}

