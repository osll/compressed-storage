static int my_getgeo( struct block_device *bdev, struct hd_geometry *geo ) {
   unsigned long sectors = ( diskmb * 1024 ) * 2;
   DBG( KERN_INFO "getgeo\n" );
   geo->heads = 4;
   geo->sectors = 16;
   geo->cylinders = sectors / geo->heads / geo->sectors;
   geo->start = geo->sectors;
   return 0;
}

static int my_ioctl( struct block_device *bdev, fmode_t mode, unsigned int cmd, unsigned long arg ) 
{
   DBG("ioctl cmd=%X\n", cmd);
   switch (cmd) 
   {
      case 5331: 
      {
         struct hd_geometry geo;
         DBG( "ioctk HDIO_GETGEO\n" );
         my_getgeo( bdev, &geo );
         if( copy_to_user( (void __user *)arg, &geo, sizeof( geo ) ) )
         {
            return -EFAULT;
         }
         return 0;
      }

      default:
         ERR( "ioctl unknown command\n" );
         return -ENOTTY;
   }
}
