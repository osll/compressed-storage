#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include <time.h>
#define NETLINK_USER 31

#define MAX_PAYLOAD 1024  /* maximum payload size*/
struct sockaddr_nl src_addr, dest_addr;
struct nlmsghdr *nlh;
struct iovec iov;
int sock_fd;
struct msghdr msg;
struct msghdr msg2;
struct nlmsghdr *nlh2;

int writeFirstToLog(void);
int writeMessage(void);
int writeMessage2(void);
int writeFirstMessage(void);
int writeFirstToLog(){
 time_t mytime;
    mytime = time(NULL);
  
   FILE * pLog;
    pLog = fopen("/home/netlink_log.log", "a");
    if(pLog == NULL) {
        return 1;
    }
    char str[312];
    bzero(str, 312);
    strcpy(str, "Start daemon");
strcat(str, "\n");

strcat(str, ctime(&mytime));
strcat(str, "\n");
    strcat(str, " ==========================\n");
  
    fputs(str, pLog);
    //fwrite(msg, 1, sizeof(msg), pLog);
    fclose(pLog);
}
int writeFirstMessage(){
 sock_fd=socket(PF_NETLINK, SOCK_RAW, NETLINK_USER);
    if(sock_fd<0)
 return -1;

    memset(&src_addr, 0, sizeof(src_addr));
    src_addr.nl_family = AF_NETLINK;
    src_addr.nl_pid = getpid();  /* self pid */
    /* interested in group 1<<0 */
    bind(sock_fd, (struct sockaddr*)&src_addr,
        sizeof(src_addr));

    memset(&dest_addr, 0, sizeof(dest_addr));
    memset(&dest_addr, 0, sizeof(dest_addr));
    dest_addr.nl_family = AF_NETLINK;
    dest_addr.nl_pid = 0;   /* For Linux Kernel */
    dest_addr.nl_groups = 0; /* unicast */
    
    nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PAYLOAD));
    memset(nlh, 0, NLMSG_SPACE(MAX_PAYLOAD));
    nlh->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD);
    nlh->nlmsg_pid = getpid();
    nlh->nlmsg_flags = 0;
    strcpy(NLMSG_DATA(nlh), "Start");
    iov.iov_base = (void *)nlh;
    iov.iov_len = nlh->nlmsg_len;
    msg.msg_name = (void *)&dest_addr;
    msg.msg_namelen = sizeof(dest_addr);
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    printf("Sending message to kernel\n");
    sendmsg(sock_fd,&msg,0);
    printf("Waiting for message from kernel\n");

    /* Read message from kernel */
    recvmsg(sock_fd, &msg, 0);
    printf(" Received message payload: %s\n",
           NLMSG_DATA(nlh));
 FILE * pLog;
    pLog = fopen("/home/netlink_log.log", "a");
    if(pLog == NULL) {
        return 1;
    }
    char str[312];
    bzero(str, 312);
    strcpy(str, NLMSG_DATA(nlh));
strcat(str, "\n");
    strcat(str, " ==========================\n");
   
    fputs(str, pLog);
    //fwrite(msg, 1, sizeof(msg), pLog);
    fclose(pLog);
    close(sock_fd);   
         
}
int writeMessage(){
 sock_fd=socket(PF_NETLINK, SOCK_RAW, NETLINK_USER);
    if(sock_fd<0)
 return -1;

    memset(&src_addr, 0, sizeof(src_addr));
    src_addr.nl_family = AF_NETLINK;
    src_addr.nl_pid = getpid();  /* self pid */
    /* interested in group 1<<0 */
    bind(sock_fd, (struct sockaddr*)&src_addr,
        sizeof(src_addr));

    memset(&dest_addr, 0, sizeof(dest_addr));
    memset(&dest_addr, 0, sizeof(dest_addr));
    dest_addr.nl_family = AF_NETLINK;
    dest_addr.nl_pid = 0;   /* For Linux Kernel */
    dest_addr.nl_groups = 0; /* unicast */
    
    nlh2 = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PAYLOAD));
    memset(nlh, 0, NLMSG_SPACE(MAX_PAYLOAD));
    nlh2->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD);
    nlh2->nlmsg_pid = getpid();
    nlh2->nlmsg_flags = 0;
    strcpy(NLMSG_DATA(nlh2), "Done");
    iov.iov_base = (void *)nlh2;
    iov.iov_len = nlh2->nlmsg_len;
    msg2.msg_name = (void *)&dest_addr;
    msg2.msg_namelen = sizeof(dest_addr);
    msg2.msg_iov = &iov;
    msg2.msg_iovlen = 1;

    printf("Sending message to kernel\n");
    sendmsg(sock_fd,&msg2,0);
recvmsg(sock_fd, &msg2, 0);
strcpy(NLMSG_DATA(nlh2), "Decompress");
    printf(" Received message payload: %s\n",
           NLMSG_DATA(nlh));
    FILE * pLog;
    pLog = fopen("/home/netlink_log.log", "a");
    if(pLog == NULL) {
        return 1;
    }
    char str[312];
    bzero(str, 312);
    strcpy(str, NLMSG_DATA(nlh2));
strcat(str, "\n");
    strcat(str, " ==========================\n");
   
    fputs(str, pLog);
    //fwrite(msg, 1, sizeof(msg), pLog);
    fclose(pLog);
    close(sock_fd);   
         
}


int writeMessage2(){
 sock_fd=socket(PF_NETLINK, SOCK_RAW, NETLINK_USER);
    if(sock_fd<0)
 return -1;

    memset(&src_addr, 0, sizeof(src_addr));
    src_addr.nl_family = AF_NETLINK;
    src_addr.nl_pid = getpid();  /* self pid */
    /* interested in group 1<<0 */
    bind(sock_fd, (struct sockaddr*)&src_addr,
        sizeof(src_addr));

    memset(&dest_addr, 0, sizeof(dest_addr));
    memset(&dest_addr, 0, sizeof(dest_addr));
    dest_addr.nl_family = AF_NETLINK;
    dest_addr.nl_pid = 0;   /* For Linux Kernel */
    dest_addr.nl_groups = 0; /* unicast */
    
    nlh2 = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PAYLOAD));
    memset(nlh, 0, NLMSG_SPACE(MAX_PAYLOAD));
    nlh2->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD);
    nlh2->nlmsg_pid = getpid();
    nlh2->nlmsg_flags = 0;
    strcpy(NLMSG_DATA(nlh2), "Done");
    iov.iov_base = (void *)nlh2;
    iov.iov_len = nlh2->nlmsg_len;
    msg2.msg_name = (void *)&dest_addr;
    msg2.msg_namelen = sizeof(dest_addr);
    msg2.msg_iov = &iov;
    msg2.msg_iovlen = 1;

    printf("Sending message to kernel\n");
    sendmsg(sock_fd,&msg2,0);
recvmsg(sock_fd, &msg2, 0);

    printf(" Received message payload: %s\n",
           NLMSG_DATA(nlh));
    FILE * pLog;
    pLog = fopen("/home/netlink_log.log", "a");
    if(pLog == NULL) {
        return 1;
    }
    char str[312];
    bzero(str, 312);
   
    strcpy(str, NLMSG_DATA(nlh2));
strcat(str, "\n");
    strcat(str, " ==========================\n");
   
    fputs(str, pLog);
    //fwrite(msg, 1, sizeof(msg), pLog);
    fclose(pLog);
    close(sock_fd);   
         
}

int main(void) {
        
        /* Our process ID and Session ID */
        pid_t pid, sid;
        
        /* Fork off the parent process */
        pid = fork();
        if (pid < 0) {
                exit(EXIT_FAILURE);
        }
        /* If we got a good PID, then
           we can exit the parent process. */
        if (pid > 0) {
                exit(EXIT_SUCCESS);
        }

        /* Change the file mode mask */
        umask(0);
                
        /* Open any logs here */        
       
        /* Create a new SID for the child process */
        sid = setsid();
        if (sid < 0) {
                /* Log the failure */
                exit(EXIT_FAILURE);
        }
        

        
        /* Change the current working directory */
        if ((chdir("/")) < 0) {
                /* Log the failure */
                exit(EXIT_FAILURE);
        }
        
        /* Close out the standard file descriptors */
        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
        writeFirstToLog();
        /* Daemon-specific initialization goes here */
        writeFirstMessage();
 sleep(30);
        /* The Big Loop */
        while (1) {
           writeMessage();
        
           sleep(30); /* wait 30 seconds */
writeMessage2();
sleep(30);
        }
   exit(EXIT_SUCCESS);
}
