#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include "lz4.h"
#include "zlib.h"
#include "lz4.c"
#include "quicklz.h"
#include "quicklz.c"
#include "daemon_param.h"
#define DATA_SIZE 700
#define MAX_SIZE 512

static struct sockaddr_nl src_addr;
static struct sockaddr_nl dest_addr;
static struct iovec iov;
static struct nlmsghdr* nlh;
static struct msghdr msg;
static int socket_descriptor;
static char* compress_buff;
char b[MAX_SIZE];
char c[MAX_SIZE];

/**
 * Отправляет сообщение модулю ядра
 *
 * @param data - данные для передачи
 * @param length - длина данных в байтах
 */
static void send_to_kernel(const char* data, size_t length)

{

		
	memcpy(NLMSG_DATA(nlh), data, length);
        sendmsg(socket_descriptor, &msg, 0);

}

static void help_function_compress(size_t compressed_size)

{

	size_t command = COMPRESS;	
	if (compressed_size < 0)
	printf("Compress operation was not success n");
	// Если блок не сжался - сохраняем исходный блок
	if (compressed_size >= KERNEL_SECTOR_SIZE)
	{
		compressed_size = KERNEL_SECTOR_SIZE;
		memcpy(compress_buff + sizeof(size_t) * 2, NLMSG_DATA(nlh) + sizeof(size_t), KERNEL_SECTOR_SIZE);
	}
	
	// Записываем команду
	memcpy(compress_buff, &command, sizeof(size_t));

	// Записываем получившийся размер
	memcpy(compress_buff + sizeof(size_t), &compressed_size, sizeof(size_t));
	
	printf("compressed size: %d\n", (*(size_t*)(compress_buff + sizeof(size_t))));
	// Отсылаем модулю сжатый блок в формате |размер блока|+|блок|
	send_to_kernel(compress_buff, sizeof(size_t) * 2 + compressed_size);

}

static void help_function_decompress(size_t decompressed_size)

{

	size_t command = DECOMPRESS;	
	if (decompressed_size < 0)
	printf("decompress operation was not success n");
	// Если блок не сжался - сохраняем исходный блок
	if (decompressed_size >= KERNEL_SECTOR_SIZE)
	{
		decompressed_size = KERNEL_SECTOR_SIZE;
		memcpy(compress_buff + sizeof(size_t) * 2, NLMSG_DATA(nlh) + sizeof(size_t), KERNEL_SECTOR_SIZE);
	}
	
	// Записываем команду
	memcpy(compress_buff, &command, sizeof(size_t));

	// Записываем получившийся размер
	memcpy(compress_buff + sizeof(size_t), &decompressed_size, sizeof(size_t));
	
	printf("decompressed size: %d\n", (*(size_t*)(compress_buff + sizeof(size_t))));
	// Отсылаем модулю сжатый блок в формате |размер блока|+|блок|
	send_to_kernel(compress_buff, sizeof(size_t) * 2 + decompressed_size);

}

static void compress_block_with_lz4(const char* block)

{

	
	size_t compressed_size = 0; 
	printf("get block: %s\n", block);
	compressed_size = LZ4_COMPRESS(block, compress_buff );//TAKE FROM DAEMON_PARAM.H 	
	help_function_compress(compressed_size);
	
}

static void compress_block_with_zlib(const char* block)

{
 
    
    size_t compressed_size = 0;
    printf("zlib compress test start:\n");
    printf("get block: %s\n", block);
    z_stream defstream;
    defstream.zalloc = Z_NULL;
    defstream.zfree = Z_NULL;
    defstream.opaque = Z_NULL;
    defstream.avail_in = (uInt)strlen(block)+1;
    defstream.next_in = (Bytef *)block;
    defstream.avail_out = (uInt)sizeof(b);
    defstream.next_out = (Bytef *)b;  
    compressed_size = ZLIB_COMPRESS(defstream);
    help_function_compress(compressed_size);		

 
}

static void decompress_block_with_zlib(const char* block)

{

      size_t decompressed_size=0;
      printf("zlib decompress test start:\n");
      printf("get sector: %s\n", block);
      z_stream defstream;
      z_stream infstream; 
      infstream.zalloc = Z_NULL;
      infstream.zfree = Z_NULL;
      infstream.opaque = Z_NULL;
      infstream.avail_in = (uInt)((char*)defstream.next_out - block);
      infstream.next_in = (Bytef *)block;
      infstream.avail_out = (uInt)sizeof(c);
      infstream.next_out = (Bytef *)c; 
      decompressed_size=ZLIB_DECOMPRESS(infstream);
      help_function_decompress(decompressed_size);
      
}
 
static void decompress_block_with_lz4(const char* block)

{       

        printf("lz4 decompress  start:\n");
	size_t decompressed_size = 0; 
	printf("get sector: %s\n", block);
	decompressed_size = LZ4_DECOMPRESS(block, compress_buff); 	
	help_function_decompress(decompressed_size);
	

}

static void compress_block_with_quicklz( char* block) 

{
        size_t compressed_size=0;
        printf("lzma compress test start:\n");
        printf("get sector: %s\n", block);
        char  *compressed;
        size_t size_out, size_in;
	qlz_state_compress *state_compress = (qlz_state_compress *)malloc(sizeof(qlz_state_compress));
        compressed = (char*) malloc(10000 + 400); 
        compressed_size=QLZ_COMPRESS(block,decompressed,state_decompress,size_in,size_out);
        help_function_compress(compressed_size);

   
}

static void decompress_block_with_quicklz(const char* block) 

{
    size_t decompressed_size=0; 
    printf("quicklz decompress start:\n");
    printf("get sector: %s\n", block);
    char  *decompressed;
    size_t size_out, size_in;
    qlz_state_decompress *state_decompress = (qlz_state_decompress *)malloc(sizeof(qlz_state_decompress));
    decompressed = (char*) malloc(10000);
    decompressed_size=QLZ_DECOMPRESS(block,decompressed,state_decompress,size_in,size_out);  
    help_function_decompress(decompressed_size);   
 
}
/**
 * Осуществляет подключение к модулю в области ядра
 */
static void connect_to_module(void)
{
	size_t command = INIT;

	send_to_kernel((const char*)&command, sizeof(size_t));
}

/**
 * Инициализирует netlink-сокет и задает все требуемые параметры
 */
static int init_netlink(void)
{
	socket_descriptor = socket(PF_NETLINK, SOCK_RAW, NETLINK_USER);
   if (socket_descriptor < 0)
	{
		printf("Can't open socket! Err: %d\n", socket_descriptor);
   	return -1;
	}

	memset(&src_addr, 0, sizeof(src_addr));
   src_addr.nl_family = AF_NETLINK;
   src_addr.nl_pid = getpid();
	bind(socket_descriptor, (struct sockaddr*)&src_addr, sizeof(src_addr));

   memset(&dest_addr, 0, sizeof(dest_addr));
   dest_addr.nl_family = AF_NETLINK;
   dest_addr.nl_pid = 0; 
   dest_addr.nl_groups = 0;

	nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE(DATA_SIZE));
   memset(nlh, 0, NLMSG_SPACE(DATA_SIZE));
   nlh->nlmsg_len = NLMSG_SPACE(DATA_SIZE);
   nlh->nlmsg_pid = getpid();
   nlh->nlmsg_flags = 0;

   iov.iov_base = (void *)nlh;
   iov.iov_len = nlh->nlmsg_len;
   msg.msg_name = (void *)&dest_addr;
   msg.msg_namelen = sizeof(dest_addr);
   msg.msg_iov = &iov;
   msg.msg_iovlen = 1;
	return 0;
}

int main()
{
	size_t command = -1;
	
	compress_buff = malloc(LZ4_compressBound(KERNEL_SECTOR_SIZE) + sizeof(size_t) * 2);
	init_netlink();	
	
	
	while (command != STOP)
	{       connect_to_module();
		printf("Waiting command...\n");
		recvmsg(socket_descriptor, &msg, 0);
		memcpy(&command, NLMSG_DATA(nlh), sizeof(size_t));
		switch (command)
		{       
			case COMPRESS:
			{
				printf("Get compress request!\n");
				
				compress_block_with_lz4(NLMSG_DATA(nlh) + sizeof(size_t));
				//compress_block_with_zlib(NLMSG_DATA(nlh) + sizeof(size_t));
				//compress_block_with_lzma(NLMSG_DATA(nlh) + sizeof(size_t));
				break;
			};
			case DECOMPRESS:
			{       
			        printf("Get decompress request!\n");
				//decompress_block_with_lz4(NLMSG_DATA(nlh) + sizeof(size_t));
				//decompress_block_with_zlib(NLMSG_DATA(nlh) + sizeof(size_t));
				//decompress_block_with_zlib(NLMSG_DATA(nlh) + sizeof(size_t));
				break;
			};
			case STOP:
			{
				printf("Stop daemon!\n");
				break;
			};
		}
	}

	free(nlh);
	free(compress_buff);
   close(socket_descriptor);
}
