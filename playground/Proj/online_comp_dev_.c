#include "../common/common.h"
#include "../compression/lz4_compress.c"
//#include "../compression/lz4_decompress.c"
#include "../compression/lz4.h"
#include "../common/ioctl.c"
#include "../common/common.c"
#define NETLINK_USER 31
/**
* Содержит информацию о блоке.
*
* Множество таких структур составляет пул блоков.
*/
struct block_info
{
   u8 *data;			// Указатель на область памяти, где расположен данный блок
   size_t size;		// Текущий размер блока
};

/**
* Внутреннее представление данного блочного устройства
*/
struct disk_dev
{               
   long block_count;                   // Размер устройства в блоках
	u8 *data;									// Массив данных
   spinlock_t lock;             			// Используется для спин-блокировки
   struct request_queue *queue; 			// Очередь запросов к устройству
   struct gendisk *gd;          			// Указатель на структуру gendisk
	struct block_info *pool;				// Общий пул блоков
	struct block_info *pool_entire;		// Пул целых блоков
	long entire_count;						// Количество целых блоков
	struct block_info *pool_halves; 		// Пул половинных блоков
	long halves_count;						// Количество половинных блоков   
	struct block_info *pool_quarters; 	// Пул четвертных блоков
	long quarters_count;						// Количество четвертных блоков
	struct block_info *empty_block;		// Пустой блок
	struct block_info **block_map;		// Карта блоков
   char *lz4_workmem;						// Рабочая область для сжатия/разжатия данных алгоритмом LZ4
   char *compress_buff; 					// Буффер, используемый для хранения данных после операции компрессии/декомпрессии
	char *file_name;							// Имя файла, в котором хранятся данные
};

static struct disk_dev *Devices = NULL;
static struct sock *nl_sk = NULL;

// TODO - добавить возможность выбора алгоритма сжатия
static int compress_block(const char* source, char* dest, char* lz4_workmem, size_t* dest_len )
{
   return lz4_compress( source, (size_t)KERNEL_SECTOR_SIZE, dest, dest_len, lz4_workmem );
}

// TODO - добавить возможность выбора алгоритма сжатия
static int decompress_block(const char* source, size_t* source_len, char* dest )
{
   return lz4_decompress( source, source_len, dest, (size_t)KERNEL_SECTOR_SIZE );
}

/**
 * Получить свободный блок в пуле
 *
 * @param dev - устройство, на котором выполняется операция ввода/вывода
 * @param pool - пул, в котором производится поиск
 * @param pool_index - индекс пула
 * @return block_info - не занятый блок или NULL - если пул заполнен
 */
static struct block_info* get_free_block(struct disk_dev* dev, struct block_info* pool, int pool_index)
{
	int i;
	int count;	

	if (pool_index == 1) count = dev->entire_count;
	if (pool_index == 2) count = dev->halves_count;
	if (pool_index == 3) count = dev->quarters_count;

	for (i = 0; i < count; ++i)
	{
		if (pool[i].size == 0)
		{
			return &(pool[i]);
		}
	}
	return NULL;
}

/**
 * Получить пул блоков, соответствующий требуемому размеру
 *
 * @param dev - устройство, на котором выполняется операция ввода/вывода
 * @param block_size - размер блока, который будет использоваться с данным пулом
 * @param pool - пул, который удовлетворяет заданному блоку
 * @return int - возвращает индекс пула (1 - целые, 2 - половинные, 3 - четвертные)
 */
static int get_available_pool(struct disk_dev* dev, int block_size, struct block_info** pool)
{
	if (block_size <= KERNEL_SECTOR_SIZE / 4)
	{
		*pool = dev->pool_quarters;	
		return 3;
	}
	if (block_size <= KERNEL_SECTOR_SIZE / 2 && block_size > KERNEL_SECTOR_SIZE / 4)
	{
		*pool = dev->pool_halves;
		return 2;
	}
	*pool = dev->pool_entire;
	return 1;
}

/**
 * Выполняет операцию записи блока
 * 
 * @param dev - устройство, на котором выполняется операция ввода/вывода
 * @param index - индекс считываемого блока
 * @param buffer - буфер, содержащий блок для записи
 */
static int write_block(struct disk_dev* dev, int index, char *buffer)
{
	int pool_index = 0;
	int size_after_compress = -1;
	struct block_info **pool = vmalloc(sizeof(struct block_info*));
	struct block_info *block = dev->block_map[index];
	*pool = NULL;
 
	if (block->size != 0)
	{
		block->size = 0;
		dev->block_map[index] = dev->empty_block;
		DBG("Block wasn't empty, clear it!");
	}
	
	if (compress_block(buffer, dev->compress_buff, dev->lz4_workmem, &size_after_compress))
   {
   	ERR("Failed compress block: %d\n", index);
      return -EIO;
   } 
	DBG("Size after compress %d block: %d\n", index, size_after_compress);
		  
	pool_index = get_available_pool(dev, size_after_compress, pool);
	block = get_free_block(dev, *pool, pool_index);
	if (block == NULL)
	{
   	ERR("Not found free block in pool: %d\n", pool_index);
      return -EIO;		
	} 

	block->size = size_after_compress;
	memcpy(block->data, dev->compress_buff, size_after_compress);	
	dev->block_map[index] = block;
	vfree(pool);
	return 0;
}

/**
 * Выполняет операцию чтения блока и запись его в буфер
 *
 * @param dev - устройство, на котором выполняется операция ввода/вывода
 * @param index - индекс считываемого блока
 * @param buffer - буфер, в который необходимо сохранить блок
 */
static int read_block(struct disk_dev *dev, int index, char *buffer)
{

	struct block_info *block = dev->block_map[index];

	if (block->size == 0)
	{
		memcpy(buffer, block->data, KERNEL_SECTOR_SIZE);		
		DBG("Read empty block: %d\n", index);
		return 0;
	}   

	// Производилась ли компрессия блока	
	if (block->size == KERNEL_SECTOR_SIZE)
	{
		DBG("Read uncompressed block\n");
		memcpy(buffer, block->data, KERNEL_SECTOR_SIZE);
	} 
	else
	{
		DBG("Read compressed block\n");
		if (decompress_block(block->data, &block->size, dev->compress_buff))
   	{
			ERR("Failed decompress block: %d\n", index);
		   return -EIO;               
   	} 
		memcpy(buffer, dev->compress_buff, KERNEL_SECTOR_SIZE);
	}
   DBG("Success read block: %d\n", index);
	return 0;
}

/**
 * Осуществляет операцию ввода-вывода для заданного устройства.
 *
 * @param dev - устройство, для которого выполняется операция
 * @param index_first - номер начального блока
 * @param num_blocks - количество блоков, над которыми необходимо произвести операцию
 * @param buffer - буфер в/из которого будет произведена запись/чтение
 * @param is_write - значение отличное от нуля означает, что  будет произведена операция записи, иначе - чтения
 *
 * @return 0 - если операция завершена успешно, иначе - код ошибки
 */
static int transfer(struct disk_dev *dev, unsigned long index_first, 
						  unsigned long num_blocks, char *buffer, int is_write ) 
{
   int i;
   // Проверяем, что не выходим за границы реально существующей памяти
   if((index_first + num_blocks) > dev->block_count) 
   {
      ERR( "Overflow I/O operation (%ld %ld)\n", index_first, num_blocks );
      return -EIO;
   }
   
	// Обрабатываем каждый запрашиваемый блок
   for (i = 0; i < num_blocks; ++i)			
   {
      if (is_write)				
      {
			if (write_block(dev, index_first + i, buffer + i * KERNEL_SECTOR_SIZE) < 0)
			{
				return -EIO;
			}
      }
      else							
      {      
 			if (read_block(dev, index_first + i, buffer + i * KERNEL_SECTOR_SIZE) < 0)
			{
				return -EIO;
			}
      }
   }
   return 0;
}

/**
 * Простой запрос с обработкой очереди
 */
static void simple_request(struct request_queue *queue) 
{
   struct request *req;
   unsigned nr_sectors, sector;
   DBG("Enterg simple request routine\n");
   req = blk_fetch_request(queue);
   while (req) 
	{
      int ret = 0;
      struct disk_dev *dev = req->rq_disk->private_data;
      if(!blk_fs_request(req)) 
		{
         ERR("Skip non-filesystem request\n");
         __blk_end_request_all(req, -EIO);
         req = blk_fetch_request(queue);
         continue;
      }
      nr_sectors = blk_rq_cur_sectors(req);
      sector = blk_rq_pos(req);
      ret = transfer(dev, sector, nr_sectors, req->buffer, rq_data_dir(req));
      if (!__blk_end_request_cur(req, ret))
		{
         req = blk_fetch_request(queue);
		}   
	}
}

/**
 * Освобождает память, позаимствованную при инициализации устройства
 *
 * @param dev - очищаемое устройство
 */
static void dev_vfree(struct disk_dev *dev)
{
	if (dev->data) 
   {
      vfree(dev->data);
		dev->data = NULL;
   }
	DBG("Free data\n");
	if (dev->lz4_workmem) 
   {
      vfree(dev->lz4_workmem);
		dev->lz4_workmem = NULL;
   }
	DBG("Free lz4_workmem\n");
   if (dev->compress_buff)
   {
      vfree(dev->compress_buff);
		dev->compress_buff = NULL;
   }
	DBG("Free compress_buff\n");
	if (dev->pool) 
   {
      vfree(dev->pool);
		dev->pool = NULL;
   }
	DBG("Free pool\n");
	if (dev->block_map)
	{
		vfree(dev->block_map);
		dev->block_map = NULL;
	}
	DBG("Free block_map\n");
	if (dev->empty_block && dev->empty_block->data)
	{
		vfree(dev->empty_block->data);
		dev->empty_block->data = NULL;
	}
	DBG("Free dev->empty_block\n");
	if (dev->empty_block)
	{
		vfree(dev->empty_block);
		dev->empty_block = NULL;
	}
	DBG("Free empty_block\n");
}

/**
 * Сохранение/загрузка карты блоков с диска
 *
 * @param data_file - файл для записи 
 * @param dev - устройство, для которого производится сохранение
 * @param offset - смещение в файле
 * @param mode - режим (0 - сохранение, 1 - загрузка) 
 * @return int - код ошибки, либо 0 - если все прошло отлично
 */
static int block_map_to_disk(struct file* data_file, struct disk_dev* dev, 
									  loff_t* offset, int mode)
{
	int i;
	int bytes = -1;
	long diff = -1;				// TODO подумать над максимальным количеством блоков и размерностью diff
	
	for (i = 0; i < dev->block_count; ++i)
	{
		if (mode == 0)				// Сохранение
		{
			diff = (dev->block_map[i]->size == 0) ? -1 : (dev->block_map[i] - dev->pool);			
			bytes = vfs_write(data_file, (const char*)&diff, sizeof(long), offset);
			if (bytes != sizeof(long))
			{
				ERR("Failed save block in block map: %d\n", i);
				return -ENOENT;		
			}
		}	
		else							// Загрузка
		{
			bytes = vfs_read(data_file, (char*)&diff, sizeof(long), offset);
			if (bytes != sizeof(long))
			{
				ERR("Failed load block in block map: %d\n", i);
				return -ENOENT;		
			}		
			dev->block_map[i] = (diff == -1) ? dev->empty_block : (dev->pool + diff);
		}	
	}
	return 0;
}

/**
 * Сохранение/загрузка пула с диска
 * 
 * @param data_file - файл для записи 
 * @param pool - сохраняемый пул
 * @param size - размер пула (количество блоков в нем)
 * @param offset - смещение в файле
 * @param mode - режим (0 - сохранение, 1 - загрузка) 
 * @return int - код ошибки, либо 0 - если все прошло отлично
 */
static int pool_to_disk(struct file* data_file, struct block_info* pool, 
								long size, loff_t* offset, int mode)
{
	int i;
	int bytes = -1;

	for (i = 0; i < size; ++i)
	{
		if (mode == 0)
		{
			bytes = vfs_write(data_file, (const char*)&(pool[i].size), sizeof(size_t), offset);
		}
		else
		{
			bytes = vfs_read(data_file, (char*)&(pool[i].size), sizeof(size_t), offset);
		}
		
		if (bytes != sizeof(size_t))
		{
		   ERR("Failed save/load block in pool: %d\n", i);
			return -ENOENT;		
		}		
	}
	return 0;
}

/**
 * Запись данных в файл на жесткий диск
 *
 * @param dev - настраиваемое устройство
 * @return int - код ошибки, либо 0 - если все прошло отлично
 */
static int save_data_to_disk(struct disk_dev* dev)
{
	long data_size = -1;
	long written = -1;
	loff_t file_offset = 0;
   struct file* data_file;
   mm_segment_t old_fs = get_fs();

   set_fs(KERNEL_DS);
	DBG("Try to open file for save data!\n");
   data_file = filp_open(dev->file_name, O_RDWR | O_CREAT | O_TRUNC, S_IWUSR);
   if (IS_ERR(data_file)) 
	{
      ERR("Open file error: %s\n", dev->file_name);
		set_fs(old_fs);
		return -ENOENT;
   }

	// Записываем размеры пулов
	written = vfs_write(data_file, (const char*)&dev->entire_count, sizeof(long), &file_offset);
	if (written != sizeof(long))
	{
      ERR("Failed to write entire_count!\n");
		set_fs(old_fs);
		return -ENOENT;		
	}
	written = vfs_write(data_file, (const char*)&dev->halves_count, sizeof(long), &file_offset);
	if (written != sizeof(long))
	{
      ERR("Failed to write entire_count!\n");
		set_fs(old_fs);
		return -ENOENT;		
	}
	written = vfs_write(data_file, (const char*)&dev->quarters_count, sizeof(long), &file_offset);
	if (written != sizeof(long))
	{
      ERR("Failed to write entire_count!\n");
		set_fs(old_fs);
		return -ENOENT;		
	}

	// Записываем массив данных
	data_size = dev->entire_count * KERNEL_SECTOR_SIZE 
				 + dev->halves_count * KERNEL_SECTOR_SIZE / 2
				 + dev->quarters_count * KERNEL_SECTOR_SIZE / 4;
	written = vfs_write(data_file, (const char*)dev->data, data_size, &file_offset);
	if (written != data_size)
	{
      ERR("Failed to write data!\n");
		set_fs(old_fs);
		return -ENOENT;		
	}

	// Записываем пулы
	if (pool_to_disk(data_file, dev->pool_entire, dev->entire_count, &file_offset, 0) < 0
			|| pool_to_disk(data_file, dev->pool_halves, dev->halves_count, &file_offset, 0) < 0
			|| pool_to_disk(data_file, dev->pool_quarters, dev->quarters_count, &file_offset, 0) < 0)
	{
		set_fs(old_fs);
		return -ENOENT;			
	}

	// Записываем карту блоков
	block_map_to_disk(data_file, dev, &file_offset, 0);

	filp_close(data_file, NULL); 
   set_fs(old_fs);
	return 0;
}

/**
 * Чтение данных из файла и запись на загружаемое устройство
 *
 * @param dev - настраиваемое устройство
 */
static int load_data_from_disk(struct disk_dev* dev)
{
	long data_size = -1;
	long readed = -1;
	long length = -1;
	loff_t file_offset = 0;
   struct file* data_file;
   mm_segment_t old_fs = get_fs();

   set_fs(KERNEL_DS);
	DBG("Try to open file for load data!\n");
   data_file = filp_open(dev->file_name, O_RDWR, S_IRUSR);
   if (IS_ERR(data_file)) 
	{
      ERR("Open file error: %s\n", dev->file_name);
		set_fs(old_fs);
		return -ENOENT;
   }
	
	// Получаем размер файла
	length = vfs_llseek(data_file, 0L, 2);
   if(length <= 0) 
	{ 
   	ERR("Failed read file: %s\n", dev->file_name); 
      set_fs(old_fs); 
      return -ENOENT;
   } 

	// Устанавливаем указатель на начало файла
   vfs_llseek(data_file, 0L, 0);    
	
	// Считываем размеры пулов
	readed = vfs_read(data_file, (char *)&dev->entire_count, sizeof(long), &file_offset);
   if (readed != sizeof(long)) 
	{ 
   	ERR("Failed read entire_count from file: %s\n", dev->file_name); 
      set_fs(old_fs); 
      return -ENOENT;
   } 
	readed = vfs_read(data_file, (char *)&dev->halves_count, sizeof(long), &file_offset);
   if (readed != sizeof(long)) 
	{ 
   	ERR("Failed read halves_count from file: %s\n", dev->file_name); 
      set_fs(old_fs); 
      return -ENOENT;
   } 
	readed = vfs_read(data_file, (char *)&dev->quarters_count, sizeof(long), &file_offset);
   if (readed != sizeof(long)) 
	{ 
   	ERR("Failed read quarters_count from file: %s\n", dev->file_name); 
      set_fs(old_fs); 
      return -ENOENT;
   } 
	
	// Вычисляем размер устройства
	dev->block_count = dev->entire_count + dev->halves_count + dev->quarters_count;

	// Считываем данные
	data_size = dev->entire_count * KERNEL_SECTOR_SIZE 
				 + dev->halves_count * KERNEL_SECTOR_SIZE / 2
				 + dev->quarters_count * KERNEL_SECTOR_SIZE / 4;
	readed = vfs_read(data_file, (char *)dev->data, data_size, &file_offset);
   if (readed != data_size) 
	{ 
   	ERR("Failed read data from file: %s\n", dev->file_name); 
      set_fs(old_fs); 
      return -ENOENT;
   } 

	// Считываем пулы
	pool_to_disk(data_file, dev->pool_entire, dev->entire_count, &file_offset, 1);
	pool_to_disk(data_file, dev->pool_halves, dev->halves_count, &file_offset, 1);
	pool_to_disk(data_file, dev->pool_quarters, dev->quarters_count, &file_offset, 1);

	// Считываем карту блоков
	block_map_to_disk(data_file, dev, &file_offset, 1);
	filp_close(data_file, NULL); 
   set_fs(old_fs);
	return 0;
}

/**
 * Инициализирует пул блоков
 * 
 * @param pool - инициализируемый пул
 * @param pointer - указатель на область памяти для хранения данных
 * @param block_count - количество блоков в пуле
 * @param block_size - размер блоков
 */
static void init_pool(struct block_info** pool, u8** pointer, int block_count, int block_size)
{
	int i;

	for (i = 0; i < block_count; ++i)
	{
		(*pool)[i].size = 0;
		(*pool)[i].data = *pointer;
		*pointer += block_size;				// Переходим к следующему блоку
	}
}

/**
 * Производит первичную настройку устройства
 *
 * @param dev - настраиваемое устройство
 * @param which - индекс устройства
 * @return int - 0, если загрузка прошла успешно, иначе - код ошибки
 */
static int setup_device (struct disk_dev* dev, int which)
{
   int i;
	u8** pointer = vmalloc(sizeof(u8*));

	// Выделяем память под хранение данных
   dev->data = vmalloc(dev->entire_count * KERNEL_SECTOR_SIZE
				         + dev->halves_count * KERNEL_SECTOR_SIZE / 2
					      + dev->quarters_count * KERNEL_SECTOR_SIZE / 4);
   if (dev->data == NULL) 
   {
      ERR("vmalloc for data array failure.\n");
      return -ENOMEM;
   }

	// Рабочая область для компрессии/декомпрессии алгоритмом lz4
   dev->lz4_workmem = vmalloc(LZ4_MEM_COMPRESS);
   if (dev->lz4_workmem == NULL) 
   {
      ERR("vmalloc for lz4_workmem failure.\n");
      goto out_vfree;
   }

	// Буфер, используемый для компрессии/декомпрессии алгоритмом lz4
   dev->compress_buff = vmalloc(KERNEL_SECTOR_SIZE + (KERNEL_SECTOR_SIZE / 255) + 17);  
   if (dev->compress_buff == NULL) 
   {
      ERR("vmalloc for compress_buff failure.\n");
      goto out_vfree;
   }

	// Инициализируем пулы блоков
	*pointer = dev->data;
	dev->pool = vmalloc((dev->entire_count + dev->halves_count + dev->quarters_count) 
							  * sizeof(struct block_info));
	if (dev->pool == NULL)	
	{
      ERR("vmalloc for pool failure.\n" );
      return false;
   }
	dev->pool_entire = dev->pool;
	dev->pool_halves = dev->pool_entire + dev->entire_count;
	dev->pool_quarters = dev->pool_halves + dev->halves_count;
	init_pool(&dev->pool_entire, pointer, dev->entire_count, KERNEL_SECTOR_SIZE);
	init_pool(&dev->pool_halves, pointer, dev->halves_count, KERNEL_SECTOR_SIZE / 2);
	init_pool(&dev->pool_quarters, pointer, dev->quarters_count, KERNEL_SECTOR_SIZE / 4);
	
	// Инициализируем пустой блок
	dev->empty_block = vmalloc(sizeof(struct block_info));
	if (dev->empty_block == NULL) 
   {
      ERR("vmalloc for empty block failure.\n");
      goto out_vfree;
   }
	dev->empty_block->size = 0;
	dev->empty_block->data = vmalloc(KERNEL_SECTOR_SIZE);
	if (dev->empty_block->data == NULL)
	{
	   ERR("vmalloc for empty block data failure.\n");
      goto out_vfree;
	}		
	memset(dev->empty_block->data, 0, KERNEL_SECTOR_SIZE);

	// Инициализируем карту блоков
	dev->block_map = vmalloc(dev->block_count * sizeof(struct block_info*));
	if (dev->block_map == NULL) 
   {
      ERR("vmalloc for block map failure.\n");
      goto out_vfree;
   }	
	for (i = 0; i < dev->block_count; ++i)
	{
		dev->block_map[i] = dev->empty_block;
	}

	// Задаем объект, через который будет осуществляться спин-лок
   spin_lock_init(&dev->lock);			
   dev->queue = blk_init_queue(simple_request, &dev->lock);
   if (dev->queue == NULL) 
   {
		ERR("Init queue failure.\n");
   	goto out_vfree;
   }
   
	// Устанавливает размер блока для данного устройства
	blk_queue_logical_block_size(dev->queue, KERNEL_SECTOR_SIZE);  	
   dev->queue->queuedata = dev;
   dev->gd = alloc_disk(DEV_MINORS);                        
   if (!dev->gd) 
   {
      ERR("alloc_disk failure\n");
      goto out_vfree;
   }

   dev->gd->major = major;
   dev->gd->minors = DEV_MINORS;
   dev->gd->first_minor = which * DEV_MINORS;
   dev->gd->fops = &mybdrv_fops;
   dev->gd->queue = dev->queue;
   dev->gd->private_data = dev;
   snprintf(dev->gd->disk_name, DISK_NAME_LEN - 1, MY_DEVICE_NAME"%c", which + 'a');
   set_capacity(dev->gd, dev->block_count);
	if (strlen(dev->file_name) != 0)
	{	
		load_data_from_disk(dev);
	}
   add_disk( dev->gd );
	if (pointer) 
   {
      vfree(pointer);
		pointer = NULL;
   }
   return 0;

out_vfree:
   if (pointer) 
   {
      vfree(pointer);
		pointer = NULL;
   }
	DBG("Free pointer\n");
	dev_vfree(dev);
	return -ENOMEM;
}

/**
 * Получить количество свободных блоков в данном пуле
 *
 * @param dev - устройство, пул которого опрашивается
 * @param pool_index - индекс пула (1 - целые, 2 - половинные, 3 - четвертные)
 * @return int - возвращает количество свободных блоков
 */
static int get_pool_load(struct disk_dev* dev, int pool_index)
{
	int i;
	int count_in_pool;
	int count_free = 0;
	struct block_info* pool;

	if (pool_index == 1) 
	{
		pool = dev->pool_entire;
		count_in_pool = dev->entire_count;
	}
	if (pool_index == 2) 
	{
		pool = dev->pool_halves;
		count_in_pool = dev->halves_count;
	}
	if (pool_index == 3) 
	{
		pool = dev->pool_quarters;
		count_in_pool = dev->quarters_count;
	}

	for (i = 0; i < count_in_pool; ++i)
	{
		if (pool[i].size == 0)
		{
			++count_free;
		}
	}
	return count_free;
}

/**
 * Обработчик получения сообщения по netlink
 *
 * @param skb - буфер, содержащий полученное сообщение
 */
static void porcess_netlink_request(struct sk_buff* skb)
{
   int res;
   int pid;
	int dev_index = -1;
	const size_t BUFF_SIZE = 60;
	char* buff = vmalloc(BUFF_SIZE);
	
	char* hello = vmalloc(BUFF_SIZE);
	int i = 0;
	
   struct sk_buff *sending_msg;
   struct nlmsghdr *received_msg;

   received_msg = (struct nlmsghdr*)skb->data;
   ERR("Netlink received msg payload:%s\n", (char *)nlmsg_data(received_msg));
	pid = received_msg->nlmsg_pid;							// Получаем PID процесса, пославшего запрос   
	sscanf((const char *)nlmsg_data(received_msg), "%d", &dev_index);
	snprintf(buff, BUFF_SIZE, "%d %ld %d %ld %d %ld",
				get_pool_load(&Devices[dev_index], 1), Devices[dev_index].entire_count,
				get_pool_load(&Devices[dev_index], 2), Devices[dev_index].halves_count,
				get_pool_load(&Devices[dev_index], 3), Devices[dev_index].quarters_count);

	//sprintf(hello, "hello from kernel!");
	//memcpy(buff, &BUFF_SIZE, sizeof(size_t));
	//memcpy(buff + sizeof(size_t), hello, 20);
	//ERR("send: %s |%s|\n", buff + sizeof(size_t), hello);
	// Создаем ответ
   sending_msg = nlmsg_new(BUFF_SIZE, 0);
	if (sending_msg == NULL)
   {
   	ERR("Failed to allocate new skb\n");
      return;
   }

	// Отсылаем ответ
   received_msg = nlmsg_put(sending_msg, 0, 0, NLMSG_DONE, BUFF_SIZE, 0);
   NETLINK_CB(sending_msg).dst_group = 0;
   memcpy(nlmsg_data(received_msg), buff, BUFF_SIZE);
   res = nlmsg_unicast(nl_sk, sending_msg, pid);

   if (res < 0)
	{
   	ERR("Error while sending back to user\n");
	}
	vfree(hello);
	vfree(buff);
}

/**
 * Получает размеры пулов из файла
 *
 * @param file_name - имя файла, из которого производится чтение
 * @return int - 0, если загрузка прошла успешно, иначе - код ошибки
 */
static int read_pools_size(char* file_name)
{
	long readed = -1;
	long length = -1;
	loff_t file_offset = 0;
   struct file* data_file;
   mm_segment_t old_fs = get_fs();

   set_fs(KERNEL_DS);
	DBG("Try to open file for load data!\n");
   data_file = filp_open(file_name, O_RDWR, S_IRUSR);
   if (IS_ERR(data_file)) 
	{
      ERR("Open file error: %s\n", file_name);
		set_fs(old_fs);
		return -ENOENT;
   }
	
	// Получаем размер файла
	length = vfs_llseek(data_file, 0L, 2);
   if(length <= 0) 
	{ 
   	ERR("Failed read file: %s\n", file_name); 
      set_fs(old_fs); 
      return -ENOENT;
   } 

	// Устанавливаем указатель на начало файла
   vfs_llseek(data_file, 0L, 0);    
	
	// Считываем размеры пулов
	readed = vfs_read(data_file, (char *)&entire_count, sizeof(long), &file_offset);
   if (readed != sizeof(long)) 
	{ 
   	ERR("Failed read entire_count from file: %s\n", file_name); 
      set_fs(old_fs); 
      return -ENOENT;
   } 
	readed = vfs_read(data_file, (char *)&halves_count, sizeof(long), &file_offset);
   if (readed != sizeof(long)) 
	{ 
   	ERR("Failed read halves_count from file: %s\n", file_name); 
      set_fs(old_fs); 
      return -ENOENT;
   } 
	readed = vfs_read(data_file, (char *)&quarters_count, sizeof(long), &file_offset);
   if (readed != sizeof(long)) 
	{ 
   	ERR("Failed read quarters_count from file: %s\n", file_name); 
      set_fs(old_fs); 
      return -ENOENT;
   } 

	set_fs(old_fs); 
	filp_close(data_file, NULL); 
	return 0;
}

/**
 * Инициализирует модуль в ядре
 */
static int __init blk_init(void) {
   int i;
	struct netlink_kernel_cfg cfg = {
      .groups = 1,
   	.input = porcess_netlink_request
   };

	DBG( "-------------------------------\n" );
   DBG( "Online compression module LOADING!\n" );

	// Проверяем входные параметры
	if (entire_count < 0 || halves_count < 0 || quarters_count < 0)
	{
		DBG("No counts but str = %s\n", file_name);
		if (strlen(file_name) == 0 || read_pools_size(file_name) < 0)
		{
			ERR("No set pools size\n");
		   return -EBUSY;
		}
	}

   major = register_blkdev(major, MY_DEVICE_NAME);
   if (major <= 0) 
   {
      ERR("unable to get major number\n");
      return -EBUSY;
   }

	// Создание массива устройств
   Devices = kmalloc(num_devices * sizeof(struct disk_dev), GFP_KERNEL); 	
   if (Devices == NULL) 
   {
		unregister_blkdev(major, MY_DEVICE_NAME);
		return -ENOMEM;
   }

   for (i = 0; i < num_devices; ++i) 
   {	
	   memset(&Devices[i], 0, sizeof(struct disk_dev));
		Devices[i].entire_count = (long)entire_count;						
		Devices[i].halves_count = (long)halves_count;
		Devices[i].quarters_count = (long)quarters_count;	
		Devices[i].block_count = (long)(entire_count + halves_count + quarters_count);
		Devices[i].file_name = file_name;
      if (setup_device(&Devices[i], i) < 0)				// Инициализация каждого устройства
		{
			return -ENOMEM;
		}
   }

	// Инициализация netlink-сокета	
   nl_sk = netlink_kernel_create(&init_net, NETLINK_USER, &cfg);
	if (nl_sk == NULL)
   {
   	ERR("Failed init netlink socket!\n");
   	return -10;
   }
   DBG( "-------------------------------\n" );
   DBG( "Online compression module LOAD!\n" );
   return 0;
}

/**
* Вызывается при выгрузке модуля из области ядра
*/
static void blk_exit(void) 
{
   int i;
	sock_release(nl_sk->sk_socket);
   for (i = 0; i < num_devices; i++) 
   {
      struct disk_dev *dev = Devices + i;
		if (strlen(dev->file_name) != 0)
		{
			save_data_to_disk(dev);
		}
      if (dev->gd) 
      {
         del_gendisk(dev->gd);
         put_disk(dev->gd);
      }
      if (dev->queue) 
      {
         if (mode == RM_NOQUEUE) 
         {
//#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,0,0)
            blk_put_queue(dev->queue);
//#endif
         }
         else
         {
            blk_cleanup_queue(dev->queue);
         }
      }
      dev_vfree(dev);
   }
   unregister_blkdev(major, MY_DEVICE_NAME);
   kfree(Devices);
   DBG("Online compression module UNLOAD!\n");
   DBG("---------------------------------\n");
}

MODULE_AUTHOR( "Plokhoy Nikolay" );
