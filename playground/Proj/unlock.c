#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <linux/netlink.h>

#include "../user_compression/lz4.h"
#include "../user_compression/lz4.c"

#define NETLINK_USER 30

#define COMPRESS 1
#define DECOMPRESS 2
#define INIT 3
#define STOP 0

#define KERNEL_SECTOR_SIZE 512
#define DATA_SIZE KERNEL_SECTOR_SIZE + sizeof(size_t)*2

static struct sockaddr_nl src_addr;
static struct sockaddr_nl dest_addr;
static struct iovec iov;
static struct nlmsghdr* nlh;
static struct msghdr msg;
static int socket_descriptor;
static char* compress_buff;

/**
 * Отправляет сообщение модулю ядра
 *
 * @param data - данные для передачи
 * @param length - длина данных в байтах
 */
static void send_to_kernel(const char* data, size_t length)
{
	size_t com = 0;
	memset(NLMSG_DATA(nlh), 0, DATA_SIZE);
	memcpy(NLMSG_DATA(nlh), data, length);
   sendmsg(socket_descriptor, &msg, 0);
}

/**
 * Инициализирует netlink-сокет и задает все требуемые параметры
 */
static int init_netlink(void)
{
	socket_descriptor = socket(PF_NETLINK, SOCK_RAW, NETLINK_USER);
   if (socket_descriptor < 0)
	{
		printf("Can't open socket! Err: %d\n", socket_descriptor);
   	return -1;
	}

	memset(&src_addr, 0, sizeof(src_addr));
   src_addr.nl_family = AF_NETLINK;
   src_addr.nl_pid = getpid();
	bind(socket_descriptor, (struct sockaddr*)&src_addr, sizeof(src_addr));

   memset(&dest_addr, 0, sizeof(dest_addr));
   dest_addr.nl_family = AF_NETLINK;
   dest_addr.nl_pid = 0; 
   dest_addr.nl_groups = 0;

	nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE(DATA_SIZE));
   memset(nlh, 0, NLMSG_SPACE(DATA_SIZE));
   nlh->nlmsg_len = NLMSG_SPACE(DATA_SIZE);
   nlh->nlmsg_pid = getpid();
   nlh->nlmsg_flags = 0;

   iov.iov_base = (void *)nlh;
   iov.iov_len = nlh->nlmsg_len;
   msg.msg_name = (void *)&dest_addr;
   msg.msg_namelen = sizeof(dest_addr);
   msg.msg_iov = &iov;
   msg.msg_iovlen = 1;
	return 0;
}

int main()
{
	size_t command = COMPRESS;
	
	compress_buff = malloc(LZ4_compressBound(KERNEL_SECTOR_SIZE) + sizeof(size_t) * 2);
	init_netlink();	
	
	memcpy(compress_buff, &command, sizeof(size_t));
	send_to_kernel(compress_buff, sizeof(size_t));
	

	free(nlh);
	free(compress_buff);
   close(socket_descriptor);
}
