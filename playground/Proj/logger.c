struct LOG 
{
	FILE* log_file;
	time_t rawtime;
  	struct tm * timeinfo;

	/**
    * Осуществляет запись сообщения в лог
    *
    * @param logger - объект, для которого вызывается метод
    * @param message - указатель на записываемое сообщение
	 * @return int - значение, отличное от нуля означает возникшую ошибку
    */
	int (*write)(struct LOG* logger, char* message);

	/**
	 * Производит закрытие логгера и освобождение ресурсов
    * @param logger - объект, для которого вызывается метод
	 * @return int - значение, отличное от нуля означает возникшую ошибку
	 */
	int (*close)(struct LOG* logger);
};

/**
 * Осуществляет запись сообщения в лог
 *
 * @param logger - объект, для которого вызывается метод
 * @param message - указатель на записываемое сообщение
 */
int LOG_write(struct LOG* logger, char* message)
{
	//char buffer[20] = {'\0'};	
	//time(&(logger->rawtime));
  	//logger->timeinfo = localtime(&(logger->rawtime));
	
	//if (fprintf(logger->log_file, "%s: %s\n", asctime(logger->timeinfo), message) < 0)
	// TODO сделать вывод времени
	if (fprintf(logger->log_file, "%s\n", message) < 0)	
	{
		return -1;
	}
	fflush(logger->log_file);
}

/**
 * Производит закрытие логгера и освобождение ресурсов
 * @param logger - объект, для которого вызывается метод
 * @return int - значение, отличное от нуля означает возникшую ошибку
 */
int LOG_close(struct LOG* logger)
{
	logger->write(logger, "LOG-file close!");	
	if (fclose(logger->log_file) < 0)
	{
		return -1;
	}
}

/**
 * Инициализирует лог
 * 
 * @param logger - объект, для которого вызывается метод
 * @param file_name - имя файла, в который будет выводиться лог
 * @return int - значение, отличное от нуля означает возникшую ошибку при инициализации
 */
int LOG_init(struct LOG* logger, char* file_name)
{
	// Инициализируем методы
	logger->write = LOG_write;
	logger->close = LOG_close;
	
	// TODO сделать дозаписываемый файл
	logger->log_file = fopen(file_name, "w");
	logger->write(logger, "LOG-file created!");
	return 0;
}

