#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x9c3f02a1, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x4d405db8, __VMLINUX_SYMBOL_STR(param_ops_string) },
	{ 0x15692c87, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0x704192bd, __VMLINUX_SYMBOL_STR(sock_release) },
	{ 0x3f6880ab, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x6ee8d721, __VMLINUX_SYMBOL_STR(__netlink_kernel_create) },
	{ 0xe8efc5e1, __VMLINUX_SYMBOL_STR(init_net) },
	{ 0x999e8297, __VMLINUX_SYMBOL_STR(vfree) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0xd6ee688f, __VMLINUX_SYMBOL_STR(vmalloc) },
	{ 0x414f988b, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0xb988d7d7, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x3e3abe7f, __VMLINUX_SYMBOL_STR(netlink_unicast) },
	{ 0x2ab3bb53, __VMLINUX_SYMBOL_STR(__nlmsg_put) },
	{ 0xd86e8c28, __VMLINUX_SYMBOL_STR(__alloc_skb) },
	{ 0x50eedeb8, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xb4390f9a, __VMLINUX_SYMBOL_STR(mcount) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "4ADEBA185A2E3516CAA6967");
