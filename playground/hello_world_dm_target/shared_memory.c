#include "shared_memory.h"

struct dentry  *file1;

struct mmap_info {
    char *data;
    int reference;       /* how many times it is mmapped */     
};


/* keep track of how many times it is mmapped */

void mmap_open(struct vm_area_struct *vma) {
    struct mmap_info *info = (struct mmap_info *)vma->vm_private_data;
    info->reference++;
}

void mmap_close(struct vm_area_struct *vma) {
    struct mmap_info *info = (struct mmap_info *)vma->vm_private_data;
    info->reference--;
}

int mmap_fault(struct vm_area_struct *vma, struct vm_fault *vmf) {
    struct page *page;
    struct mmap_info *info;
    //the data is in vma->vm_private_data
    info = (struct mmap_info *)vma->vm_private_data;
    if (!info->data) {
        printk("There is no data\n");
        return 0;    
    }

    /* get the page */
    page = virt_to_page(info->data);
    
    /* increment the reference count of this page */
    get_page(page);
    vmf->page = page;
    return 0;
}

/*
 virtual MM functions - opening of an area, closing and unmapping it 
 (needed to keep files on disk up-to-date etc)
*/
struct vm_operations_struct mmap_vm_ops = {
    .open =     mmap_open,
    .close =    mmap_close,
    .fault =    mmap_fault,
};

int my_mmap(struct file *filp, struct vm_area_struct *vma) {
    vma->vm_ops = &mmap_vm_ops;
    vma->vm_flags |= VM_RESERVED;
    /* assign the file private data to the vm private data */
    vma->vm_private_data = filp->private_data;
    mmap_open(vma);
    return 0;
}

//Free memmory allocated in my_open
int my_close(struct inode *inode, struct file *filp) {
    struct mmap_info *info = filp->private_data;
    free_page((unsigned long)info->data);
    kfree(info);
    filp->private_data = NULL;
    return 0;
}

//Allocate new memmory to shared it with user space.
int my_open(struct inode *inode, struct file *filp) {
    struct mmap_info *info = kmalloc(sizeof(struct mmap_info), GFP_KERNEL);
    //Allocate one page of memmory filled by zeros.
    info->data = (char *)get_zeroed_page(GFP_KERNEL);
    //Add message in new page.
    memcpy(info->data, "hello from kernel this is file: ", 32);
    memcpy(info->data + 32, filp->f_dentry->d_name.name, strlen(filp->f_dentry->d_name.name));
    //Assign this info struct to the file
    filp->private_data = info;
    return 0;
}

const struct file_operations my_fops = {
    .open = my_open,
    .release = my_close,
    .mmap = my_mmap,
};

void create_shared_memory() {
    file1 = debugfs_create_file("mmap_example", 0644, NULL, NULL, &my_fops);
}

void remove_shared_memory() {
    debugfs_remove(file1);
}