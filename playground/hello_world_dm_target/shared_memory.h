#ifndef SHARED_MEMORY_H
#define SHARED_MEMORY_H

#include <linux/fs.h>
#include <linux/debugfs.h>
#include <linux/slab.h>
#include <linux/mm.h>  /* mmap related stuff */

#ifndef VM_RESERVED
# define VM_RESERVED   (VM_DONTEXPAND | VM_DONTDUMP)
#endif

void mmap_open(struct vm_area_struct *vma);

void mmap_close(struct vm_area_struct *vma);

int mmap_fault(struct vm_area_struct *vma, struct vm_fault *vmf);

int my_mmap(struct file *filp, struct vm_area_struct *vma);

int my_close(struct inode *inode, struct file *filp);

int my_open(struct inode *inode, struct file *filp);

void create_shared_memory(void);

void remove_shared_memory(void);

#endif