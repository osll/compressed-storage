#ifndef NETLINK_COMUNICATION
#define NETLINK_COMUNICATION

#include <net/sock.h>
#include <linux/skbuff.h>
#include <linux/netlink.h>

#ifndef NETLINK_USERSOCK
#define NETLINK_USERSOCK 21
#endif

int netlink_create(void);

void netlink_release(void);

void nl_callback(struct sk_buff *skb);

#endif