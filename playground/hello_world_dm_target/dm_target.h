#ifndef DM_TARGET
#define DM_TARGET

#include <linux/bio.h>
#include <linux/device-mapper.h>
#include <linux/slab.h>

int test_dev_map(struct dm_target *ti, struct bio *bio);

int test_dev_ctr(struct dm_target *ti,unsigned int argc,char **argv);

void test_dev_dtr(struct dm_target *ti);

int register_target(void);

void unregister_target(void);

#endif