#include "dm_target.h"

struct my_dm_target {
    struct dm_dev *dev;
    sector_t start;
};

int test_dev_map(struct dm_target *ti, struct bio *bio) {
    struct my_dm_target *mdt = (struct my_dm_target *) ti->private;

    bio->bi_bdev = mdt->dev->bdev;

    if((bio->bi_rw & WRITE) == WRITE) {
        printk(KERN_CRIT "\n test_dev_map : bio is a write request.... \n");
    } else {
        printk(KERN_CRIT "\n test_dev_map : bio is a read request.... \n");
    }
    submit_bio(bio->bi_rw,bio);
     
    return DM_MAPIO_SUBMITTED;
}

int test_dev_ctr(struct dm_target *ti,unsigned int argc,char **argv) {
    struct my_dm_target *mdt;
    unsigned long long start;

    if (argc != 2) {
        ti->error = "Invalid argument count";
        return -EINVAL;
    }

    mdt = kmalloc(sizeof(struct my_dm_target), GFP_KERNEL);

    if(mdt==NULL) {
        ti->error = "dm-test_dev: Cannot allocate linear context";
        return -ENOMEM;
    }       

    if(sscanf(argv[1], "%llu", &start)!=1) {
        ti->error = "dm-test_dev: Invalid device sector";
        kfree(mdt);         
        return -EINVAL;
    }

    mdt->start=(sector_t)start;

    if (dm_get_device(ti, argv[0], dm_table_get_mode(ti->table), &mdt->dev)) {
        ti->error = "dm-test_dev: Device lookup failed";
        kfree(mdt);          
        return -EINVAL;
    }

    ti->private = mdt;                   
    return 0;
}

void test_dev_dtr(struct dm_target *ti) {
    struct my_dm_target *mdt = (struct my_dm_target *) ti->private;       
    dm_put_device(ti, mdt->dev);
    kfree(mdt);
}

struct target_type test_dev = { 
    .name = "test_dev",
    .version = {1,0,0},
    .module = THIS_MODULE,
    .ctr = test_dev_ctr,
    .dtr = test_dev_dtr,
    .map = test_dev_map,
};

int register_target() {
    return dm_register_target(&test_dev);
}

void unregister_target() {
    dm_unregister_target(&test_dev);
}