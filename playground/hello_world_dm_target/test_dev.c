#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include "netlink_comunication.h"
#include "dm_target.h"
#include "shared_memory.h"
       
/*---------Module Functions -----------------*/

static int init_test_dev(void) {
    int result = register_target();
    if(result < 0) {
        printk(KERN_CRIT "\n Error in registering target \n");
        return result;
    }

    if (netlink_create() < 0) {
        printk(KERN_CRIT "Can't create netlink.\n");
        return -ENOMEM;
    }
    create_shared_memory();
    return 0;
}


static void cleanup_test_dev(void) {
    unregister_target();
    netlink_release();
    remove_shared_memory();
}

module_init(init_test_dev);
module_exit(cleanup_test_dev);
MODULE_LICENSE("GPL");
