#include "netlink_comunication.h"

struct sock *nlsk;

struct netlink_kernel_cfg netlink_cfg = {
    .input = nl_callback,
};

int netlink_create() {
    nlsk = netlink_kernel_create(&init_net, NETLINK_USERSOCK, &netlink_cfg);
    if (!nlsk) {
        return -ENOMEM;
    }
    return 0;
}

void netlink_release() {
    netlink_kernel_release(nlsk);
}

void nl_callback(struct sk_buff *skb){
    int pid;
    struct nlmsghdr * nlh;
    int msg_size;
    char* msg = "This is an answer.";
    struct sk_buff *skb_out;

    nlh = (struct nlmsghdr *) skb->data;
    printk(KERN_INFO "Netlink received msg: %s\n", (char *)nlmsg_data(nlh));
    pid = nlh->nlmsg_pid;

    msg_size = strlen(msg);
    skb_out = nlmsg_new(msg_size, 0);
    if (!skb_out) {
        printk(KERN_ERR "Failed to allocate new skb.\n");
        return;
    }

    nlh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, msg_size, 0);
    NETLINK_CB(skb_out).dst_group = 0; /* not in mcast group */
    strncpy(nlmsg_data(nlh), msg, msg_size);

    if (nlmsg_unicast(nlsk, skb_out, pid) < 0) {
        printk(KERN_INFO "Error while sending message back to user.\n");
    }
}