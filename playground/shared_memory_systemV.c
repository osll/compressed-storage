#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
 
#define MAXSIZE     27
 
void die(char *s)
{
    perror(s);
    exit(1);
}
 
int main()
{
    char c;
    int shmid;
    key_t key;
    char *shm, *s;
 
    key = 5678;

 /*  shmget- системный вызов для получения идентификатора разделяемого сегмента памяти         
  *  возвращает shmid
  *   
  *  
  */
    if ((shmid = shmget(key, MAXSIZE, IPC_CREAT | 0666)) < 0)
        die("shmget");

 /*  shmat-включение области разделяемой памяти в адресное пространство         
  *  текущего процесса
  *  shmaddr = NULL, позволяя операционной системе самой разместить разделяемую 
  *  память в адресном пространстве нашего процесса
  */
    if ((shm = shmat(shmid, NULL, 0)) == (char *) -1)
        die("shmat");
 
    /*
     *       Помещаем информацию в память(все буквы английского алфавита),       для    того 
              чтобы другой процесс прочел
     *        
     *       
     */
    s = shm;
 
    for (c = 'a'; c <= 'z'; c++)
        *s++ = c;
 
 
    /*
     * Ждем пока другой процесс
     * поменяет первый символ в памяти на 
     *  '*', означающее, что мы прочли из памяти.
     */
    while (*shm != '*')
        sleep(1);
 
    exit(0);
}
